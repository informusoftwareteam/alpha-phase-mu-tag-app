//
//  Login.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/11/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON
import PromiseKit
import Locksmith
import RealmSwift
import GoogleSignIn
import FacebookCore
import FacebookLogin

enum LoginData {
    case informu(String, String)
    case google
    case facebook
}

enum AuthenticationError: Error {
    case blankPassword
    case blankVerifyPassword
    case unmatchingPasswords
    case sessionLoginDataNotFound
    case accountNotFoundInDatabase
    case undefinedLoginProvider
    case failedToSaveAccountInDatabase
    case facebookAccessTokenNotFound
    case facebookLoginCancelled
    case timeout
}

extension AuthenticationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .blankPassword: return NSLocalizedString("You have not entered a password", comment: "")
        case .blankVerifyPassword: return NSLocalizedString("You must enter your password twice", comment: "")
        case .unmatchingPasswords: return NSLocalizedString("The passwords you entered do no match", comment: "")
        case .sessionLoginDataNotFound: return NSLocalizedString("Could not find any previously saved login sessions", comment: "")
        case .accountNotFoundInDatabase: return NSLocalizedString("Account not found in the database", comment: "")
        case .undefinedLoginProvider: return NSLocalizedString("Account not found in the database", comment: "")
        case .failedToSaveAccountInDatabase: return NSLocalizedString("Account could not be saved to database", comment: "")
        case .facebookAccessTokenNotFound: return NSLocalizedString("Account could not be saved to database", comment: "")
        case .facebookLoginCancelled: return NSLocalizedString("Account could not be saved to database", comment: "")
        case .timeout: return NSLocalizedString("The connection timed out", comment: "")
        }
    }
}

extension AuthErrorCode: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .userDisabled: return NSLocalizedString("This account is disabled", comment: "")
        case .emailAlreadyInUse: return NSLocalizedString("An account already exists with this email address", comment: "")
        case .invalidEmail: return NSLocalizedString("You have not entered a valid email address", comment: "")
        case .wrongPassword: return NSLocalizedString("The password you have entered is incorrect", comment: "")
        case .tooManyRequests: return NSLocalizedString("You have made too many invalid login attempts", comment: "")
        case .userNotFound: return NSLocalizedString("No account exists with this email address", comment: "")
        case .weakPassword: return NSLocalizedString("Password does not meet complexity requirements", comment: "")
        default: return nil
        }
    }
}

extension AuthCredential {
    enum Provider: String {
        case facebook = "facebook.com"
        case google = "google.com"
    }
    
    var providerType: Provider? {
        return Provider(rawValue: provider)
    }
}

extension NSNotification.Name {
    static let googleSignInComplete = NSNotification.Name("googleSignInComplete")
}

class Login: NSObject {
    fileprivate let realm = try! Realm()
    fileprivate let firebase = Database.database().reference()
    fileprivate let accountFactory = AccountFactory()
    fileprivate let muTagRepo: MuTagRepository
    fileprivate let locationService = LocationService.shared
    //let muTagBleSessionManager = MuTagBleSessionManager.shared
    fileprivate let notificationCenter = NotificationCenter.default
    
    fileprivate var session: Session
    
    init(muTagRepo: MuTagRepository) throws {
        self.session = try Session.shared()
        self.muTagRepo = muTagRepo
        super.init()
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func create(username: String, password: String, passwordVerify: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            if username.isEmpty {
                reject(AuthErrorCode.invalidEmail)
            }
            
            if password.isEmpty {
                reject(AuthenticationError.blankPassword)
            }
            
            if passwordVerify.isEmpty {
                reject(AuthenticationError.blankVerifyPassword)
            }
            
            if !doesPasswordMatch(password: password, passwordVerify: passwordVerify) {
                reject(AuthenticationError.unmatchingPasswords)
            }
            
            Auth.auth().createUser(withEmail: username.lowercased(), password: password) { (fibuser, error) in
                if let e = error {
                    reject(e)
                } else {
                    let user = User(value: ["id": fibuser!.uid, "email": fibuser!.email])

                    self.accountFactory.create(with: user).then { account -> Void in
                        try self.saveAccountToDatabase(account: account)
                        
                        let loginData = LoginData.informu(username.lowercased(), password)
                        
                        try self.updateSession(account: account, data: loginData)
                        
                        fulfill(())
                    }.catch { error in
                        reject(error)
                    }
                }
            }
        }
    }
    
    func authenticateFromSession() -> Promise<Void> {
        guard let loginData = session.loginData else { return Promise<Void> { _,_ in throw AuthenticationError.sessionLoginDataNotFound } }
        
        switch loginData {
        case .informu(let username, let password):
            return authenticate(username: username, password: password)
        case .google:
            return signInWithGoogle()
        case .facebook:
            return signInWithFacebookAccessToken()
        }
    }
    
    func authenticate(username: String, password: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            var promiseHasFulfilled = false
            var promiseHasRejected = false
            if username.isEmpty {
                reject(AuthErrorCode.invalidEmail)
                return
            } else if password.isEmpty {
                reject(AuthenticationError.blankPassword)
                return
            }
            print("Authenticating to Firebase with email and password credentials...")
            Timer.scheduledTimer(withTimeInterval: 8.0, repeats: false) { _ in
                if promiseHasFulfilled { return }
                promiseHasRejected = true
                reject(AuthenticationError.timeout)
            }
            Auth.auth().signIn(withEmail: username.lowercased(), password: password) { (fibuser, error) in
                if promiseHasRejected { return }
                if let e = error {
                    reject(e)
                } else {
                    let user = User(value: ["id": fibuser!.uid, "email": fibuser!.email])
                    print("Successfully authenticated to Firebase. Received user ID \(user.id) with email \(user.email)")
                    self.setCrashlytics(identifier: user.email)
                    self.getAccount(for: user).then { account -> Void in
                        let loginData = LoginData.informu(username.lowercased(), password)
                        try self.updateSession(account: account, data: loginData)
                        promiseHasFulfilled = true
                        fulfill(())
                    }.catch { error in
                        reject(error)
                    }
                }
            }
        }
    }
    
    func authenticate(with federatedCredentials: AuthCredential) -> Promise<Void> {
        return Promise { fulfill, reject in
            var promiseHasFulfilled = false
            var promiseHasRejected = false
            guard let provider = federatedCredentials.providerType else { reject(AuthenticationError.undefinedLoginProvider); return }
            
            print("Authenticating to Firebase with \(provider) credentials...")
            
            Timer.scheduledTimer(withTimeInterval: 8.0, repeats: false) { _ in
                if promiseHasFulfilled { return }
                
                promiseHasRejected = true
                reject(AuthenticationError.timeout)
            }
            
            Auth.auth().signIn(with: federatedCredentials) { (fibuser, error) in
                if promiseHasRejected { return }
                if let e = error {
                    reject(e)
                } else {
                    let user = User(value: ["id": fibuser!.uid, "email": fibuser!.email])
                    print("Successfully authenticated to Firebase. Received user ID \(user.id) with email \(user.email)")
                    self.setCrashlytics(identifier: user.email)
                    self.getAccount(for: user).then { account -> Void in
                        try completeFulfill(account: account, authCredential: federatedCredentials)
                    }.catch { error in
                        if error == AuthenticationError.accountNotFoundInDatabase {
                            print("Account does not exist for \(user.email). Let's try creating one now...")
                            self.accountFactory.create(with: user).then { account -> Void in
                                try self.saveAccountToDatabase(account: account)
                                print("Successfully created account in Firebase")
                                try completeFulfill(account: account, authCredential: federatedCredentials)
                            }.catch { error in
                                reject(error)
                            }
                        } else {
                            reject(error)
                        }
                    }
                    func completeFulfill(account: Account, authCredential: AuthCredential) throws {
                        switch provider {
                        case .google:
                            try self.updateSession(account: account, data: LoginData.google)
                        case .facebook:
                            try self.updateSession(account: account, data: LoginData.facebook)
                        }
                        promiseHasFulfilled = true
                        fulfill(())
                    }
                }
            }
        }
    }
    
    func signInWithGoogle() -> Promise<Void> {
        return Promise { fulfill, reject in
            GIDSignIn.sharedInstance().signIn()
            
            var notificationObserver: NSObjectProtocol?
            notificationObserver = notificationCenter.addObserver(forName: .googleSignInComplete, object: nil, queue: nil) { notification in
                self.notificationCenter.removeObserver(notificationObserver!, name: .googleSignInComplete, object: nil)
                
                if let error = notification.userInfo?["error"] as? Error {
                    reject(error); return
                }
                
                fulfill(()); return
            }
        }
    }
    
    func signInWithFacebook(viewController: UIViewController) -> Promise<Void> {
        return Promise { fulfill, reject in
            let loginManager = LoginManager()
            
            loginManager.logIn(readPermissions: [.email], viewController: viewController) { loginResult in
                switch loginResult {
                case .failed(let error):
                    reject(error)
                case .cancelled:
                    reject(AuthenticationError.facebookLoginCancelled)
                case .success(_,_, let accessToken):
                    let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
                    
                    self.authenticate(with: credential).then { _ in
                        fulfill(())
                    }.catch { error in
                        reject(error)
                    }
                }
            }
        }
    }
    
    func signInWithFacebookAccessToken() -> Promise<Void> {
        guard let accessToken = AccessToken.current else { return Promise<Void> { _,_ in throw AuthenticationError.facebookAccessTokenNotFound } }
        let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
        
        return authenticate(with: credential)
    }
}

private extension Login {
    func checkPasswordComplexity() {
        
    }
    
    func doesPasswordMatch(password: String, passwordVerify: String) -> Bool {
        if password != passwordVerify {
            return false
        }
        return true
    }
    
    func getAccount(for user: User) -> Promise<Account> {
        return Promise { fulfill, reject in
            getAccountId(for: user).then { accountId -> Void in
                print("Retrieving account from Firebase for account ID \(accountId)...")
                
                let ref = Database.database().reference()
                
                ref.child("accounts/\(accountId)").observeSingleEvent(of: .value, with: { (snapshot) in
                    print("Successfully retrieved account from Firebase")
                    
                    let accountProperties = snapshot.value as? [String : AnyObject] ?? [:]
                    let safeZone = accountProperties["safe_zone"] as? String ?? ""
                    let account = Account(value: ["id": accountId, "user": user, "safeZone": safeZone])
                    
                    self.muTagRepo.get(forAccountId: accountId).then { muTags -> Void in
                        if let m = muTags {
                            // Determine if any mu tags have been deleted from the database but not from the local device
                            //
                            let databaseMuTagIds = Set(m.map { $0.id })
                            let localMuTags = self.realm.objects(MuTag.self)
                            let localMuTagIds = Set(localMuTags.map { $0.id })
                            let deletedMuTags = localMuTagIds.subtracting(databaseMuTagIds)
                            // Delete local mu tags that have already been deleted from the database
                            //
                            DispatchQueue.global(qos: .background).sync {
                                autoreleasepool {
                                    do {
                                        let realm = try Realm()
                                        try realm.write {
                                            for muTagId in deletedMuTags {
                                                if let muTag = MuTag.get(with: muTagId) {
                                                    self.locationService.stopMonitoringFor(muTagIds: [muTag.id])
                                                    realm.delete(muTag)
                                                }
                                            }
                                        }
                                    } catch {
                                        print(error)
                                    }
                                }
                            }
                            account.muTags.append(objectsIn: m)
                            let muTagIds: Set<String> = Set(account.muTags.flatMap { $0.id })
                            self.locationService.startMonitoringFor(muTagIds: muTagIds)
                            fulfill(account)
                        } else {
                            // All mu tags have been deleted so delete them locally
                            //
                            DispatchQueue.global(qos: .background).sync {
                                autoreleasepool {
                                    do {
                                        let realm = try Realm()
                                        let localMuTags = realm.objects(MuTag.self)
                                        try realm.write {
                                            realm.delete(localMuTags)
                                            self.locationService.stopMonitoringFor()
                                        }
                                    } catch {
                                        print(error)
                                    }
                                }
                            }
                            fulfill(account)
                        }
                    }.catch { error in
                        reject(error)
                    }
                }) { (error) in
                    reject(error); return
                }
            }.catch { error in
                reject(error)
            }
        }
    }
    
    func getAccountId(for user: User) -> Promise<String> {
        return Promise { fulfill, reject in
            print("Retrieving account ID from Firebase for user ID \(user.id)...")
            
            firebase.child("users/\(user.id)/account_id").observeSingleEvent(of: .value, with: { (snapshot) in
                guard let accountId = snapshot.value as? String else { reject(AuthenticationError.accountNotFoundInDatabase); return }
                
                print("Found account ID \(accountId)")
                
                fulfill(accountId)
            }) { (error) in
                reject(error)
            }
        }
    }
    
    func updateSession(account: Account, data: LoginData) throws {
        // Update working credentials or login type in keychain
        //
        try Session.saveLogin(with: data)
        print("Login data successfully saved to keychain")
        DispatchQueue.global(qos: .background).sync {
            autoreleasepool {
                do {
                    let realm = try Realm()
                    try realm.write {
                        // Here 'account' is an unmanaged Realm object so we do not need to create a thread safe reference
                        //
                        realm.create(Session.self, value: ["account": account], update: true)
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func saveAccountToDatabase(account: Account) throws {
        guard let user = account.user else { throw AuthenticationError.failedToSaveAccountInDatabase }
        
        let accountValues: Dictionary<String, Any> = [
            "user_id": user.id,
            "email": user.email
        ]
        
        // Save user to account association and account to Firebase
        //
        firebase.child("users/\(user.id)").setValue(["account_id": account.id])
        firebase.child("accounts/\(account.id)").setValue(accountValues)
    }
    
    func setCrashlytics(identifier: String) {
        Crashlytics.sharedInstance().setUserEmail(identifier)
    }
}

extension Login: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let e = error {
            self.notificationCenter.post(name: .googleSignInComplete, object: nil, userInfo: ["error": e])
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        authenticate(with: credential).then {
            self.notificationCenter.post(name: .googleSignInComplete, object: nil)
        }.catch { error in
            self.notificationCenter.post(name: .googleSignInComplete, object: nil, userInfo: ["error": error])
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // TODO ?
    }
}
