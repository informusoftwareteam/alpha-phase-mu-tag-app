//
//  MuTagRepository.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/2/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import SwiftyJSON
import PromiseKit
import Firebase
import RealmSwift

enum MuTagRepoError: Error {
    case requiredMuTagPropertiesNotSet
    case jsonToStringFailed
    case createMuTagObjectFailed
    case accountNotFound
    case muTagIdGenerationFailed
    case muTagNotFound
}

class MuTagRepository {
    // Singleton shared instance
    //
    static let shared = MuTagRepository()
    
    fileprivate let realm: Realm
    fileprivate let ref = Database.database().reference()
    var notificationToken: NotificationToken?
    //var tokenThread: Thread?
    
    // Prevent init of this singleton with 'private'
    //
    private init() {
        realm = try! Realm()
        prepareRealmNotifications()
        
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    // Create, Read, Update, Delete (CRUD)
    ///
    
    func add(muTag: MuTag) -> Promise<Void> {
        return Promise { fulfill, reject in
            if ( muTag.id == "" ||
                muTag.macAddress == "" ||
                muTag.firmwareVersion == "" ||
                muTag.major == "" ||
                muTag.minor == "" ||
                muTag.attachedTo == "" ||
                muTag.tagColor == 0
            ) {
                reject(MuTagRepoError.requiredMuTagPropertiesNotSet); return
            }
            print("Adding new mμ tag to account...")
            muTag.dateAdded = ISO8601DateFormatter().string(from: Date())
            muTag.lastSeen = muTag.dateAdded
            let muTagDictionary: Dictionary<String, Any> = [
                "account_id": muTag.major,
                "mac_address": muTag.macAddress,
                "firmware_version": muTag.firmwareVersion,
                "date_added": muTag.dateAdded,
                "major": muTag.major,
                "minor": muTag.minor,
                "attached_to": muTag.attachedTo,
                "expensive": muTag.expensive,
                "irreplaceable": muTag.irreplaceable,
                "tag_color": muTag.tagColor
            ]
            // Save registered mu tag to Firebase
            //
            ref.child("accounts/\(muTag.major)/mu_tags/\(muTag.id)").setValue(true)
            ref.child("mu_tags/\(muTag.id)").setValue(muTagDictionary)
            // Save mu tag to Realm after Firebase to complete registration
            //
            DispatchQueue.global(qos: .background).sync {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        let session = try Session.shared()
                        guard let account = session.account else { return }
                        muTag.recentLocation = session.mostRecentLocation ?? ""
                        muTag.recentLocationName = session.mostRecentLocationName ?? ""
                        try realm.write {
                            // Here 'muTag' is an unmanaged Realm object so we do not need to create a thread safe reference
                            //
                            account.muTags.append(muTag)
                        }
                    } catch {
                        reject(error)
                    }
                }
            }
            fulfill(())
        }
    }
    
    func get(forAccountId accountId: String, with muTagIds: Array<String>? = nil) -> Promise<[MuTag]?> {
        return Promise { fulfill, reject in
            if muTagIds != nil {
                // TODO: Get only mu tags requested from Firebase
            } else {
                ref.child("accounts/\(accountId)/mu_tags").observeSingleEvent(of: .value, with: { (snapshot) in
                    // Determine how many mu tags were returned by Firebase
                    //
                    var waitCount = snapshot.childrenCount
                    
                    if waitCount == 0 {
                        fulfill(nil); return
                    }
                    
                    var muTags = [MuTag]()
                    let enumerator = snapshot.children
                    while let childSnapshot = enumerator.nextObject() as? DataSnapshot {
                        let c = childSnapshot
                        
                        self.ref.child("mu_tags/\(c.key)").observeSingleEvent(of: .value, with: { (snapshot) in
                            let muTagFirebaseProperties = snapshot.value as! Dictionary<String, Any>
                            let muTagProperties = [
                                "id": snapshot.key,
                                "major": muTagFirebaseProperties["major"],
                                "minor": muTagFirebaseProperties["minor"],
                                "attachedTo": muTagFirebaseProperties["attached_to"],
                                "macAddress": muTagFirebaseProperties["mac_address"],
                                "firmwareVersion": muTagFirebaseProperties["firmware_version"],
                                "expensive": muTagFirebaseProperties["expensive"],
                                "irreplaceable": muTagFirebaseProperties["irreplaceable"],
                                "tagColor": muTagFirebaseProperties["tag_color"],
                                "dateAdded": muTagFirebaseProperties["date_added"],
                                "txPower": muTagFirebaseProperties["tx_power"],
                                "recentLocation": muTagFirebaseProperties["recent_location"],
                                "recentLocationName": muTagFirebaseProperties["recent_location_name"],
                                "inSafeZone": muTagFirebaseProperties["in_safe_zone"],
                                "lastReentry": muTagFirebaseProperties["last_re-entry"],
                                "unsafe": muTagFirebaseProperties["unsafe"],
                                "proximity": muTagFirebaseProperties["proximity"],
                                "accuracy": muTagFirebaseProperties["accuracy"],
                                "rssi": muTagFirebaseProperties["rssi"],
                                "batteryLevel": muTagFirebaseProperties["battery_level"],
                                "detectInterval": muTagFirebaseProperties["detect_interval"],
                                "advertisingInterval": muTagFirebaseProperties["advertising_interval"]
                            ]
                            
                            let muTag = MuTag(value: muTagProperties)
                            muTags.append(muTag)
                            
                            waitCount -= 1
                            
                            if waitCount == 0 {
                                fulfill(muTags)
                            }
                        }) { (error) in
                            reject(error); return
                        }
                    }
                }) { (error) in
                    reject(error); return
                }
            }
        }
    }
    
    func updateFor(muTagId: String, enterEvent: Bool = false, exitEvent: Bool = false, usefulEvent: Bool? = nil, overrideTimestamp: Date? = nil) -> Promise<String> {
        return Promise { fulfill, reject in
            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { throw MuTagRepoError.muTagNotFound }
            let useful: Int = {
                guard let useful = usefulEvent else {
                    return 0
                }
                if useful {
                    return 1
                }
                return 2
            }()
            let dateNow = ISO8601DateFormatter().string(from: overrideTimestamp ?? Date())
            // Prepare for write to Firebase
            //
            let ref = Database.database().reference()
            let muTagStream = [
                "tx_power": muTag.txPower,
                "recent_location": muTag.recentLocation,
                "in_safe_zone": muTag.inSafeZone,
                "last_seen": muTag.lastSeen,
                "last_re-entry": muTag.lastReentry,
                "unsafe": muTag.unsafe,
                "detect_interval": muTag.detectInterval,
                "advertising_interval": muTag.advertisingInterval,
                "rssi": muTag.rssi,
                "battery_level": muTag.batteryLevel,
                "proximity": muTag.proximity,
                "accuracy": muTag.accuracy,
                "ios_enter_event": enterEvent,
                "ios_exit_event": exitEvent,
                "useful": useful
                ] as [String : Any]
            
            // Update mu tag to Firebase
            //
            ref.child("mu_tags/\(muTagId)/attached_to").setValue(muTag.attachedTo)
            ref.child("mu_tags/\(muTagId)/mac_address").setValue(muTag.macAddress)
            ref.child("mu_tags/\(muTagId)/firmware_version").setValue(muTag.firmwareVersion)
            ref.child("mu_tags/\(muTagId)/expensive").setValue(muTag.expensive)
            ref.child("mu_tags/\(muTagId)/irreplaceable").setValue(muTag.irreplaceable)
            ref.child("mu_tags/\(muTagId)/tag_color").setValue(muTag.tagColor)
            ref.child("mu_tags/\(muTagId)/tx_power").setValue(muTagStream["tx_power"])
            ref.child("mu_tags/\(muTagId)/recent_location").setValue(muTagStream["recent_location"])
            ref.child("mu_tags/\(muTagId)/recent_location_name").setValue(muTag.recentLocationName)
            ref.child("mu_tags/\(muTagId)/in_safe_zone").setValue(muTagStream["in_safe_zone"])
            ref.child("mu_tags/\(muTagId)/last_seen").setValue(muTagStream["last_seen"])
            ref.child("mu_tags/\(muTagId)/last_re-entry").setValue(muTagStream["last_re-entry"])
            ref.child("mu_tags/\(muTagId)/unsafe").setValue(muTagStream["unsafe"])
            ref.child("mu_tags/\(muTagId)/detect_interval").setValue(muTagStream["detect_interval"])
            ref.child("mu_tags/\(muTagId)/advertising_interval").setValue(muTagStream["advertising_interval"])
            ref.child("mu_tags/\(muTagId)/rssi").setValue(muTagStream["rssi"])
            ref.child("mu_tags/\(muTagId)/battery_level").setValue(muTagStream["battery_level"])
            ref.child("mu_tags/\(muTagId)/proximity").setValue(muTagStream["proximity"])
            ref.child("mu_tags/\(muTagId)/accuracy").setValue(muTagStream["accuracy"])
            // Now write the stream data
            //
            ref.child("mu_tag_data_streams/\(muTagId)/\(dateNow)").setValue(muTagStream)
            fulfill(dateNow)
        }
    }
    
    
    
    func removeWith(muTagId: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            print("Attempting to remove mµ tag \(muTagId) from this account...")
            guard let muTag = MuTag.get(with: muTagId) else { throw MuTagRepoError.muTagNotFound }
            self.unprovisionInDatabaseFor(accountId: muTag.major, muTagId: muTagId).then { _ -> () in
                let muTagRef = ThreadSafeReference(to: muTag)
                DispatchQueue.global(qos: .background).async {
                    autoreleasepool {
                        do {
                            let realm = try Realm()
                            guard let muTag = realm.resolve(muTagRef) else { return }
                            try realm.write {
                                realm.delete(muTag)
                            }
                            fulfill(())
                        } catch {
                            reject(error)
                        }
                    }
                }
            }.catch { error in
                reject(error)
            }
        }
    }
}

private extension MuTagRepository {
    
    // TODO: Move this to FirebaseReactiveService
    
    func prepareRealmNotifications() {
        do {
            let realm = try Realm()
            let muTags = realm.objects(MuTag.self)
            notificationToken = muTags.observe { [weak self] (changes: RealmCollectionChange) in
                switch changes {
                case .initial: break
                case .update(_, _, _, let modifications):
                    for modification in modifications {
                        let muTag = muTags[modification]
                        _ = self?.updateFor(muTagId: muTag.id)
                    }
                    break
                case .error(let error):
                    // An error occurred while opening the Realm file on the background worker thread
                    fatalError("\(error)")
                    break
                }
            }
        } catch {
            print(error)
        }
    }
    
    func unprovisionInDatabaseFor(accountId: String, muTagId: String) -> Promise<Void> {
        var promise: Promise<Void>?
        promise = Promise { fulfill, reject in
            print("Attempting to unprovision mµ tag in database...")
            ref.child("mu_tags/\(muTagId)").observeSingleEvent(of: .value, with: { snapshot in
                self.ref.child("unprovisioned_mu_tags/\(muTagId)").setValue(snapshot.value)
                self.ref.child("mu_tags/\(muTagId)").removeValue()
                self.ref.child("accounts/\(accountId)/unprovisioned_mu_tags/\(muTagId)").setValue(true)
                self.ref.child("accounts/\(accountId)/mu_tags/\(muTagId)").removeValue()
                fulfill(())
            }) { error in
                if promise!.isPending {
                    reject(error)
                }
            }
        }
        return promise!
    }
}
