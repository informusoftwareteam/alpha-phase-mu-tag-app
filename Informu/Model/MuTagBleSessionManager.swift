//
//  MuTagBleSessionManager.swift
//  Informu
//
//  Created by Tom Daniel D. on 10/20/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import CoreBluetooth
import PromiseKit
import RealmSwift

enum MuTagBleSessionManagerError: Error {
    case muTagOutOfRange
    case failedToConnect
    case connectionTimedOut
    case dispatcherNotFound
}

protocol MuTagBleSessionManagerDelegate: class {
    func didConnectToNewMuTag(muTag: MuTag, bleDispatcher: MuTagBleDispatcher)
}

extension NSNotification.Name {
    static let updatePeripheralConnectionStatus = NSNotification.Name("updatePeripheralConnectionStatus")
    static let peripheralDidDisconnect = NSNotification.Name("peripheralDidDisconnect")
}

class MuTagBleSessionManager: NSObject {
    // Singleton shared instance
    //
    static let shared = MuTagBleSessionManager()
    
    fileprivate let centralManager = CBCentralManager()
    fileprivate let notificationCenter = NotificationCenter.default
    fileprivate let requirementsService = RequirementsService()
    fileprivate let muTagName = "Informu Beacon"
    
    weak var delegate: MuTagBleSessionManagerDelegate?
    
    //fileprivate var peripheralsInDiscovery = [UUID : CBPeripheral]()
    fileprivate var peripheralsInDiscovery = Set<CBPeripheral>()
    fileprivate var muTagPeripheralIdentifiers = [String : UUID]()
    fileprivate var ignoredPeripherals = Set<UUID>()
    fileprivate var bleDispatchers = [UUID : MuTagBleDispatcher]()
    fileprivate var muTagConnectionsToKeepOpen = Set<String>()
    fileprivate var scanningForNewMuTag = false
    
    // Prevent init of this singleton with 'private'
    //
    private override init() {
        super.init()
        self.centralManager.delegate = self
    }
    
    func scanForNewMuTagAndConnect() {
        scanningForNewMuTag = true
        
        // This method will automatically connect and stay connected to a new mu tag if 'scanningForNewMuTag' is 'true'
        //
        startScanningForMuTags()
    }
    
    func stopScanningForNewMuTag() {
        scanningForNewMuTag = false
        stopScanningForMuTags()
    }
    
    func connect(to muTag: MuTag, force: Bool = false) -> Promise<MuTagBleDispatcher> {
        var promise: Promise<MuTagBleDispatcher>?
        promise = Promise<MuTagBleDispatcher> { fulfill, reject in
            if muTag.unsafe && force == false {
                reject(MuTagBleSessionManagerError.muTagOutOfRange); return
            }
            muTagConnectionsToKeepOpen.insert(muTag.id)
            if let dispatcher = getDispatcherFor(muTagId: muTag.id) {
                if dispatcher.peripheral.state == .connected {
                    fulfill(dispatcher); return
                }
                centralManager.connect(dispatcher.peripheral)
            } else {
                startScanningForMuTags()
            }
            // Wait for peripheral to connect
            //
            var notificationObserver: NSObjectProtocol?
            notificationObserver = notificationCenter.addObserver(forName: .updatePeripheralConnectionStatus, object: nil, queue: nil, using: { (notification) in
                guard let peripheralIdentifier = self.muTagPeripheralIdentifiers[muTag.id] else { return }
                guard let userInfo = notification.userInfo as? [String : CBPeripheral] else { return }
                guard let (userInfoKey, notificationPeripheral) = userInfo.first else { return }
                
                if notificationPeripheral.identifier != peripheralIdentifier {
                    return
                }
                switch userInfoKey {
                case "didFailToConnect": reject(MuTagBleSessionManagerError.failedToConnect)
                case "didConnect":
                    guard let dispatcher = self.getDispatcherFor(muTagId: muTag.id) else {
                        self.stopScanningForMuTags()
                        reject(MuTagBleSessionManagerError.dispatcherNotFound); break
                    }
                    dispatcher.discoverServicesAndCharactertistics().then { _ -> () in
                        self.stopScanningForMuTags()
                        fulfill(dispatcher)
                    }.catch { error in
                        self.stopScanningForMuTags()
                        reject(error)
                    }
                default: return
                }
                
                self.notificationCenter.removeObserver(notificationObserver!, name: .updatePeripheralConnectionStatus, object: nil)
            })
            after(seconds: 20).then { _ -> () in
                if promise!.isPending {
                    self.stopScanningForMuTags()
                    self.notificationCenter.removeObserver(notificationObserver!, name: .updatePeripheralConnectionStatus, object: nil)
                    self.muTagConnectionsToKeepOpen.remove(muTag.id)
                    if let peripheralIdentifier = self.muTagPeripheralIdentifiers[muTag.id] {
                        if let dispatcher = self.bleDispatchers[peripheralIdentifier] {
                            self.centralManager.cancelPeripheralConnection(dispatcher.peripheral)
                        }
                    }
                    reject(MuTagBleSessionManagerError.connectionTimedOut)
                }
            }
        }
        return promise!
    }
    
    func disconnectFromMuTagWith(id: String) {
        muTagConnectionsToKeepOpen.remove(id)
        closeConnectionIfUnnecessary(for: id)
        guard let dispatcher = getDispatcherFor(muTagId: id) else { return }
        dispatcher.getPeripheralStatusWithMuTag().then { status -> () in
            switch status {
            case .unregistered(_):
                print("Removing peripheral references for unregistered mµ tag...")
                self.muTagPeripheralIdentifiers[id] = nil
                self.bleDispatchers[dispatcher.peripheral.identifier] = nil
            default: break
            }
        }.catch { error in
            print(error)
        }
    }

    func getDispatcherFor(muTagId: String) -> MuTagBleDispatcher? {
        // Here we are seeing if the saved peripheral for a given mu tag is still active in Core Bluetooth.
        // If it's not, then we will remove all references to it along with the associated dispatcher.
        //
        print("Getting BLE dispatcher for mµ tag \(muTagId)")
        guard let peripheralIdentifier = muTagPeripheralIdentifiers[muTagId] else { return nil }
        guard let peripheral = getPeripheral(from: peripheralIdentifier) else {
            removeLegacyPeripheralFor(muTagId: muTagId)
            return nil
        }
        return bleDispatchers[peripheral.identifier]
    }
    
    func removeLegacyPeripheralFor(muTagId: String) {
        guard let peripheralIdentifier = muTagPeripheralIdentifiers[muTagId] else { return }
        muTagPeripheralIdentifiers[muTagId] = nil
        bleDispatchers[peripheralIdentifier] = nil
    }
    
    // TODO: This may not be needed
    //
    /*func cancelAllBleConnections() {
        for peripheralUuid in unregisteredMuTags.keys {
            guard let peripheral = muTagPeripheralIdentifiers[peripheralUuid] else { return }
            
            centralManager.cancelPeripheralConnection(peripheral)
        }
    }*/
}

fileprivate extension MuTagBleSessionManager {
    // TODO: Need to test if this is necessary since we are scanning
    //       for only peripherals with specified services
    //
    /*func doesPeripheralHaveMuTagName(peripheral: CBPeripheral) -> Bool {
        if peripheral.name == muTagName {
            return true
        }
        
        return false
    }*/
    
    func startScanningForMuTags() {
        print("Started scanning for mμ tags...")
        
        // TODO: Test with CBCentralManagerScanOptionAllowDuplicatesKey: false
        //
        // Cannot scan for mu tag by services because Central Manager will ignore them since mu tag is an iBeacon
        //
        centralManager.scanForPeripherals(withServices: nil,
                                          options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }
    
    func stopScanningForMuTags() {
        print("Scanning for mµ tags has stopped")
        centralManager.stopScan()
    }
    
    func isMuTagInImmediateProximity(rssi: Int) -> Bool {
        if (rssi > -56) && (rssi != 127) {
            return true
        }
        
        return false
    }
    
    func hasPeripheralAlreadyBeenFound(peripheral: CBPeripheral) -> Bool {
        if (
            peripheralsInDiscovery.contains(peripheral) ||
            muTagPeripheralIdentifiers.values.contains(peripheral.identifier)
        ) {
            return true
        }
        
        return false
    }
    
    func closeConnectionIfUnnecessary(for muTagId: String) {
        if !muTagConnectionsToKeepOpen.contains(muTagId) {
            print("mu tag \(muTagId) connection deemed unnecessary and is now closing")
            let realm = try! Realm()
            guard let dispatcher = getDispatcherFor(muTagId: muTagId) else { return }
            centralManager.cancelPeripheralConnection(dispatcher.peripheral)
        }
    }
    
    func getPeripheral(from uuid: UUID) -> CBPeripheral? {
        if let peripheral = centralManager.retrievePeripherals(withIdentifiers: [uuid]).first {
            return peripheral
        }
        return nil
    }
}

extension MuTagBleSessionManager: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        requirementsService.checkBluetoothSettings(from: central)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        // DEBUG
        if peripheral.name == "Informu Beacon" {
            print("DEBUG - discovered peripheral with ID \(peripheral.identifier), name \(peripheral.name!), and RSSI \(RSSI)")
        }
        
        if scanningForNewMuTag && !isMuTagInImmediateProximity(rssi: RSSI.intValue) { return }
        if (
            !ignoredPeripherals.contains(peripheral.identifier) &&
            !hasPeripheralAlreadyBeenFound(peripheral: peripheral) &&
            peripheral.state == .disconnected
        ) {
            print("Found a new BLE peripheral that might be a mμ tag")
            
            // Cache the peripheral as a currently available device (required by CentralManager)
            //
            print("Caching BLE peripheral with UUID \(peripheral.identifier)...")
            peripheralsInDiscovery.insert(peripheral)
            
            print("Connecting to \(peripheral.identifier)...")
            centralManager.connect(peripheral, options: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        if !scanningForNewMuTag && (peripheral.name != muTagName) && (peripheral.name != "OTA") {
            peripheralsInDiscovery.remove(peripheral)
            ignoredPeripherals.insert(peripheral.identifier)
            centralManager.cancelPeripheralConnection(peripheral)
        } else if !bleDispatchers.keys.contains(peripheral.identifier) {
            let dispatcher: MuTagBleDispatcher!
            do {
                dispatcher = try MuTagBleDispatcher(peripheral: peripheral)
            } catch {
                print(error)
                self.centralManager.cancelPeripheralConnection(peripheral)
                self.bleDispatchers[peripheral.identifier] = nil
                self.peripheralsInDiscovery.remove(peripheral)
                return
            }
            bleDispatchers[peripheral.identifier] = dispatcher
            
            
            dispatcher.getPeripheralStatusWithMuTag().then { statusWithMuTag -> () in
                switch statusWithMuTag {
                case .valid(let muTag):
                    print("Found BLE peripheral that matches a mμ tag for this account")
                    self.muTagPeripheralIdentifiers[muTag.id] = peripheral.identifier
                    self.notificationCenter.post(name: .updatePeripheralConnectionStatus, object: nil, userInfo: ["didConnect": peripheral])
                    self.closeConnectionIfUnnecessary(for: muTag.id)
                case .unregistered(let muTag):
                    print("Found BLE peripheral that is an unregistered mμ tag")
                    self.muTagPeripheralIdentifiers[muTag.id] = peripheral.identifier
                    if self.scanningForNewMuTag {
                        self.stopScanningForNewMuTag()
                        self.muTagConnectionsToKeepOpen.insert(muTag.id)
                        guard let dispatcher = self.getDispatcherFor(muTagId: muTag.id) else {
                            throw MuTagBleSessionManagerError.dispatcherNotFound
                        }
                        self.delegate?.didConnectToNewMuTag(muTag: muTag, bleDispatcher: dispatcher)
                    }
                    
                    self.closeConnectionIfUnnecessary(for: muTag.id)
                case .invalid, .registeredToOtherAccount:
                    print("Found BLE peripheral that is not a valid mμ tag or is a mμ tag that's registered to a different account")
                    self.centralManager.cancelPeripheralConnection(peripheral)
                    self.ignoredPeripherals.insert(peripheral.identifier)
                    self.bleDispatchers[peripheral.identifier] = nil
                case .unknown:
                    print("Found BLE peripheral with an unknown status")
                    self.centralManager.cancelPeripheralConnection(peripheral)
                    self.bleDispatchers[peripheral.identifier] = nil
                }
                
                self.peripheralsInDiscovery.remove(peripheral)
            }.catch { error in
                print(error)
                self.centralManager.cancelPeripheralConnection(peripheral)
                self.bleDispatchers[peripheral.identifier] = nil
                self.peripheralsInDiscovery.remove(peripheral)
            }
        } else {
            self.notificationCenter.post(name: .updatePeripheralConnectionStatus, object: nil, userInfo: ["didConnect": peripheral])
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected from bluetooth peripheral \(peripheral.identifier)")
        
        // TODO: Create delegate to respond when current mu tag being added disconnects
        
        if let e = error {
            print(e)
        }
        self.notificationCenter.post(name: .peripheralDidDisconnect, object: nil, userInfo: ["didDisconnect": peripheral.identifier])
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Failed to connect to bluetooth peripheral \(peripheral.identifier)")
        notificationCenter.post(name: .updatePeripheralConnectionStatus, object: nil, userInfo: ["didFailToConnect": peripheral])
        if let e = error {
            print(e)
        }
    }
}
