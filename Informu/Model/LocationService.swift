//
//  LocationService.swift
//  Informu
//
//  Created by Tom Daniel D. on 10/18/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift
import PromiseKit

enum LocationServiceError: Error {
    case receivedBadDateString
}

protocol LocationServiceDelegate: class {
    //func muTagDidEnterRegion(muTag: MuTag)
    //func muTagDidExitRegion(muTag: MuTag)
    func didEnterSafeZone()
    func didExitSafeZone()
}

class LocationService: NSObject {
    // MARK: Singleton shared instance
    //
    static let shared = LocationService()
    
    fileprivate let realm = try! Realm()
    fileprivate let locationManager = CLLocationManager()
    fileprivate let requirementsService = RequirementsService()
    fileprivate let muTagStatusManager = MuTagStatusManager()
    
    weak var delegate: LocationServiceDelegate?
    
    fileprivate var geocoderTimeout = false
    fileprivate var geocoderQueuedLocation = [(CLLocation, Set<String>)]()
    
    // Prevent init of this singleton with 'private'
    //
    private override init() {
        self.locationManager.allowsBackgroundLocationUpdates = true
        super.init()
        self.setLocationAccuracyForForeground()
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        //self.locationManager.pausesLocationUpdatesAutomatically = true
        
        // DEBUG
        //self.stopMonitoringFor()
        //print("DEBUG - self.locationManager.monitoredRegions \(self.locationManager.monitoredRegions)")
    }
    
    func startMonitoringFor(muTagIds: Set<String>) {
        for muTagId in muTagIds {
            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { continue }
            guard let beaconRegion = muTag.beaconRegion else { continue }
            print("Mu tag with beacon region \(beaconRegion) has started monitoring...")
            locationManager.startMonitoring(for: beaconRegion)
        }
    }
    
    func stopMonitoringFor(muTagIds: Set<String>? = nil) {
        if let muTagIds = muTagIds {
            for muTagId in muTagIds {
                guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { continue }
                guard let beaconRegion = muTag.beaconRegion else { continue }
                print("Mu tag with beacon region \(beaconRegion) has stopped monitoring")
                locationManager.stopMonitoring(for: beaconRegion)
            }
        } else {
            for region in locationManager.monitoredRegions {
                locationManager.stopMonitoring(for: region)
            }
            print("All regions have stopped monitoring")
        }
    }
    
    func startRanging(for muTags: Array<MuTag>) -> Promise<Void> {
        return Promise { fulfill, _ in
            for muTag in muTags {
                guard let beaconRegion = muTag.beaconRegion else { continue }
                locationManager.startRangingBeacons(in: beaconRegion)
                print("Mu tag with beacon region \(beaconRegion) has started ranging...")
                Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { timer in
                    if let muTagRecentlySeen = try? self.wasMuTagRecentlySeen(muTag: muTag) {
                        if !muTagRecentlySeen && !muTag.unsafe {
                            self.muTagStatusManager.handleDidExitRegionIfValidFor(muTagId: muTag.id)
                        }
                    } else {
                        if !muTag.unsafe {
                            self.muTagStatusManager.handleDidExitRegionIfValidFor(muTagId: muTag.id)
                        }
                    }
                    fulfill(())
                }
            }
        }
    }
    
    func registerSafeZone(at location: CLLocationCoordinate2D) {
        print("Registering safe zone...")
        let latitude = location.latitude
        let longitude = location.longitude
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                do {
                    let session = try Session.shared()
                    guard let account = session.account else { return }
                    let realm = try Realm()
                    try realm.write {
                        account.safeZone = String(latitude) + " " + String(longitude)
                    }
                    self.startMonitoringSafeZone(at: location)
                    _ = self.getSafeZoneStatus()
                } catch {
                    print(error)
                }
            }
        }
    }

    func getSafeZoneStatus() -> Bool {
        print("Updating safe zone status...")
        if let safeZoneRegion = locationManager.monitoredRegions.filter({ $0.identifier == "safezone" }).first {
            locationManager.requestState(for: safeZoneRegion)
            return true
        } else {
            print("Monitored region for safe zone not found. Setting up safe zone monitoring...")
            do {
                let session = try Session.shared()
                guard let account = session.account else { return false }
                if let safeZoneCoordinate = parseLocationCoordinate(from: account.safeZone) {
                    startMonitoringSafeZone(at: safeZoneCoordinate)
                    return getSafeZoneStatus()
                } else {
                    // TODO: Should prompt user to setup safe zone
                    return false
                }
            } catch {
                print(error); return false
            }
        }
    }
    
    func parseLocationCoordinate(from string: String) -> CLLocationCoordinate2D? {
        let splitLocation = string.components(separatedBy: " ")
        
        guard let latitude = Double(splitLocation[0]) else { return nil }
        guard let longitude = Double(splitLocation[1]) else { return nil }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func setLocationAccuracyForForeground() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 0
    }
    
    func setLocationAccuracyForBackground() {
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 10
    }
}

private extension LocationService {
    func wasMuTagRecentlySeen(muTag: MuTag) throws -> Bool {
        let dateFormatter = ISO8601DateFormatter()
        let lastSeenDate = dateFormatter.date(from: muTag.lastSeen)
        guard let lastSeen = lastSeenDate else { throw LocationServiceError.receivedBadDateString }
        let secondsSinceLastSeen = Date().timeIntervalSince(lastSeen)
        if secondsSinceLastSeen < 8 {
            return true
        }
        return false
    }
    
    func startMonitoringSafeZone(at location: CLLocationCoordinate2D) {
        let safeZone = CLCircularRegion(center: location, radius: 50, identifier: "safezone")
        self.locationManager.startMonitoring(for: safeZone)
        print("Successfully registered and started monitoring safe zone")
    }
    
    func updateLocation(from location: CLLocation) {
        //print("Attempting to update location...")
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        let recentLocationCoordinates = "\(latitude) \(longitude)"
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                do {
                    let realm = try Realm()
                    let session = try Session.shared()
                    let presentMuTags = realm.objects(MuTag.self).filter("unsafe = false")
                    try realm.write {
                        session.previousLocation = session.mostRecentLocation
                        session.mostRecentLocation = recentLocationCoordinates
                        for muTag in presentMuTags {
                            muTag.recentLocation = recentLocationCoordinates
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func updateAddress(from location: CLLocation) {
        let presentMuTagIds: Set<String> = Set(realm.objects(MuTag.self).filter("unsafe = false").flatMap { $0.id })
        if let (previousLocation, previousMuTagIds) = geocoderQueuedLocation.last {
            geocoderQueuedLocation.removeLast()
            let previousDifferences = previousMuTagIds.filter { !presentMuTagIds.contains($0) }
            // If there are any mu tags in the former queue slot that are not currently present
            // then we replace the last queue slot with only the mu tag differences remaining.
            //
            if previousDifferences.count > 0 {
                geocoderQueuedLocation.append((previousLocation, previousDifferences))
            }
        }
        geocoderQueuedLocation.append((location, presentMuTagIds))
        if geocoderTimeout {
            print("Location address update has been queued")
        } else {
            print("Attempting to update location address...")
            updateNameFromGeocoderQueue()
        }
    }
    
    func updateNameFromGeocoderQueue() {
        print("There are currently \(geocoderQueuedLocation.count) location(s) queued for a name update")
        print("Attempting to update next location name from queue...")
        let (location, muTagIds) = geocoderQueuedLocation.removeFirst()
        self.geocoderTimeout = true
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            Timer.scheduledTimer(withTimeInterval: 61.0, repeats: false) { timer in
                self.geocoderTimeout = false
                if self.geocoderQueuedLocation.count > 0 {
                    self.updateNameFromGeocoderQueue()
                }
            }
            if let e = error { print(e); return }
            guard let placemark = placemarks?.first else { return }
            var streetNumber = ""
            if let subThoroughfare = placemark.subThoroughfare {
                streetNumber = subThoroughfare + " "
            }
            let street = placemark.thoroughfare ?? "see on map"
            let city: String = {
                if let locality = placemark.locality {
                    return ", \(locality)"
                } else {
                    return ""
                }
            }()
            let recentLocationName = "\(streetNumber):\(street):\(city)"
            DispatchQueue.global(qos: .background).async {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        let session = try Session.shared()
                        let muTags = Set(realm.objects(MuTag.self))
                        let muTagsToUpdate = muTags.filter { muTagIds.contains($0.id) }
                        try realm.write {
                            session.previousLocationName = session.mostRecentLocationName
                            session.mostRecentLocationName = recentLocationName
                            for muTag in muTagsToUpdate {
                                muTag.recentLocationName = recentLocationName
                            }
                        }
                    } catch {
                        print(error)
                    }
                }
            }
        })
    }
}

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        requirementsService.checkLocationPermissions(from: status)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Failed monitoring region: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        if beacons.count > 0 {
            for beacon in beacons {
                guard let muTag = MuTag.get(from: region) else { continue }
                muTag.updateStateForSignificantChange(from: beacon)
                if muTag.unsafe {
                    muTagStatusManager.handleDidExitRegionIfValidFor(muTagId: muTag.id)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        print("Detected state (\(state.rawValue)); for region (\(region))")
        if region.identifier == "safezone" {
            switch state {
            case .inside: delegate?.didEnterSafeZone()
            case .outside: delegate?.didExitSafeZone()
            case .unknown: delegate?.didExitSafeZone()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        muTagStatusManager.handleDidEnterRegionIfValidFor(muTagId: region.identifier, cbEvent: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        muTagStatusManager.handleDidExitRegionIfValidFor(muTagId: region.identifier, cbEvent: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        updateLocation(from: location)
        updateAddress(from: location)
    }
}
