//
//  User.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/11/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class User: Object {
    dynamic var id = ""
    dynamic var email = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
