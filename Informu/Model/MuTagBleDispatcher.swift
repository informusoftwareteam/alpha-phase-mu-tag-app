//
//  MuTagBleDispatcher.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/14/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
//import UIKit
import CoreBluetooth
import PromiseKit
import RealmSwift

enum MuTagBleDispatcherError: Error {
    //case muTagPeripheralNotFound
    case muTagServicesNotFound
    case muTagCharacteristicNotFound
    case peripheralCharacteristicNotFound
    case convertToDataFailed
    case convertToUtf8Failed
    case wrongDataTypeForCharacteristic
    case invalidValueType
    case invalidCharacteristicWrittenResponse
    case invalidCharacteristicReadResponse
    case invalidPeripheralStatusResponse
    case writingCharacteristicsTimedOut
    case readingCharacteristicsTimedOut
    case discoveringCharacteristicsForServicesTimedOut
    case requiredDataNotProvided
}

enum PeripheralStatusWithMuTag {
    case valid(MuTag)
    case unregistered(MuTag)
    case registeredToOtherAccount
    case invalid
    case unknown
}

enum FirmwareMode {
    case beacon
    case ota
}

enum MuTagBleService {
    case deviceInfo
    case config
    case ota
    
    static let allValues = [deviceInfo, config, ota]
}

// Enum needs to return a CBUUID as the rawValue
//
extension MuTagBleService: RawRepresentable {
    typealias RawValue = CBUUID
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case CBUUID(string: "180A"): self = .deviceInfo
        case CBUUID(string: "A173424A-9708-4C4C-AEED-0AB1AF539797"): self = .config
        case CBUUID(string: "1D14D6EE-FD63-4FA1-BFA4-8F47B42119F0"): self = .ota
        default: return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .deviceInfo: return CBUUID(string: "180A")
        case .config: return CBUUID(string: "A173424A-9708-4C4C-AEED-0AB1AF539797")
        case .ota: return CBUUID(string: "1D14D6EE-FD63-4FA1-BFA4-8F47B42119F0")
        }
    }
}

enum MuTagBleCharacteristicDataType {
    case utf8
    case hexString
    case reversedHexString
    case int
    case data
}

enum MuTagBleCharacteristic {
    case manufacturerName
    case firmwareRevision
    case modelNumber
    case systemId
    case batteryLevel
    case uuid
    case major
    case minor
    case txPower
    case authenticate
    case tagColor
    case deepSleep
    case provision
    case advertisingInterval
    case otaControl
    case otaData
    
    static let allValues = [manufacturerName, firmwareRevision, modelNumber, systemId, batteryLevel, uuid, major, minor, txPower, authenticate, tagColor, deepSleep, provision, advertisingInterval, otaControl, otaData]
    
    var sizeInBytes: UInt8 {
        switch self {
        case .manufacturerName: return 12
        case .firmwareRevision: return 3
        case .modelNumber: return 6
        case .systemId: return 6
        case .batteryLevel: return 1
        case .uuid: return 16
        case .major: return 2
        case .minor: return 2
        case .txPower: return 1
        case .authenticate: return 1
        case .tagColor: return 1
        case .deepSleep: return 1
        case .provision: return 1
        case .advertisingInterval: return 1
        case .otaControl: return 1
        case .otaData: return 55
        }
    }
    
    var service: MuTagBleService {
        switch self {
        case .manufacturerName: return .deviceInfo
        case .firmwareRevision: return .deviceInfo
        case .modelNumber: return .deviceInfo
        case .systemId: return .deviceInfo
        case .batteryLevel: return .deviceInfo
        case .uuid: return .config
        case .major: return .config
        case .minor: return .config
        case .txPower: return .config
        case .authenticate: return .config
        case .tagColor: return .config
        case .deepSleep: return .config
        case .provision: return .config
        case .advertisingInterval: return .config
        case .otaControl: return .ota
        case .otaData: return .ota
        }
    }
    
    var dataType: MuTagBleCharacteristicDataType {
        switch self {
        case .manufacturerName: return .hexString
        case .firmwareRevision: return .utf8
        case .modelNumber: return .hexString
        case .systemId: return .reversedHexString
        case .batteryLevel: return .int
        case .uuid: return .hexString
        case .major: return .hexString
        case .minor: return .hexString
        case .txPower: return .int
        case .authenticate: return .hexString
        case .tagColor: return .int
        case .deepSleep: return .int
        case .provision: return .int
        case .advertisingInterval: return .int
        case .otaControl: return .int
        case .otaData: return .data
        }
    }
    
    var writeType: CBCharacteristicWriteType {
        switch self {
        case .manufacturerName: return .withResponse
        case .firmwareRevision: return .withResponse
        case .modelNumber: return .withResponse
        case .systemId: return .withResponse
        case .batteryLevel: return .withResponse
        case .uuid: return .withResponse
        case .major: return .withResponse
        case .minor: return .withResponse
        case .txPower: return .withResponse
        case .authenticate: return .withoutResponse
        case .tagColor: return .withResponse
        case .deepSleep: return .withResponse
        case .provision: return .withResponse
        case .advertisingInterval: return .withResponse
        case .otaControl: return .withResponse
        case .otaData: return .withResponse
        }
    }
}

// Enum needs to return a CBUUID as the rawValue
//
extension MuTagBleCharacteristic: RawRepresentable {
    typealias RawValue = CBUUID
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case CBUUID(string: "2A29"): self = .manufacturerName
        case CBUUID(string: "2A26"): self = .firmwareRevision
        case CBUUID(string: "2A24"): self = .modelNumber
        case CBUUID(string: "2A23"): self = .systemId
        case CBUUID(string: "2A19"): self = .batteryLevel
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB01"): self = .uuid
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB02"): self = .major
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB03"): self = .minor
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB04"): self = .txPower
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB05"): self = .authenticate
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB06"): self = .tagColor
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB07"): self = .deepSleep
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB08"): self = .provision
        case CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB09"): self = .advertisingInterval
        case CBUUID(string: "F7BF3564-FB6D-4E53-88A4-5E37E0326063"): self = .otaControl
        case CBUUID(string: "984227F3-34FC-4045-A5D0-2C581F81A153"): self = .otaData
        default: return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .manufacturerName: return CBUUID(string: "2A29")
        case .firmwareRevision: return CBUUID(string: "2A26")
        case .modelNumber: return CBUUID(string: "2A24")
        case .systemId: return CBUUID(string: "2A23")
        case .batteryLevel: return CBUUID(string: "2A19")
        case .uuid: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB01")
        case .major: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB02")
        case .minor: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB03")
        case .txPower: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB04")
        case .authenticate: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB05")
        case .tagColor: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB06")
        case .deepSleep: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB07")
        case .provision: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB08")
        case .advertisingInterval: return CBUUID(string: "AC9B44EA-AA5E-40F4-888A-C2637573AB09")
        case .otaControl: return CBUUID(string: "F7BF3564-FB6D-4E53-88A4-5E37E0326063")
        case .otaData: return CBUUID(string: "984227F3-34FC-4045-A5D0-2C581F81A153")
        }
    }
}

extension NSNotification.Name {
    static let readCharacteristicFromMuTag = NSNotification.Name("readCharacteristicFromMuTag")
    static let writeCharacteristicToMuTag = NSNotification.Name("writeCharacteristicToMuTag")
    static let peripheralDiscoveryComplete = NSNotification.Name("peripheralDiscoveryComplete")
    static let discoveredCharacteristicsForService = NSNotification.Name("discoveredCharacteristicsForService")
}

extension Data {
    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
    func hexEncodedString() -> String {
        return map { String(format: "%02hhX", $0) }.joined()
    }
}

class MuTagBleDispatcher: NSObject {
    fileprivate let realm = try! Realm()
    fileprivate let notificationCenter = NotificationCenter.default
    fileprivate let authenticationCode = 0x55
    fileprivate let deepSleep = 0x27
    fileprivate let muTagFactory: MuTagFactory
    
    var firmwareMode = FirmwareMode.beacon
    fileprivate var enumToCbCharacteristic = [MuTagBleCharacteristic : CBCharacteristic]()
    var peripheral: CBPeripheral
    fileprivate var peripheralStatusWithMuTag: PeripheralStatusWithMuTag?
    fileprivate var muTagId: String?
    
    //weak var delegate: MuTagBleDispatcherDelegate?
    
    init(peripheral: CBPeripheral) throws {
        self.peripheral = peripheral
        self.muTagFactory = try MuTagFactory()
        super.init()
        peripheral.delegate = self
        discoverAllMuTagServices()
    }
    
    func getPeripheralStatusWithMuTag() -> Promise<PeripheralStatusWithMuTag> {
        return Promise { fulfill, reject in
            //print("Trying to determine if peripheral is a mu tag...")
            
            if let status = peripheralStatusWithMuTag {
                fulfill(status)
            } else {
                // Wait for notification to get mu tag status
                //
                var notificationObserver: NSObjectProtocol?
                notificationObserver = notificationCenter.addObserver(forName: .peripheralDiscoveryComplete, object: nil, queue: nil) { notification in
                    guard let userInfo = notification.userInfo as? [String : Any] else {
                        reject(MuTagBleDispatcherError.invalidPeripheralStatusResponse); return
                    }
                    guard let peripheralIdentifier = userInfo["peripheralIdentifier"] as? UUID else {
                        reject(MuTagBleDispatcherError.invalidPeripheralStatusResponse); return
                    }
                    if peripheralIdentifier != self.peripheral.identifier { return }
                    guard let status = userInfo["peripheralStatusWithMuTag"] as? PeripheralStatusWithMuTag else {
                        reject(MuTagBleDispatcherError.invalidPeripheralStatusResponse); return
                    }
                    self.notificationCenter.removeObserver(notificationObserver!, name: .peripheralDiscoveryComplete, object: nil)
                    fulfill(status); return
                }
            }
        }
    }
    
    func updatePeripheralStatusWithMuTag(to status: PeripheralStatusWithMuTag) {
        peripheralStatusWithMuTag = status
    }
    
    func readValue(from characteristic: MuTagBleCharacteristic) -> Promise<Any?> {
        var promise: Promise<Any?>?
        promise = Promise { fulfill, reject in
            guard let services = peripheral.services else {
                reject(MuTagBleDispatcherError.muTagServicesNotFound); return
            }
            var cbCharacteristic: CBCharacteristic?
            for service in services {
                guard let characteristics = service.characteristics else { continue }
                guard let characteristic = characteristics.filter({ $0.uuid == characteristic.rawValue }).first else { continue }
                cbCharacteristic = characteristic
            }
            guard let peripheralCharacteristic = cbCharacteristic else {
                reject(MuTagBleDispatcherError.muTagCharacteristicNotFound); return
            }
            // Wait for notification to determine if reading of characteristic completed successfully with value or failed
            //
            var notificationObserver: NSObjectProtocol?
            notificationObserver = notificationCenter.addObserver(forName: .readCharacteristicFromMuTag, object: nil, queue: nil) { notification in
                if let error = notification.userInfo?["error"] as? Error {
                    self.notificationCenter.removeObserver(notificationObserver!, name: .readCharacteristicFromMuTag, object: nil)
                    reject(error); return
                }
                guard let cbCharacteristic = notification.userInfo?["characteristic"] as? CBCharacteristic,
                    let muTagCharacteristic = MuTagBleCharacteristic(rawValue: cbCharacteristic.uuid),
                    let peripheralIdentifier = notification.userInfo?["peripheralIdentifier"] as? UUID
                else {
                    self.notificationCenter.removeObserver(notificationObserver!, name: .readCharacteristicFromMuTag, object: nil)
                    reject(MuTagBleDispatcherError.invalidCharacteristicReadResponse); return
                }
                if peripheralIdentifier != self.peripheral.identifier { return }
                if muTagCharacteristic != characteristic { return }
                self.notificationCenter.removeObserver(notificationObserver!, name: .readCharacteristicFromMuTag, object: nil)
                guard let data = cbCharacteristic.value else {
                    reject(MuTagBleDispatcherError.invalidCharacteristicReadResponse); return
                }
                do {
                    // DEBUG
                    print("DEBUG - data \(data as NSData), muTagCharacteristic \(muTagCharacteristic)")
                    
                    if data.isEmpty {
                        fulfill(nil)
                    } else {
                        let value = try self.convertFrom(data: data, for: muTagCharacteristic)
                        fulfill(value)
                    }
                } catch {
                    reject(error)
                }
            }
            after(seconds: 2).then { _ -> () in
                if promise!.isPending {
                    self.notificationCenter.removeObserver(notificationObserver!, name: .writeCharacteristicToMuTag, object: nil)
                    reject(MuTagBleDispatcherError.readingCharacteristicsTimedOut)
                }
            }
            print("Reading value for charactertistic \(characteristic) from peripheral \(peripheral.identifier)...")
            peripheral.readValue(for: peripheralCharacteristic)
        }
        return promise!
    }
    
    func writeToCharacteristics(with values: [(MuTagBleCharacteristic, Any)]) -> Promise<Void> {
        return Promise { fulfill, reject in
            var mutableValues = values
            func writeAllValues() {
                let (characteristic, value) = mutableValues.removeFirst()
                write(to: characteristic, with: value).then { _ -> () in
                    if mutableValues.isEmpty {
                        fulfill(())
                    } else {
                        writeAllValues()
                    }
                }.catch { error in
                    reject(error)
                }
            }
            writeAllValues()
        }
    }
    
    func discoverServicesAndCharactertistics(refresh: Bool = false) -> Promise<Void> {
        return Promise { fulfill, reject in
            if (peripheral.services != nil) && !refresh { fulfill(()); return }
            // Create a log to mark all characteristics for a service that have been discovered
            //
            var characteristicsDiscoveredForServices = [MuTagBleService : Bool]()
            switch firmwareMode {
            case .beacon:
                for service in MuTagBleService.allValues {
                    characteristicsDiscoveredForServices[service] = false
                }
            case .ota:
                characteristicsDiscoveredForServices[MuTagBleService.ota] = false
            }
            // When all required characteristics for all services have been discovered, then the following will return true
            //
            var requiredCharacteristicsForServicesDiscovered: Bool {
                return !Array(characteristicsDiscoveredForServices.values).contains(false)
            }
            // Wait for notification to determine if all characteristics for all services have been discovered
            //
            var notificationObserver: NSObjectProtocol?
            notificationObserver = notificationCenter.addObserver(forName: .discoveredCharacteristicsForService, object: nil, queue: nil, using: { (notification) in
                guard let service = notification.userInfo?["service"] as? CBService,
                    let muTagBleService = MuTagBleService(rawValue: service.uuid),
                    let peripheralIdentifier = notification.userInfo?["peripheralIdentifier"] as? UUID else { return }
                if peripheralIdentifier != self.peripheral.identifier { return }
                if characteristicsDiscoveredForServices.keys.contains(muTagBleService) {
                    characteristicsDiscoveredForServices[muTagBleService] = true
                }
                if requiredCharacteristicsForServicesDiscovered {
                    self.notificationCenter.removeObserver(notificationObserver!, name: .discoveredCharacteristicsForService, object: nil)
                    print("Successfully discovered all characteristics for all services")
                    fulfill(())
                }
            })
            switch firmwareMode {
            case .beacon: discoverAllMuTagServices()
            case .ota: discoverOtaService()
            }
            Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { timer in
                if !requiredCharacteristicsForServicesDiscovered {
                    self.notificationCenter.removeObserver(notificationObserver!, name: .discoveredCharacteristicsForService, object: nil)
                    reject(MuTagBleDispatcherError.discoveringCharacteristicsForServicesTimedOut)
                }
            }
        }
    }
    
    func fulfillOnDisconnect() -> Promise<Void> {
        return Promise { fulfill, _ in
            var notificationObserver: NSObjectProtocol?
            notificationObserver = notificationCenter.addObserver(forName: .peripheralDidDisconnect, object: nil, queue: nil, using: { (notification) in
                guard let peripheralIdentifier = notification.userInfo?["didDisconnect"] as? UUID else { return }
                if peripheralIdentifier == self.peripheral.identifier {
                    self.notificationCenter.removeObserver(notificationObserver!, name: .peripheralDidDisconnect, object: nil)
                    fulfill(())
                }
            })
        }
    }
}

private extension MuTagBleDispatcher {
    func write(to characteristic: MuTagBleCharacteristic, with value: Any) -> Promise<Void> {
        return Promise { fulfill, reject in
            if characteristic.writeType == .withResponse {
                var characteristicWritten = false
                // Wait for notification to determine if writing of characteristics completed successfully or failed
                //
                var notificationObserver: NSObjectProtocol?
                notificationObserver = notificationCenter.addObserver(forName: .writeCharacteristicToMuTag, object: nil, queue: nil, using: { (notification) in
                    if let error = notification.userInfo?["error"] as? Error {
                        self.notificationCenter.removeObserver(notificationObserver!, name: .writeCharacteristicToMuTag, object: nil)
                        reject(error); return
                    }
                    guard let peripheralIdentifier = notification.userInfo?["peripheralIdentifier"] as? UUID else {
                        self.notificationCenter.removeObserver(notificationObserver!, name: .writeCharacteristicToMuTag, object: nil)
                        reject(MuTagBleDispatcherError.invalidCharacteristicWrittenResponse); return
                    }
                    if peripheralIdentifier != self.peripheral.identifier { return }
                    guard let cbCharacteristic = notification.userInfo?["characteristic"] as? CBCharacteristic,
                        let receivedCharacteristic = MuTagBleCharacteristic(rawValue: cbCharacteristic.uuid)
                    else {
                        self.notificationCenter.removeObserver(notificationObserver!, name: .writeCharacteristicToMuTag, object: nil)
                        reject(MuTagBleDispatcherError.invalidCharacteristicWrittenResponse); return
                    }
                    if characteristic == receivedCharacteristic {
                        characteristicWritten = true
                        self.notificationCenter.removeObserver(notificationObserver!, name: .writeCharacteristicToMuTag, object: nil)
                        print("Successfully wrote characteristic \(characteristic) to mμ tag")
                        fulfill(())
                    }
                })
                Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { timer in
                    if !characteristicWritten {
                        self.notificationCenter.removeObserver(notificationObserver!, name: .writeCharacteristicToMuTag, object: nil)
                        reject(MuTagBleDispatcherError.writingCharacteristicsTimedOut)
                    }
                }
            }
            let data = try convertToData(for: characteristic, with: value)
            guard let cbCharacteristic = enumToCbCharacteristic[characteristic] else {
                reject(MuTagBleDispatcherError.peripheralCharacteristicNotFound); return
            }
            // Actually write the characteristics
            //
            peripheral.writeValue(data, for: cbCharacteristic, type: characteristic.writeType)
            if characteristic.writeType == .withoutResponse {
                fulfill(())
            }
        }
    }
    
    func authenticateToMuTag(with service: CBService) {
        //
        // There is no need to verify authentication because the mu tag will disconnect if it fails
        guard let cbCharacteristic = enumToCbCharacteristic[MuTagBleCharacteristic.authenticate] else {
            return
        }
        guard let authenticationData = convertToBigEndianData(from: authenticationCode, withSize: 1) else { return }
        print("Authenticating to mµ tag...")
        peripheral.writeValue(authenticationData, for: cbCharacteristic, type: MuTagBleCharacteristic.authenticate.writeType)
    }
    
    func convertToMuTagData(from hex: String, withSize inBytes: UInt8) -> Data? {
        guard let int = Int(hex, radix: 16) else { return nil }
        
        return convertToMuTagData(from: int, withSize: inBytes)
    }
    
    func convertToMuTagData(from int: Int, withSize inBytes: UInt8) -> Data? {
        guard let bigEndianData = convertToBigEndianData(from: int, withSize: inBytes) else { return nil }
        
        // TODO: Security is temporarily disabled on the firmware
        //
        //return addMuTagSecurity(toData: bigEndianData)
        return bigEndianData
    }
    
    func convertToBigEndianData(from int: Int, withSize inBytes: UInt8) -> Data? {
        let uInt = UInt(int)
        
        switch inBytes {
        case 1: return Data(from: UInt8(uInt))
        case 2: return Data(from: UInt16(uInt).bigEndian)
        case 3, 4: return Data(from: UInt32(uInt).bigEndian)
        case 5...8: return Data(from: UInt64(uInt).bigEndian)
        default: return nil
        }
    }
    
    func swapEndian(for data : Data) -> Data {
        var mdata = data // make a mutable copy
        let count = data.count / MemoryLayout<UInt16>.size
        mdata.withUnsafeMutableBytes { (i16ptr: UnsafeMutablePointer<UInt16>) in
            for i in 0..<count {
                i16ptr[i] =  i16ptr[i].byteSwapped
            }
        }
        return mdata
    }
    
    func addMuTagSecurity(toData: Data) -> Data {
        var data = toData
        data.append(Data(from: UInt16(0xFFFF).bigEndian))
        return data
    }
    
    func convertFrom(data: Data, for characteristic: MuTagBleCharacteristic) throws -> Any {
        switch characteristic.dataType {
        case .utf8:
            guard let value = String(data: data, encoding: .utf8) else {
                throw MuTagBleDispatcherError.convertToUtf8Failed
            }
            print("Retrieved value of '\(value)' for characteristic '\(characteristic)' on peripheral \(self.peripheral.identifier)")
            return value
        case .hexString:
            let value = data.hexEncodedString()
            print("Retrieved value of '\(value)' for characteristic '\(characteristic)' on peripheral \(self.peripheral.identifier)")
            return value
        case .reversedHexString:
            let reversedData = Data(data.reversed())
            let value = reversedData.hexEncodedString()
            print("Retrieved value of '\(value)' for characteristic '\(characteristic)' on peripheral \(self.peripheral.identifier)")
            return value
        case .int:
            let value = data.to(type: Int.self)
            print("Retrieved value of '\(value)' for characteristic '\(characteristic)' on peripheral \(self.peripheral.identifier)")
            return value
        case .data:
            print("Retrieved value of '\(data as NSData)' for characteristic '\(characteristic)' on peripheral \(self.peripheral.identifier)")
            return data
        }
    }
    
    func convertToData(for characteristic: MuTagBleCharacteristic, with value: Any) throws -> Data {
        switch characteristic.dataType {
        case .data:
            guard let data = value as? Data else { throw MuTagBleDispatcherError.wrongDataTypeForCharacteristic }
            return data
        case .hexString:
            guard let stringValue = value as? String else { throw MuTagBleDispatcherError.wrongDataTypeForCharacteristic }
            guard let data = convertToMuTagData(from: stringValue, withSize: characteristic.sizeInBytes) else {
                throw MuTagBleDispatcherError.convertToDataFailed
            }
            return data
        case .reversedHexString:
            // TODO
            throw MuTagBleDispatcherError.convertToDataFailed
        case .int:
            guard let intValue = value as? Int else { throw MuTagBleDispatcherError.wrongDataTypeForCharacteristic }
            guard let data = convertToMuTagData(from: intValue, withSize: characteristic.sizeInBytes) else {
                throw MuTagBleDispatcherError.convertToDataFailed
            }
            return data
        case .utf8:
            // TODO
            throw MuTagBleDispatcherError.convertToDataFailed
        }
    }
    
    func getMuTagId(with service: CBService) -> Promise<String> {
        return Promise { fulfill, reject in
            print("Requesting mµ tag ID from peripheral \(peripheral.identifier)...")
            //var major: String!
            readValue(from: MuTagBleCharacteristic.major).then { value -> Promise<(Any?, String)> in
                let major = value as? String ?? ""
                return self.readValue(from: MuTagBleCharacteristic.minor).then { ($0, major) }
            }.then { (value, major) -> () in
                let minor = value as? String ?? ""
                fulfill(major + minor)
            }.catch { error in
                reject(error)
            }
        }
    }
    
    func discoverPeripheralStatusWithMuTag(from service: CBService) {
        getMuTagId(with: service).then { muTagId -> () in
            if let thisMuTag = MuTag.get(with: muTagId) {
                self.muTagId = muTagId
                self.updateLocalModelFromPeripheral()
                setAndPostStatus(with: .valid(thisMuTag))
            } else {
                if muTagId == "" {
                    self.muTagFactory.create(with: self).then { muTag -> () in
                        self.muTagId = muTag.id
                        self.updateLocalModelFromPeripheral()
                        setAndPostStatus(with: .unregistered(muTag))
                    }.catch { error in
                        print(error)
                        setStatusOnError(with: error)
                    }
                } else {
                    setAndPostStatus(with: .registeredToOtherAccount)
                }
            }
        }.catch { error in
            print(error)
            setStatusOnError(with: error)
        }
        
        func setStatusOnError(with error: Error) {
            if error == MuTagBleDispatcherError.invalidCharacteristicReadResponse {
                setAndPostStatus(with: .invalid)
            } else {
                setAndPostStatus(with: .unknown)
            }
        }
        
        func setAndPostStatus(with status: PeripheralStatusWithMuTag) {
            self.peripheralStatusWithMuTag = status
            self.notificationCenter.post(name: .peripheralDiscoveryComplete, object: nil, userInfo: ["peripheralStatusWithMuTag": status, "peripheralIdentifier": self.peripheral.identifier])
        }
    }
    
    func discoverAllMuTagServices() {
        print("Discovering mµ tag services for peripheral \(peripheral.identifier)...")
        peripheral.discoverServices([
            MuTagBleService.deviceInfo.rawValue,
            MuTagBleService.config.rawValue,
            MuTagBleService.ota.rawValue
            ])
    }
    
    func discoverOtaService() {
        print("Discovering OTA services for peripheral \(peripheral.identifier)...")
        peripheral.discoverServices([MuTagBleService.ota.rawValue])
    }
    
    func updateLocalModelFromPeripheral() {
        guard let cbServices = peripheral.services else { return }
        for cbService in cbServices {
            guard let muTagId = muTagId else { return }
            guard let cbCharacteristics = cbService.characteristics else { return }
            for cbCharacteristic in cbCharacteristics {
                guard let characteristic = MuTagBleCharacteristic(rawValue: cbCharacteristic.uuid) else { continue }
                if (
                    characteristic == .advertisingInterval ||
                        characteristic == .batteryLevel ||
                        characteristic == .firmwareRevision ||
                        characteristic == .txPower ||
                        characteristic == .tagColor
                    ){
                    readValue(from: characteristic).then { valueOfAnyType -> () in
                        DispatchQueue.global(qos: .background).async {
                            autoreleasepool {
                                do {
                                    let realm = try Realm()
                                    let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId)
                                    try realm.write {
                                        switch characteristic {
                                        case .advertisingInterval:
                                            guard let value = valueOfAnyType as? Int else { break }
                                            muTag?.advertisingInterval = value
                                        case .batteryLevel:
                                            guard let value = valueOfAnyType as? Int else { break }
                                            muTag?.batteryLevel = value
                                        case .firmwareRevision:
                                            guard let value = valueOfAnyType as? String else { break }
                                            muTag?.firmwareVersion = value
                                        case .txPower:
                                            guard let value = valueOfAnyType as? Int else { break }
                                            muTag?.txPower = value
                                        case .tagColor:
                                            guard let value = valueOfAnyType as? Int else { break }
                                            muTag?.tagColor = value
                                        default: break
                                        }
                                    }
                                } catch {
                                    print(error)
                                }
                            }
                        }
                        }.catch { error in
                            print(error)
                    }
                }
            }
        }
    }
}

extension MuTagBleDispatcher: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let e = error {
            print(e)
            
            // TODO: Probably need to try discovering services again
        }
        guard let services = peripheral.services else { return }
        print("Successfully discovered services for (\(peripheral.identifier))")
        print("Now attempting to discover characteristics for peripheral \(peripheral.identifier)...")
        enumToCbCharacteristic.removeAll()
        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let e = error {
            print(e)
            
            // TODO: Probably need to try discovering characteristics again
        }
        print("Successfully discovered characteristics for peripheral \(peripheral.identifier) with service \(service.uuid)")
        if let characteristics = service.characteristics {
            for cbCharacteristic in characteristics {
                guard let muTagCharacteristic = MuTagBleCharacteristic(rawValue: cbCharacteristic.uuid) else { continue }
                enumToCbCharacteristic[muTagCharacteristic] = cbCharacteristic
                
                // DEBUG
                print("DEBUG - muTagCharacteristic \(muTagCharacteristic)")
            }
        }
        notificationCenter.post(name: .discoveredCharacteristicsForService, object: nil, userInfo: ["service": service, "peripheralIdentifier": self.peripheral.identifier])
        if service.uuid == MuTagBleService.config.rawValue {
            print("Found mμ tag config service for peripheral \(peripheral.identifier)!")
            if firmwareMode == .beacon {
                authenticateToMuTag(with: service)
            }
            if peripheralStatusWithMuTag == nil {
                discoverPeripheralStatusWithMuTag(from: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if let e = error {
            notificationCenter.post(name: .writeCharacteristicToMuTag, object: nil, userInfo: ["error": e, "peripheralIdentifier": self.peripheral.identifier])
        } else {
            notificationCenter.post(name: .writeCharacteristicToMuTag, object: nil, userInfo: ["characteristic": characteristic, "peripheralIdentifier": self.peripheral.identifier])
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let e = error {
            notificationCenter.post(name: .readCharacteristicFromMuTag, object: nil, userInfo: ["error": e, "peripheralIdentifier": self.peripheral.identifier])
        } else {
            notificationCenter.post(name: .readCharacteristicFromMuTag, object: nil, userInfo: ["characteristic": characteristic, "peripheralIdentifier": self.peripheral.identifier])
        }
    }
    
    func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        print("Name has updated for peripheral \(peripheral.identifier) to \(String(describing: peripheral.name))")
    }
}
