//
//  Firebase.swift
//  Informu
//
//  Created by Thomas Dickerson on 10/4/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Firebase
import RealmSwift
import PromiseKit

extension NSNotification.Name {
    static let realmNotificationSetupsComplete = NSNotification.Name("realmNotificationSetupsComplete")
}

class FirebaseReactiveService {
    // MARK: Singleton shared instance
    //
    static let shared: FirebaseReactiveService? = {
        do {
            return try FirebaseReactiveService()
        } catch {
            print(error)
            return nil
        }
    }()
    
    fileprivate let firebase = Database.database().reference()
    fileprivate let notificationCenter = NotificationCenter.default
    fileprivate let session: Session
    
    fileprivate var notificationTokens = [String : NotificationToken]()
    
    init() throws {
        self.session = try Session.shared()
        prepareRealmNotificationForSession()
        prepareRealmNotificationForAccount()
    }
}

private extension FirebaseReactiveService {
    func prepareRealmNotificationForSession() {
        print("FirebaseReactiveService creating Realm notification token for session")
        let token = session.observe { [weak self] change in
            switch change {
            case .change(let properties):
                let filtered = properties.filter { $0.name == "account"}
                
                for _ in filtered {
                    self?.prepareRealmNotificationForAccount()
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("Warning: Session was deleted. This should never happen!")
                return
            }
        }
        notificationTokens["session"] = token
    }
    
    func prepareRealmNotificationForAccount() {
        guard let account = session.account else { return }
        print("FirebaseReactiveService creating Realm notification token for account")
        let accountId = account.id
        let token = account.observe { [weak self] change in
            switch change {
            case .change(let properties):
                let filtered = properties.filter { $0.name == "safeZone"}
                for property in filtered {
                    self?.firebase.child("accounts/\(account.id)/safe_zone").setValue(property.newValue)
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("FirebaseReactiveService.prepareRealmNotification() - The Realm 'account' object \(accountId) was deleted.")
                
                self?.notificationTokens["account"] = nil
            }
        }
        notificationTokens["account"] = token
    }
}
