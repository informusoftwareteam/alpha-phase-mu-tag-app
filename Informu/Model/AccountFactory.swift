//
//  AccountFactory.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/19/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import PromiseKit
import Firebase

enum AccountFactoryError: Error {
    case notCommittedToDatabase
    case accountLimitReached
}

extension AccountFactoryError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notCommittedToDatabase: return NSLocalizedString("There was a problem comitting the account to the database", comment: "")
        case .accountLimitReached: return NSLocalizedString("The number of user accounts has reached its maximum", comment: "")
        }
    }
}

class AccountFactory {
    func create(with user: User) -> Promise<Account> {
        return Promise { fulfill, reject in
            createAccountId().then { accountId in
                fulfill(Account(value: ["id": accountId, "user": user]))
            }.catch { error in
                reject(error)
            }
        }
    }
}

private extension AccountFactory {
    func createAccountId() -> Promise<String> {
        return Promise { fulfill, reject in
            let ref = Database.database().reference()
            
            ref.child("last_account_id").runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
                guard let lastAccountId = currentData.value as? String else {
                    currentData.value = "0001"
                    return TransactionResult.success(withValue: currentData)
                }
                
                if lastAccountId == "FFFF" {
                    reject(AccountFactoryError.accountLimitReached)
                    return TransactionResult.abort()
                } else {
                    let incrimentAccountId = Int(lastAccountId, radix: 16)! + 1
                    currentData.value = String(format:"%04X", incrimentAccountId)
                    
                    return TransactionResult.success(withValue: currentData)
                }
            }) { (error, committed, snapshot) in
                if let e = error {
                    reject(e)
                }
                
                if !committed {
                    reject(AccountFactoryError.notCommittedToDatabase)
                }
                
                fulfill(snapshot!.value as! String)
            }
        }
    }
}
