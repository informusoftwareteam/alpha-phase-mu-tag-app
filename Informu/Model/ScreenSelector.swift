//
//  ScreenSelector.swift
//  Scrip
//
//  Created by Tom Daniel D. on 4/28/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import Foundation
import UIKit

enum MainScreen: String {
    case Home, Settings
}

struct ScreenSelector {
    let screens: Dictionary<MainScreen, ActiveController>
    
    init(screens: Dictionary<MainScreen, ActiveController>) {
        self.screens = screens
    }
    
    func loadController(screen: MainScreen) -> ActiveController? {
        return screens[screen]
        
        /*switch screen {
        case MainScreen.Home:
            return HomeController(state: state)
        case MainScreen.Settings:
            return SettingsController()
        }*/
    }
}
