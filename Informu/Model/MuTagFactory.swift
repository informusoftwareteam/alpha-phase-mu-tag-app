//
//  MuTagFactory.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/5/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import PromiseKit

enum MuTagFactoryError: Error {
    case accountCouldNotBeFound
}

class MuTagFactory {
    fileprivate let session: Session
    
    init() throws {
        self.session = try Session.shared()
    }
    
    func create(with dispatcher: MuTagBleDispatcher) -> Promise<MuTag> {
        return Promise { fulfill, reject in
            guard let accountId = session.account?.id else { throw MuTagFactoryError.accountCouldNotBeFound }
            var firmwareVersion: Any!
            dispatcher.readValue(from: .firmwareRevision).then { revision -> Promise<Any?> in
                firmwareVersion = revision
                return dispatcher.readValue(from: .systemId)
            }.then { systemId -> () in
                let major = accountId
                let minor = try self.generateMuTagMinor()
                let properties = [
                    "id": major + minor,
                    "macAddress": systemId ?? "",
                    "firmwareVersion": firmwareVersion,
                    "major": major,
                    "minor": minor,
                    ] as [String : Any]
                fulfill(MuTag(value: properties))
            }.catch { error in
                reject(error)
            }
        }
    }
}

private extension MuTagFactory {
    func generateMuTagMinor() throws -> String {
        guard let muTags = session.account?.muTags else { throw MuTagFactoryError.accountCouldNotBeFound }
        let random = arc4random_uniform(65536)
        let muTagId = String(format:"%04X", random)
        
        // Ensure that there is no collision with mu tag ids already registered to account
        //
        //guard let muTags = account?.muTags else { return nil }
        for muTag in muTags {
            if muTag.minor == muTagId {
                return try generateMuTagMinor()
            }
        }
        
        return muTagId
    }
}
