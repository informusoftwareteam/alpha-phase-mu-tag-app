//
//  MuTag.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/2/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import PromiseKit
//import CoreBluetooth
import CoreLocation

enum MuTagError: Error {
    case requiredMuTagPropertiesNotSet
}

enum MuTagAdvertisingInterval: Int {
    case adv1285 = 1
    case adv1022
    case adv852
    case adv760
    case adv546
    case adv417
    case adv318
    case adv211
    case adv152
}

enum MuTagTxPower: Int {
    case paradise = 0x01    // +6dBm
    case home               // 0dBm
    case wondering          // -8dBm
    case narrows            // -15dBm
    case thiefMagnet        // -20dBm
}

enum MuTagColor: Int {
    case charcoal = 1
    case cloud
    case indiegogo
    case kickstarter
    case mu
    case scarlet
    case sky
    case smoke
    
    static let allValues = [charcoal, cloud, indiegogo, kickstarter, mu, scarlet, sky, smoke]
    
    var stringValue: String {
        switch self {
        case .charcoal: return "charcoal"
        case .cloud: return "cloud"
        case .indiegogo: return "indiegogo"
        case .kickstarter: return "kickstarter"
        case .mu: return "mu"
        case .scarlet: return "scarlet"
        case .sky: return "sky"
        case .smoke: return "smoke"
        }
    }
    
    var colorValue: UIColor {
        switch self {
        case .charcoal: return UIColor.init(red: 92/255, green: 92/255, blue: 94/255, alpha: 1)
        case .cloud: return UIColor.white
        case .indiegogo: return UIColor.init(red: 216/255, green: 119/255, blue: 176/255, alpha: 1)
        case .kickstarter: return UIColor.init(red: 52/255, green: 223/255, blue: 124/255, alpha: 1)
        case .mu: return UIColor.init(red: 227/255, green: 152/255, blue: 53/255, alpha: 1)
        case .scarlet: return UIColor.init(red: 221/255, green: 88/255, blue: 76/255, alpha: 1)
        case .sky: return UIColor.init(red: 130/255, green: 190/255, blue: 237/255, alpha: 1)
        case .smoke: return UIColor.init(red: 161/255, green: 160/255, blue: 163/255, alpha: 1)
        }
    }
}

@objcMembers
class MuTag: Object {
    fileprivate let batteryService = MuTagBatteryService()
    fileprivate let bleSessionManager = MuTagBleSessionManager.shared
    fileprivate let muTagProximityUuid = UUID(uuidString: "DE7EC7ED-1055-B055-C0DE-DEFEA7EDFA7E")!
    fileprivate let provisionCode = 0x27
    fileprivate let unprovisionCode = 0x45
    
    var beaconRegion: CLBeaconRegion? {
        if id.isEmpty || major.isEmpty || minor.isEmpty {
            return nil
        }
        
        guard let major = UInt16(major, radix: 16) else { return nil }
        guard let minor = UInt16(minor, radix: 16) else { return nil }
        return CLBeaconRegion(proximityUUID: muTagProximityUuid, major: major, minor: minor, identifier: id)
    }
    
    var name: String {
        return major + minor
    }

    var clProximity: CLProximity {
        return CLProximity(rawValue: proximity) ?? .unknown
    }
    
    var proximityString: String {
        switch clProximity {
        case .unknown: return "..."
        case .far: return "far"
        case .near: return "close"
        case .immediate: return "safe"
        }
    }
    
    var muTagColor: String {
        if let color = MuTagColor(rawValue: tagColor) {
            return color.stringValue
        } else {
            return "not set"
        }
    }
    
    // Realm properties
    //
    dynamic var id = ""
    dynamic var macAddress = ""
    dynamic var firmwareVersion = ""
    dynamic var major = ""
    dynamic var minor = ""
    dynamic var attachedTo = ""
    dynamic var expensive = false
    dynamic var irreplaceable = false
    dynamic var tagColor = 0
    dynamic var dateAdded = ""
    dynamic var txPower = 0
    dynamic var recentLocation = ""
    dynamic var recentLocationName = ""
    dynamic var inSafeZone = ""
    dynamic var lastSeen = ""
    dynamic var lastReentry = ""
    dynamic var unsafe = false
    dynamic var proximity = 0
    dynamic var accuracy = 0.0
    dynamic var rssi = 0
    dynamic var batteryLevel = 0
    dynamic var detectInterval = ""
    dynamic var advertisingInterval = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // For efficiency ensure that there was a significant change
    //
    func updateStateForSignificantChange(from beacon: CLBeacon) {
        if isChangeSignificant(from: beacon) {
            let muTagRef = ThreadSafeReference(to: self)
            DispatchQueue.global(qos: .background).async {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        guard let muTag = realm.resolve(muTagRef) else { return }
                        try realm.write {
                            muTag.proximity = beacon.proximity.rawValue
                            muTag.accuracy = beacon.accuracy
                            muTag.rssi = beacon.rssi
                            muTag.lastSeen = ISO8601DateFormatter().string(from: Date())
                            if muTag.unsafe {
                                muTag.unsafe = false
                            }
                        }
                    } catch {
                        print(error)
                    }
                }
            }
        }
    }
    
    func updateBatteryLevelStatus() -> Promise<Int> {
        return Promise { fulfill, reject in
            print("Reading and updating battery level...")
            let muTagId = self.id
            batteryService.readLevelFrom(muTagId: muTagId).then { batteryLevel -> () in
                //let muTagRef = ThreadSafeReference(to: self)
                DispatchQueue.global(qos: .background).async {
                    autoreleasepool {
                        do {
                            let realm = try Realm()
                            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
                            try realm.write {
                                muTag.batteryLevel = batteryLevel
                            }
                            fulfill(batteryLevel)
                        } catch {
                            reject(error)
                        }
                    }
                }
            }.catch { error in
                reject(error)
            }
        }
    }
    
    func provision() -> Promise<Void> {
        return Promise { fulfill, reject in
            print("Attempting to write provisioning data to mμ tag...")
            // Verify that the required properties have been provided within the MuTag object
            //
            if major == "" || minor == "" || attachedTo == "" || tagColor == 0 {
                reject(MuTagError.requiredMuTagPropertiesNotSet); return
            }
            bleSessionManager.connect(to: self).then { dispatcher -> Promise<Void> in
                let values: [(MuTagBleCharacteristic, Any)] = [
                    (.major, self.major),
                    (.minor, self.minor),
                    (.tagColor, self.tagColor),
                    (.advertisingInterval, self.advertisingInterval),
                    (.txPower, self.txPower),
                    (.provision, self.provisionCode)
                ]
                return dispatcher.writeToCharacteristics(with: values)
            }.then { _ -> () in
                self.bleSessionManager.disconnectFromMuTagWith(id: self.id)
                fulfill(())
            }.catch { error in
                reject(error)
            }
        }
    }
    
    func unprovision() -> Promise<Void> {
        return Promise { fulfill, reject in
            print("Attempting to unprovision mµ tag device...")
            bleSessionManager.connect(to: self).then { dispatcher -> () in
                // TODO: This connection will automatically disconnect after writing '0x45' to the provision characteristic.
                //       Need to figure out a way for this promise to fulfill in this scenerio. We might want to adjust firmware behavior.
                //
                _ =  dispatcher.writeToCharacteristics(with: [(.provision, self.unprovisionCode)])
                fulfill(())
            }.catch { error in
                reject(error)
            }
        }
    }
    
    func updateStatusToProvisioned() {
        guard let dispatcher = bleSessionManager.getDispatcherFor(muTagId: self.id) else { return }
        dispatcher.updatePeripheralStatusWithMuTag(to: .valid(self))
        print("Mu tag \(id) status updated to 'provisioned'")
    }
    
    func updateAdvertisingInterval(to value: Int) -> Promise<Void> {
        return Promise { fulfill, reject in
            bleSessionManager.connect(to: self).then { dispatcher -> Promise<Void> in
                let values: [(MuTagBleCharacteristic, Any)] = [
                    (.advertisingInterval, value)
                ]
                
                // DEBUG
                print("DEBUG - attempting to update muTag \(self.id)...")
                
                return dispatcher.writeToCharacteristics(with: values)
            }.then { _ -> () in
                self.bleSessionManager.disconnectFromMuTagWith(id: self.id)
                let muTagId = self.id
                DispatchQueue.global(qos: .background).async {
                    autoreleasepool {
                        do {
                            let realm = try Realm()
                            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
                            try realm.write {
                                muTag.advertisingInterval = value
                            }
                            fulfill(())
                        } catch {
                            reject(error)
                        }
                    }
                }
            }.catch { error in
                reject(error)
            }
        }
    }
    
    func updateTxPower(to value: Int) -> Promise<Void> {
        return Promise { fulfill, reject in
            bleSessionManager.connect(to: self).then { dispatcher -> Promise<Void> in
                let values: [(MuTagBleCharacteristic, Any)] = [
                    (.txPower, value)
                ]
                return dispatcher.writeToCharacteristics(with: values)
            }.then { _ -> () in
                self.bleSessionManager.disconnectFromMuTagWith(id: self.id)
                let muTagId = self.id
                DispatchQueue.global(qos: .background).async {
                    autoreleasepool {
                        do {
                            let realm = try Realm()
                            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
                            try realm.write {
                                muTag.txPower = value
                            }
                            fulfill(())
                        } catch {
                            reject(error)
                        }
                    }
                }
            }.catch { error in
                reject(error)
            }
        }
    }
}

extension MuTag {
    static func get(from region: CLRegion) -> MuTag? {
        guard let muTag = try? Realm().object(ofType: MuTag.self, forPrimaryKey: region.identifier) else { return nil }
        return muTag
    }
    
    static func get(with id: String) -> MuTag? {
        guard let muTag = try? Realm().object(ofType: MuTag.self, forPrimaryKey: id) else { return nil }
        return muTag
    }
}

private extension MuTag {
    func isChangeSignificant(from beacon: CLBeacon) -> Bool {
        let proximityIsNotUnknown = beacon.proximity != .unknown
        let proximityHasChanged = proximity != beacon.proximity.rawValue
        let rssiHasChanged = rssi != beacon.rssi
        let significantChangeInAccuracy = round(accuracy*10)/10 != round(beacon.accuracy*10)/10
        return
             proximityIsNotUnknown &&
                (
                    proximityHasChanged ||
                    rssiHasChanged ||
                    significantChangeInAccuracy
                )
    }
}
