//
//  InteractiveFeedback.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/22/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit
import Firebase

class InteractiveFeedback: NSObject {
    // Singleton shared instance
    //
    static let shared = InteractiveFeedback()
    
    fileprivate let notificationCenter = UNUserNotificationCenter.current()
    fileprivate let ref = Database.database().reference()
    
    // Prevent init of this singleton with 'private'
    //
    private override init() {
        super.init()
        notificationCenter.delegate = self
    }
    
    func alertForFeedbackIfNeeded() {
        if UIApplication.shared.applicationIconBadgeNumber == 0 {
            return
        }
        let random = arc4random_uniform(2)
        switch random {
        case 0: presentFeedbackAlert()
        case 1: return
        default: return   // By nature of 'random', default switch option should never even be called
        }
    }
}

private extension InteractiveFeedback {
    func handleUnwantedAlertFor(identifier: String) {
        let splitIdentifier = identifier.components(separatedBy: "|")
        let muTagId = splitIdentifier[0]
        let timestamp = splitIdentifier[1]
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        ref.child("mu_tag_data_streams/\(muTagId)/\(timestamp)/useful").setValue(2)
    }
    
    func handleUsefulAlertFor(identifier: String) {
        let splitIdentifier = identifier.components(separatedBy: "|")
        let muTagId = splitIdentifier[0]
        let timestamp = splitIdentifier[1]
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        ref.child("mu_tag_data_streams/\(muTagId)/\(timestamp)/useful").setValue(1)
    }
    
    func presentFeedbackAlert() {
        let title = "Notification feedback needed"
        let message = "We have noticed that you have not responded to all of your mµ tag notifications. In order for us to provide you with the best experience please select either 'I didn't want this' or 'This was helpful!' for every notification."
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let updateAction = UIAlertAction(title: "Okay, I will help out!", style: .default)
        let cancelAction = UIAlertAction(title: "Just clear the Informµ app badge numbers", style: .cancel) { _ in
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        alertController.addAction(updateAction)
        alertController.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}

extension InteractiveFeedback: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let notificationIdentifier = response.notification.request.identifier
        guard let actionIdentifier = UNNotificationActionIdentifier(rawValue: response.actionIdentifier) else { return }
        switch actionIdentifier {
        case .unwanted:
            handleUnwantedAlertFor(identifier: notificationIdentifier)
        case .useful:
            handleUsefulAlertFor(identifier: notificationIdentifier)
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
}
