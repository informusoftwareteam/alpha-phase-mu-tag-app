//
//  RequirementsService.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/6/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UserNotifications
import CoreBluetooth
import CoreLocation
import UIKit
import PromiseKit

@available(iOS 10.0, *)
class RequirementsService {
    let notificationCenter = UNUserNotificationCenter.current()
    let notification = Notification()
    let locationManager = CLLocationManager()
    let criticalTitle = "Warning: App permissions or settings are incorrect"
    
    func verifyAllRequirements() -> Promise<Void> {
        return Promise { fulfill, _ in
            //
            // Bluetooth not checked here because it is controlled by a delegate in MuTagBleSessionManager
            notificationCenter.requestAuthorization(options: [.alert, .sound, .badge, .carPlay]) { (granted, error) in
                if let e = error {
                    self.alert(title: "Error", message: e.localizedDescription).then { _ -> Promise<Void> in
                        return verifyRemainingRequirements()
                    }.always {
                        fulfill(())
                    }
                }
                if !granted {
                    let message = "This app cannot function properly until notifications are authorized."
                    self.alert(title: self.criticalTitle, message: message).then { _ -> Promise<Void> in
                        return verifyRemainingRequirements()
                    }.always {
                        fulfill(())
                    }
                } else {
                    verifyRemainingRequirements().always {
                        fulfill(())
                    }
                }
            }
            func verifyRemainingRequirements() -> Promise<Void> {
                return Promise { fulfill, _ in
                    checkLocationPermissions(from: CLLocationManager.authorizationStatus())
                    notificationCenter.getNotificationSettings { settings in
                        self.checkNotificationSettings(from: settings).always {
                            fulfill(())
                        }
                    }
                }
            }
        }
    }
    
    func checkBluetoothSettings(from central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            notification.clearBluetoothNotifications()
        case .poweredOff:
            notification.notifyBluetoothOff()
            _ = alert(title: criticalTitle, message: "Bluetooth must be powered on for this app to function properly. Please enable Bluetooth from your phone's settings.")
        case .unsupported: _ = alert(title: criticalTitle, message: "This app WILL NOT function properly because Bluetooth Low Energy is not support by this device.")
        case .unauthorized:
            notification.notifyBluetoothDenied()
            _ = alert(title: criticalTitle, message: "You must authorize this app to use Bluetooth for it to function properly. Please allow from your phone's settings.")
        case .resetting: print("The BLE Manager is resetting; a state update is pending.")
        case .unknown: print("The state of the BLE Manager is unknown.")
        }
    }
    
    func checkLocationPermissions(from status: CLAuthorizationStatus) {
        if status != .authorizedAlways {
            locationManager.requestAlwaysAuthorization()
        }
        /*switch status {
        case .authorizedAlways: return
        case .authorizedWhenInUse: alert(with: "This app WILL NOT function properly until you select 'Always Allow' location tracking. Mu tags use low-energy Bluetooth location services to communicate with your phone and log their location.")
        case .denied: alert(with: "This app WILL NOT function properly until you select 'Always Allow' location tracking. Mu tags use low-energy Bluetooth location services to communicate with your phone and log their location.")
        case .notDetermined: alert(with: "This app WILL NOT function properly until you select 'Always Allow' location tracking. Mu tags use low-energy Bluetooth location services to communicate with your phone and log their location.")
        case .restricted: alert(with: "This app WILL NOT function properly until you select 'Always Allow' location tracking. Mu tags use low-energy Bluetooth location services to communicate with your phone and log their location.")
        }*/
    }
}

private extension RequirementsService {
    func checkNotificationSettings(from settings: UNNotificationSettings) -> Promise<Void> {
        var invalidSettings = Set<String>()
        var unsupportedSettings = Set<String>()
        switch settings.alertSetting {
        case .enabled: break
        case .disabled: invalidSettings.insert("Alerts")
        case .notSupported: unsupportedSettings.insert("Alerts")
        }
        switch settings.alertStyle {
        case .alert: break
        case .banner: break
        case .none: invalidSettings.insert("Alert style")
        }
        switch settings.authorizationStatus {
        case .authorized: break
        case .denied: invalidSettings.insert("Authorize notifications")
        case .notDetermined: invalidSettings.insert("Authorize notifications")
        }
        switch settings.badgeSetting {
        case .enabled: break
        case .disabled: invalidSettings.insert("Badges")
        case .notSupported: unsupportedSettings.insert("Badges")
        }
        switch settings.carPlaySetting {
        case .enabled: break
        case .disabled: break
        case .notSupported: break
        }
        switch settings.lockScreenSetting {
        case .enabled: break
        case .disabled: invalidSettings.insert("Lock screen notifications")
        case .notSupported: unsupportedSettings.insert("Lock screen notifications")
        }
        switch settings.notificationCenterSetting {
        case .enabled: break
        case .disabled: invalidSettings.insert("Notification center")
        case .notSupported: unsupportedSettings.insert("Notification center")
        }
        switch settings.soundSetting {
        case .enabled: break
        case .disabled: invalidSettings.insert("Sounds")
        case .notSupported: unsupportedSettings.insert("Sounds")
        }
        if #available(iOS 11.0, *) {
            switch settings.showPreviewsSetting {
            case .always: break
            case .never: invalidSettings.insert("Notification preview")
            case .whenAuthenticated: break
            }
        }
        if !invalidSettings.isEmpty {
            var message = "This app cannot function properly unless the following settings are enabled:\n"
            for string in invalidSettings {
                message.append(" - \(string)\n")
            }
            return alert(title: criticalTitle, message: message)
        }
        if !unsupportedSettings.isEmpty {
            var message = "This app may not function properly because the following features are currently unsupported:\n"
            for string in unsupportedSettings {
                message.append(" - \(string)\n")
            }
            return alert(message: message)
        }
        return Promise(value: ())
    }
    
    func alertForBluetoothSettings() {
        
    }
    
    func alertForLocationPermissions() {
        
    }
    
    func alertForNotificationSettings() {
        
    }
    
    func alert(title: String? = "Attention", message: String) -> Promise<Void> {
        return Promise { fulfill, _ in
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "Okay", style: .default) { _ in
                fulfill(())
            }
            alertController.addAction(okayAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
}
