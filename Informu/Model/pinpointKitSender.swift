//
//  pinpointKitSender.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/13/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import PinpointKit
import SlackKit

class PinpointKitSender: Sender {
    fileprivate let slackBot = SlackKit()
    
    weak var delegate: SenderDelegate?
    
    init() {
        prepareSlackBot()
    }
    
    func send(_ feedback: Feedback, from viewController: UIViewController?) {
        print("Attempting to send feedback...")
        if let data = UIImageJPEGRepresentation(feedback.screenshot.preferredImage, 0.1) {
            let session = try? Session.shared()
            var comment = ""
            comment += "User ID: \(session?.account?.user?.id ?? "not found")\n"
            comment += "Email: \(session?.account?.user?.email ?? "not found")\n"
            comment += "Account ID: \(session?.account?.id ?? "not found")\n"
            comment += "From location:\n"
            comment += "\(session?.mostRecentLocation ?? "not found")\n"
            comment += "\(session?.mostRecentLocationName ?? "not found")"
            slackBot.webAPI?.uploadFile(
                file: data,
                filename: "app-screenshot",
                filetype: "jpg",
                title: "Some delicious feedback",
                initialComment: comment,
                channels: ["G7ZL3E35Y"],
            success: { _ in
                print("Feedback successfully submitted!")
            },
            failure: { error in
                print(error)
            })
        } else {
            
        }
        
        
        
        viewController?.dismiss(animated: true, completion: nil)
    }
}

private extension PinpointKitSender {
    func prepareSlackBot() {
        print("Setting up the Slack bot...")
        slackBot.addWebAPIAccessWithToken("xoxb-271677718354-yVdyPdIDR2cgrlETahkPfD8J")
        slackBot.webAPI?.authenticationTest(success: { (stringOne, stringTwo) in
            guard let stringOne = stringOne else { return }
            print(stringOne)
            guard let stringTwo = stringTwo else { return }
            print(stringTwo)
        }, failure: { (error) in
            print(error)
        })
    }
}
