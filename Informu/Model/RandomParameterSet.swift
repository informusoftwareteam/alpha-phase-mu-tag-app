//
//  RandomParameterSet.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/24/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation

enum ParameterSet: String {
    case one = "1022ms/0dB"
    case two = "1022ms/+6dB"
    case three = "546ms/0dB"
    case four = "546ms/+6dB"
    
    static let allValues = [one, two, three, four]
    
    var advertisingInterval: Int {
        switch self {
        case .one: return 2
        case .two: return 2
        case .three: return 5
        case .four: return 5
        }
    }
    
    var txPower: Int {
        switch self {
        case .one: return 2
        case .two: return 1
        case .three: return 2
        case .four: return 1
        }
    }
}

struct RandomParameterSet {
    var parameters: ParameterSet!
    var advertisingInterval: Int!
    var txPower: Int!
    
    init() {
        self.parameters = selectRandomParameterSet()
        self.advertisingInterval = self.parameters.advertisingInterval
        self.txPower = self.parameters.txPower
    }
}

private extension RandomParameterSet {
    func selectRandomParameterSet() -> ParameterSet {
        let random = arc4random_uniform(4)
        switch random {
        case 0: return .one
        case 1: return .two
        case 2: return .three
        case 3: return .four
        default: return .one    // By nature of 'random', default switch option should never even be called
        }
    }
}
