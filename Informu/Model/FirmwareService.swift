//
//  FirmwareService.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/14/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Firebase
import PromiseKit
import RealmSwift

enum FirmwareServiceError: Error {
    case muTagNotFoundForId
    case updateCancelledByUser
    case firmwareDataNotFound
    case receivedInvalidValue
    case noMuTagsAvailableForUpdate
    case newestFirmwareVersionUnknown
}

class FirmwareService {
    // Singleton shared instance
    //
    static let shared = FirmwareService()
    
    fileprivate let databaseRef = Database.database().reference()
    fileprivate let storageRef = Storage.storage().reference()
    fileprivate let bleSessionManager = MuTagBleSessionManager.shared
    fileprivate let realm = try! Realm()
    
    //fileprivate var initialValue = true
    fileprivate var firmwareRefHandle: UInt?
    fileprivate var newestFirmwareVersion: String?
    fileprivate var downloadedFirmware: Data?
    fileprivate var downloadedFirmwareVersion: String?
    fileprivate var outdatedMuTagsInRange: Results<MuTag>?
    fileprivate var progressViewC: ProgressViewC?
    
    // Prevent init of this singleton with 'private'
    //
    private init() {
        prepareDatabaseListener()
    }
    
    func updateIfNeeded() -> Promise<Void> {
        return Promise { fulfill, reject in
            updateFirmwareVersionFromPeripherals().then { _ -> Promise<Results<MuTag>> in
                guard let outdatedMuTagsInRange = self.outdatedMuTagsInRange else { throw FirmwareServiceError.noMuTagsAvailableForUpdate }
                if outdatedMuTagsInRange.isEmpty { throw FirmwareServiceError.noMuTagsAvailableForUpdate }
                guard let version = self.newestFirmwareVersion else { throw FirmwareServiceError.newestFirmwareVersionUnknown }
                return self.downloadFirmware(version: version).then { outdatedMuTagsInRange }
            }.then { outdatedMuTagsInRange -> () in
                var mutableMuTagCollection = Set(outdatedMuTagsInRange)
                func updateMuTags() {
                    if mutableMuTagCollection.isEmpty { return }
                    let muTagId = mutableMuTagCollection.removeFirst().id
                    self.attemptUpdate(forMuTagId: muTagId).then {
                        updateMuTags()
                        }.catch { error in
                            print(error)
                            updateMuTags()
                    }
                }
                updateMuTags()
                fulfill(())
            }.catch { error in
                reject(error)
            }
        }
    }
}

private extension FirmwareService {
    func updateFirmwareVersionFromPeripherals() -> Promise<Void> {
        return Promise { fulfill, _ in
            var muTagsInRange = Set(realm.objects(MuTag.self).filter("unsafe = false"))
            func readAndUpdateFirmwareVersion() {
                if muTagsInRange.isEmpty {
                    fulfill(()); return
                }
                let muTag = muTagsInRange.removeFirst()
                bleSessionManager.connect(to: muTag).then { dispatcher -> Promise<Any?> in
                    return dispatcher.readValue(from: .firmwareRevision)
                }.then { value -> () in
                    self.bleSessionManager.disconnectFromMuTagWith(id: muTag.id)
                    guard let version = value as? String else { throw FirmwareServiceError.receivedInvalidValue }
                    let muTagRef = ThreadSafeReference(to: muTag)
                    DispatchQueue.global(qos: .background).async {
                        autoreleasepool {
                            do {
                                let realm = try Realm()
                                guard let muTag = realm.resolve(muTagRef) else { return }
                                try realm.write {
                                    muTag.firmwareVersion = version
                                }
                            } catch {
                                print(error)
                            }
                        }
                    }
                    readAndUpdateFirmwareVersion()
                }.catch { error in
                    print(error)
                    readAndUpdateFirmwareVersion()
                }
            }
            readAndUpdateFirmwareVersion()
        }
    }
    
    func prepareDatabaseListener() {
        let firmwareRef = databaseRef.child("firmware_version")
        firmwareRefHandle = firmwareRef.observe(.value) { snapshot in
            guard let version = snapshot.value as? String else { return }
            self.newestFirmwareVersion = version
            print("Deployed firmware version should be \(version)")
            self.outdatedMuTagsInRange = self.realm.objects(MuTag.self).filter("unsafe = false AND firmwareVersion != %@", version).sorted(byKeyPath: "rssi", ascending: false)
            self.updateIfNeeded()
        }
    }
    
    func downloadFirmware(version: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            if downloadedFirmwareVersion == version {
                print("Firmware version \(version) already downloaded")
                fulfill(()); return
            }
            print("Attempting to download new firmware \(version)...")
            let fileRef = storageRef.child("firmware/\(version).gbl")
            fileRef.getData(maxSize: 1 * 1024 * 1024, completion: { data, error in
                if let error = error {
                    reject(error); return
                }
                print("Successfully downloaded new firmware \(version)")
                self.downloadedFirmware = data
                self.downloadedFirmwareVersion = version
                fulfill(())
            })
        }
    }
    
    func flashMuTagWith(id: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            guard let data = downloadedFirmware else { throw FirmwareServiceError.firmwareDataNotFound }
            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: id) else {
                throw FirmwareServiceError.muTagNotFoundForId
            }
            showFlashProgress(forMuTagAttachedTo: muTag.attachedTo)
            updateFlashProgress(to: 1) {
                self.updateFlashProgress(to: 10, speed: 4.0)
            }
            //var muTagDispatcher: MuTagBleDispatcher?
            bleSessionManager.connect(to: muTag).then { dispatcher -> Promise<MuTagBleDispatcher> in
                //muTagDispatcher = dispatcher
                return dispatcher.writeToCharacteristics(with: [(.otaControl, 0)]).then { dispatcher }
            }.then { dispatcher -> Promise<Void> in
                dispatcher.firmwareMode = .ota
                return dispatcher.fulfillOnDisconnect()
            }.then { _ -> Promise<MuTagBleDispatcher> in
                // Silicon Labs has a 4 second delay in their DFU software so we are replicating here
                //
                return after(seconds: 4).then { _ -> Promise<MuTagBleDispatcher> in
                    return self.bleSessionManager.connect(to: muTag, force: true)
                }
            }.then { dispatcher -> Promise<MuTagBleDispatcher> in
                //muTagDispatcher = dispatcher
                return dispatcher.writeToCharacteristics(with: [(.otaControl, 0)]).then { dispatcher }
            }.then { dispatcher -> Promise<MuTagBleDispatcher> in
                return self.writeToMuTag(data: data, dispatcher: dispatcher).then { dispatcher }
            }.then { dispatcher -> Promise<MuTagBleDispatcher> in
                self.updateFlashProgress(to: 95, speed: 2)
                return dispatcher.writeToCharacteristics(with: [(.otaControl, 3)]).then { dispatcher }
            }.then { dispatcher -> Promise<MuTagBleDispatcher> in
                dispatcher.firmwareMode = .beacon
                // A manual disconnect here seems important to get refreshed charactertistics after peripheral reboot
                //
                self.bleSessionManager.disconnectFromMuTagWith(id: id)
                return self.bleSessionManager.connect(to: muTag, force: true)
            }.then { dispatcher -> Promise<(Any?, MuTagBleDispatcher)> in
                return dispatcher.readValue(from: .firmwareRevision).then { ($0, dispatcher) }
            }.then { (firmwareVersion, dispatcher) -> () in
                self.bleSessionManager.disconnectFromMuTagWith(id: id)
                if let version = firmwareVersion as? String {
                    DispatchQueue.global(qos: .background).async {
                        autoreleasepool {
                            do {
                                let realm = try Realm()
                                guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: id) else { return }
                                try realm.write {
                                    muTag.firmwareVersion = version
                                }
                            } catch {
                                print(error)
                            }
                        }
                    }
                }
                self.updateFlashProgress(to: 100) {
                    print("mµ tag \(id) successfully flashed to latest firmeware!")
                    self.showCompletionAlert()
                    fulfill(())
                }
            }.catch { error in
                print("Firmware update failed for mµ tag \(id)")
                if let dispatcher = self.bleSessionManager.getDispatcherFor(muTagId: id) {
                    dispatcher.firmwareMode = .beacon
                }
                self.bleSessionManager.disconnectFromMuTagWith(id: id)
                self.showCompletionAlert(failed: true)
                reject(error)
            }
        }
    }
    
    func writeToMuTag(data: Data, dispatcher: MuTagBleDispatcher) -> Promise<Void> {
        return Promise { fulfill, reject in
            self.updateFlashProgress(to: 10)
            let length = data.count
            let chunkSize = 20
            var offset = 0
            var dataChunksAndBytesRemaining = [(Data, Int)]()
            repeat {
                let thisChunkSize = ((length - offset) > chunkSize) ? chunkSize : (length - offset)
                var thisChunk = data.subdata(in: offset..<offset + thisChunkSize)
                offset += thisChunkSize
                let bytesRemaining = length - offset
                dataChunksAndBytesRemaining.append((thisChunk, bytesRemaining))
            } while offset < length
            func writeDataChunks() {
                let (chunk, bytesRemaining) = dataChunksAndBytesRemaining.removeFirst()
                // DEBUG
                print("DEBUG - (chunk, bytesRemaining) \((chunk as NSData, bytesRemaining))")
                
                dispatcher.writeToCharacteristics(with: [(.otaData, chunk)]).then { _ -> () in
                    // Should have a delay if using 'writeWithoutResponse'
                    //
                    //after(interval: 0.01).then { _ -> () in
                        if dataChunksAndBytesRemaining.isEmpty {
                            fulfill(())
                        } else {
                            let dataLeftMultiplier: Double = Double(bytesRemaining) / Double(length)
                            let overallPercentage = (80 - (dataLeftMultiplier * 80)) + 10
                            self.updateFlashProgress(to: CGFloat(overallPercentage))
                            writeDataChunks()
                        }
                    //}
                }.catch { error in
                    reject(error)
                }
            }
            writeDataChunks()
        }
    }
    
    func attemptUpdate(forMuTagId: String) -> Promise<Void> {
        guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: forMuTagId) else {
            return Promise { _, _ in throw FirmwareServiceError.muTagNotFoundForId }
        }
        return askForUpdate(onMuTagAttachedTo: muTag.attachedTo).then { update -> Promise<Void> in
            return self.flashMuTagWith(id: forMuTagId)
        }
    }
    
    func askForUpdate(onMuTagAttachedTo: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            let title = "Mµ tag update available"
            let message = "Mµ tag attached to \(onMuTagAttachedTo) needs to be updated. This is important to improve performance of the device."
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let updateAction = UIAlertAction(title: "Update", style: .default) { _ in
                fulfill(())
            }
            let cancelAction = UIAlertAction(title: "Not now", style: .cancel) { _ in
                reject(FirmwareServiceError.updateCancelledByUser)
            }
            alertController.addAction(updateAction)
            alertController.addAction(cancelAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showFlashProgress(forMuTagAttachedTo: String) {
        let vc = ProgressViewC()
        progressViewC = vc
        vc.setMuTag(attachedTo: forMuTagAttachedTo)
        vc.modalPresentationStyle = .overCurrentContext
        UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: false, completion: nil)
    }
    
    func updateFlashProgress(to value: CGFloat, speed: Double = 0.5, completion: (() -> Void)? = nil) {
        progressViewC?.updateProgress(to: value, speed: speed, completion: completion)
    }
    
    func dismissFlashProgress() {
        if progressViewC != nil {
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
            progressViewC = nil
        }
    }
    
    func showCompletionAlert(failed: Bool = false) {
        var title = ""
        var message = ""
        if failed {
            title = "Failed"
            message = "Sorry, the firmware update failed. Please try again when prompted."
        } else {
            title = "Success!"
            message = "This mµ tag has been updated!"
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { _ in
            self.dismissFlashProgress()
        }
        alertController.addAction(okayAction)
        progressViewC?.present(alertController, animated: true, completion: nil)
    }
}
