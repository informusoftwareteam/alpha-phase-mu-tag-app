//
//  MuTagParameters.swift
//  Informu
//
//  Created by Tom Daniel D. on 1/23/18.
//  Copyright © 2018 informu. All rights reserved.
//

import Foundation
import PromiseKit
import Firebase

enum MuTagParametersError: Error {
    case databaseReturnedInvalidValue
    case noMuTagsFound
}

class MuTagParameters {
    fileprivate let session: Session
    fileprivate let databaseRef = Database.database().reference()
    
    init() throws {
        self.session = try Session.shared()
    }
    
    func updateAccountMuTagsFromGlobalIfNeeded() {
        getGlobalAdvertisingInterval().then { advertisingInterval -> Promise<(Int, Int)> in
            return self.getGlobalTxPower().then { ($0, advertisingInterval) }
        }.then { (txPower, advertisingInterval) -> Void in
            guard let muTags = self.session.account?.muTags else { throw MuTagParametersError.noMuTagsFound }
            for muTag in muTags {
                
                // DEBUG
                print("DEBUG - Checking if muTag \(muTag.id) parameters need update")
                print("DEBUG - muTag.advertisingInterval = \(muTag.advertisingInterval), global.advertisingInterval = \(advertisingInterval)")
                
                if advertisingInterval != 0 && muTag.advertisingInterval != advertisingInterval {
                    // DEBUG
                    print("DEBUG - muTag.advertisingInterval \(muTag.id) needs update!")
                    
                    _ = muTag.updateAdvertisingInterval(to: advertisingInterval)
                }
                // A sloppy way to wait for advertising interval to update
                //
                Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { timer in
                    if txPower != 0 && muTag.txPower != txPower {
                        // DEBUG
                        print("DEBUG - muTag.txPower \(muTag.id) needs update!")
                        print("DEBUG - muTag.txPower = \(muTag.txPower), global.txPower = \(txPower)")
                        
                        _ = muTag.updateTxPower(to: txPower)
                    }
                }
            }
        }.catch { error in
            print(error)
        }
    }
    
    func getGlobalAdvertisingInterval() -> Promise<Int> {
        return Promise { fulfill, reject in
            databaseRef.child("global_settings/advertising_interval").observeSingleEvent(of: .value, with: { (snapshot) in
                print("Successfully retrieved global advertising interval from database")
                guard let advertisingInterval = snapshot.value as? Int else {
                    reject(MuTagParametersError.databaseReturnedInvalidValue); return
                }
                fulfill(advertisingInterval); return
            }) { (error) in
                reject(error); return
            }
        }
    }
    
    func getGlobalTxPower() -> Promise<Int> {
        return Promise { fulfill, reject in
            databaseRef.child("global_settings/tx_power").observeSingleEvent(of: .value, with: { (snapshot) in
                print("Successfully retrieved global tx power from database")
                guard let txPower = snapshot.value as? Int else {
                    reject(MuTagParametersError.databaseReturnedInvalidValue); return
                }
                fulfill(txPower); return
            }) { (error) in
                reject(error); return
            }
        }
    }
}
