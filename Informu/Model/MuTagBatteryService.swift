//
//  MuTagBatteryService.swift
//  Informu
//
//  Created by Tom Daniel D. on 10/18/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import PromiseKit
import RealmSwift

enum MuTagBatteryServiceError: Error {
    case tryingToReadTooSoon(Int, String)
    case receivedBadValue(String)
    case muTagNotFound
}

extension MuTagBatteryServiceError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .tryingToReadTooSoon(let minutesLeft, let muTagId):
            return NSLocalizedString("Sorry, you must wait for \(minutesLeft) minutes before reading the battery level of mμ tag \(muTagId)", comment: "")
        case .receivedBadValue(let muTagId):
            return NSLocalizedString("Received bad value from mµ tag \(muTagId)", comment: "")
        case .muTagNotFound:
            return NSLocalizedString("Mu tag not found with provided ID", comment: "")
        }
    }
}

class MuTagBatteryService {
    fileprivate let bleSessionManager = MuTagBleSessionManager.shared
    fileprivate let date = Date()
    
    fileprivate var timeOfLastBatteryRead = 0.0
    
    func readLevelFrom(muTagId: String) -> Promise<Int> {
        return Promise { fulfill, reject in
            let timeNow = date.timeIntervalSinceReferenceDate
            // TODO: temporarily disabled battery read limits
            //
            let hoursSinceLastRead = 13.0//(timeNow - timeOfLastBatteryRead) / 60 / 60
            let minutesUntilReadAvailable: Int = {
                let hoursRemaining = 12 - hoursSinceLastRead
                
                if hoursRemaining > 0 {
                    return Int(ceil(hoursRemaining * 60))
                }
                
                return 0
            }()
            
            if hoursSinceLastRead < 12 {
                reject(MuTagBatteryServiceError.tryingToReadTooSoon(minutesUntilReadAvailable, muTagId))
                // TODO: Automatically queue the read of battery level after the 12 hour expiration period
            } else {
                let realm = try Realm()
                guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else {
                    throw MuTagBatteryServiceError.muTagNotFound
                }
                bleSessionManager.connect(to: muTag).then { dispatcher -> Promise<Any?> in
                    print("Reading battery level of mμ tag \(muTagId)...")
                    return dispatcher.readValue(from: .batteryLevel)
                }.then { batteryLevel -> () in
                    self.timeOfLastBatteryRead = timeNow
                    self.bleSessionManager.disconnectFromMuTagWith(id: muTagId)
                    guard let level = batteryLevel as? Int else { throw MuTagBatteryServiceError.receivedBadValue(muTagId) }
                    fulfill(level)
                }.catch { error in
                    reject(error)
                }
            }
        }
    }
}
