//
//  MuTagStatusManager.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/21/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import RealmSwift

class MuTagStatusManager {
    fileprivate let realm = try! Realm()
    fileprivate let notification = Notification()
    fileprivate let muTagRepo = MuTagRepository.shared
    
    fileprivate var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    fileprivate var didExitRegionQueue = [String: Date]()
    
    /*  We cannot use a timer of more than 10 seconds for 'handleDidExitRegionIfValidFor' and 'handleDidEnterRegionIfValidFor' because
     *  timers do not continue to function while the app is in background mode. They instead pick back where they left off when the app
     *  starts execution again. Realistically we should only use 8 seconds to provide a 2 second buffer for everything to finish before
     *  the app goes back into background mode.
     *
     *  As an extra precaution also use 'beginBackgroundTask(expirationHandler:)'.
     */
    
    func handleDidExitRegionIfValidFor(muTagId: String, cbEvent: Bool = false) {
        //
        // Core Bluetooth sometimes fires false 'didExitRegion' events. We need to wait for 10-15 seconds to see if
        // a 'didEnterRegion' has come through and removed our 'didExitRegion' event from the queue before the timeout.

        if let inSafeZone = isMuTagInSafeZone(withId: muTagId) {
            if inSafeZone { return }
        }
        let dateNow = Date()
        _ = muTagRepo.updateFor(muTagId: muTagId, exitEvent: cbEvent, overrideTimestamp: dateNow)
        didExitRegionQueue[muTagId] = dateNow
        switch UIApplication.shared.applicationState {
        case .background: beginBackgroundTask()
        default: break
        }
        Timer.scheduledTimer(withTimeInterval: 8.0, repeats: false) { [weak self] timer in
            if self == nil { return }
            if self!.didExitRegionQueue.keys.contains(muTagId) {
                self!.didExitRegionQueue.removeValue(forKey: muTagId)
                self!.setMuTagStatusAsUnsafeFor(muTagId: muTagId, timestamp: dateNow)
            }
        }
        if backgroundTask != UIBackgroundTaskInvalid {
            endBackgroundTask()
        }
    }
    
    func handleDidEnterRegionIfValidFor(muTagId: String, cbEvent: Bool = false) {
        //
        // Must check if this 'didEnterRegion' event was part of a false 'didExitRegion' event.
        // If the 'didExitRegion' timeout has not removed itself from the queue, then it is event
        // is a false positive.

        if let inSafeZone = isMuTagInSafeZone(withId: muTagId) {
            if inSafeZone { return }
        }
        let dateNow = Date()
        if didExitRegionQueue.keys.contains(muTagId) {
            let timestamp = didExitRegionQueue[muTagId]
            didExitRegionQueue.removeValue(forKey: muTagId)
            _ = muTagRepo.updateFor(muTagId: muTagId, exitEvent: cbEvent, usefulEvent: false, overrideTimestamp: timestamp)
        } else {
            setMuTagStatusAsFoundFor(muTagId: muTagId, timestamp: dateNow)
        }
    }
}

private extension MuTagStatusManager {
    func setMuTagStatusAsUnsafeFor(muTagId: String, timestamp: Date) {
        guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
        if muTag.unsafe { return }
        let dateNowString = ISO8601DateFormatter().string(from: timestamp)
        notification.notifyUnsafeMuTagWith(id: muTagId, attachedTo: muTag.attachedTo, timestamp: dateNowString)
        do {
            let realm = try Realm()
            guard let token = self.muTagRepo.notificationToken else { return }
            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
            muTag.realm?.beginWrite()
            muTag.proximity = 0
            muTag.rssi = -127
            muTag.accuracy = 0.0
            muTag.unsafe = true
            try muTag.realm?.commitWrite(withoutNotifying: [token])
            print("MuTag \(muTagId) is now set to 'unsafe'")
            _ = self.muTagRepo.updateFor(muTagId: muTagId, exitEvent: true, overrideTimestamp: timestamp)
        } catch {
            print(error)
        }
    }
    
    func setMuTagStatusAsFoundFor(muTagId: String, timestamp: Date) {
        guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
        if !muTag.unsafe { return }
        let dateNowString = ISO8601DateFormatter().string(from: timestamp)
        //self.notification.notifyFoundMuTagWith(id: muTagId, attachedTo: muTag.attachedTo, timestamp: dateNowString)
        do {
            let realm = try Realm()
            guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: muTagId) else { return }
            guard let token = self.muTagRepo.notificationToken else { return }
            let session = try Session.shared()
            muTag.realm?.beginWrite()
            muTag.lastSeen = dateNowString
            muTag.lastReentry = dateNowString
            if let mostRecentLocation = session.mostRecentLocation {
                muTag.recentLocation = mostRecentLocation
            }
            if let mostRecentLocationName = session.mostRecentLocationName {
                muTag.recentLocationName = mostRecentLocationName
            }
            muTag.unsafe = false
            try muTag.realm?.commitWrite(withoutNotifying: [token])
            print("MuTag \(muTagId) is now set to 'found'")
            _ = self.muTagRepo.updateFor(muTagId: muTagId, enterEvent: true, overrideTimestamp: timestamp)
        } catch {
            print(error)
        }
    }
    
    func isMuTagInSafeZone(withId: String) -> Bool? {
        guard let muTag = realm.object(ofType: MuTag.self, forPrimaryKey: withId) else { return nil }
        return !muTag.inSafeZone.isEmpty
    }
    
    func beginBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
}
