//
//  Notification.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/15/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

enum UNNotificationActionIdentifier: String {
    case unwanted
    case useful
}

class Notification {
    fileprivate let notificationCenter = UNUserNotificationCenter.current()
    fileprivate let defaultTrigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
    fileprivate let defaultTitle = NSString.localizedUserNotificationString(forKey: "mμ tag alert!", arguments: nil)
    fileprivate let informuTitle = NSString.localizedUserNotificationString(forKey: "Informµ app alert!", arguments: nil)
    fileprivate let unsafeNotificationContent = UNMutableNotificationContent()
    fileprivate let foundNotificationContent = UNMutableNotificationContent()
    fileprivate let bluetoothOffNotificationContent = UNMutableNotificationContent()
    fileprivate let bluetoothDeniedNotificationContent = UNMutableNotificationContent()
    
    init() {
        prepareNotificationCategoriesWithActions()
        prepareUnsafeNotificationContent()
        prepareFoundNotificationContent()
        prepareBluetoothOffNotificationContent()
        prepareBluetoothDeniedNotificationContent()
    }
    
    func notifyUnsafeMuTagWith(id: String, attachedTo: String, timestamp: String) {
        let bodyMessage = "Your \(attachedTo) is no longer with you"
        unsafeNotificationContent.body = NSString.localizedUserNotificationString(forKey: bodyMessage, arguments: nil)
        unsafeNotificationContent.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
        let request = UNNotificationRequest.init(identifier: "\(id)|\(timestamp)", content: unsafeNotificationContent, trigger: defaultTrigger)
        notificationCenter.add(request)
    }
    
    func notifyFoundMuTagWith(id: String, attachedTo: String, timestamp: String) {
        let bodyMessage = "You found your \(attachedTo)!"
        foundNotificationContent.body = NSString.localizedUserNotificationString(forKey: bodyMessage, arguments: nil)
        foundNotificationContent.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
        let request = UNNotificationRequest.init(identifier: "\(id)|\(timestamp)", content: foundNotificationContent, trigger: defaultTrigger)
        notificationCenter.add(request)
    }
    
    func notifyBluetoothOff() {
        let bodyMessage = "Warning: Bluetooth has been turned off. Your mµ tags will no longer function!"
        bluetoothOffNotificationContent.body = NSString.localizedUserNotificationString(forKey: bodyMessage, arguments: nil)
        let request = UNNotificationRequest.init(identifier: "notifyBluetoothOff", content: bluetoothOffNotificationContent, trigger: defaultTrigger)
        notificationCenter.add(request)
    }
    
    func notifyBluetoothDenied() {
        let bodyMessage = "Warning: Bluetooth permissions have been denied. Your mµ tags will not function!"
        bluetoothDeniedNotificationContent.body = NSString.localizedUserNotificationString(forKey: bodyMessage, arguments: nil)
        let request = UNNotificationRequest.init(identifier: "notifyBluetoothDenied", content: bluetoothDeniedNotificationContent, trigger: defaultTrigger)
        notificationCenter.add(request)
    }
    
    func clearBluetoothNotifications() {
        notificationCenter.removeDeliveredNotifications(withIdentifiers: ["notifyBluetoothOff", "notifyBluetoothDenied"])
    }
}

private extension Notification {
    func prepareNotificationCategoriesWithActions() {
        let unwantedNotifyAction = UNNotificationAction(identifier: UNNotificationActionIdentifier.unwanted.rawValue, title: "I didn't want this", options: [.destructive])
        let usefulNotifyAction = UNNotificationAction(identifier: UNNotificationActionIdentifier.useful.rawValue, title: "This was helpful!")
        let muTagNotifyCategory = UNNotificationCategory(
            identifier: "MuTagNotifyCategory",
            actions: [unwantedNotifyAction, usefulNotifyAction],
            intentIdentifiers: [],
            options: []
        )
        let bluetoothNotifyCategory = UNNotificationCategory(
            identifier: "BluetoothNotifyCategory",
            actions: [],
            intentIdentifiers: [],
            options: []
        )
        notificationCenter.setNotificationCategories([muTagNotifyCategory, bluetoothNotifyCategory])
    }
    
    func prepareUnsafeNotificationContent() {
        unsafeNotificationContent.title = defaultTitle
        unsafeNotificationContent.sound = UNNotificationSound.default()
        unsafeNotificationContent.categoryIdentifier = "MuTagNotifyCategory"
        unsafeNotificationContent.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
    }
    
    func prepareFoundNotificationContent() {
        foundNotificationContent.title = defaultTitle
        foundNotificationContent.sound = UNNotificationSound.default()
        foundNotificationContent.categoryIdentifier = "MuTagNotifyCategory"
        foundNotificationContent.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
    }
    
    func prepareBluetoothOffNotificationContent() {
        bluetoothOffNotificationContent.title = informuTitle
        bluetoothOffNotificationContent.sound = UNNotificationSound.default()
        bluetoothOffNotificationContent.categoryIdentifier = "BluetoothNotifyCategory"
        bluetoothOffNotificationContent.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
    }
    
    func prepareBluetoothDeniedNotificationContent() {
        bluetoothDeniedNotificationContent.title = informuTitle
        bluetoothDeniedNotificationContent.sound = UNNotificationSound.default()
        bluetoothDeniedNotificationContent.categoryIdentifier = "BluetoothNotifyCategory"
        bluetoothDeniedNotificationContent.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
    }
}
