//
//  Session.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/19/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Locksmith
import FirebaseAuth
import RealmSwift

@objcMembers
class Session: Object {
    var loginData: LoginData? {
        guard let loginCollection = Locksmith.loadDataForUserAccount(userAccount: "informuCurrentUser") else {
            return nil
        }
        if let service = loginCollection["federatedLogin"] as? String {
            switch service {
            case "google": return LoginData.google
            case "facebook": return LoginData.facebook
            default: return nil
            }
        }
        if let username = loginCollection["username"] as? String,
            let password = loginCollection["password"] as? String
        {
            return LoginData.informu(username, password)
        }
        return nil
    }
    
    // Realm properties
    //
    dynamic var id = "singleton"
    dynamic var account: Account?
    dynamic var inSafeZone = ""
    dynamic var mostRecentLocation: String?
    dynamic var mostRecentLocationName: String?
    dynamic var previousLocation: String?
    dynamic var previousLocationName: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Session {
    // This is basically a singleton because there is only ever one realm session object
    //
    static func shared() throws -> Session {
        let realm = try Realm()
        if let session = realm.object(ofType: Session.self, forPrimaryKey: "singleton") {
            return session
        }
        try realm.write {
            realm.add(Session(), update: false)
        }
        // We can force-unwrap because Realm will throw if it failed to add the Realm object
        //
        return realm.object(ofType: Session.self, forPrimaryKey: "singleton")!
    }
    
    static func saveLogin(with data: LoginData) throws {
        print("Attempting to save login data to session (keychain) for persisted login...")
        switch data {
        case .informu(let username, let password):
            try Locksmith.updateData(data: ["username": username, "password": password], forUserAccount: "informuCurrentUser")
        case .google:
            try Locksmith.updateData(data: ["federatedLogin": "google"], forUserAccount: "informuCurrentUser")
        case .facebook:
            try Locksmith.updateData(data: ["federatedLogin": "facebook"], forUserAccount: "informuCurrentUser")
        }
    }
    
    static func resetSession() throws {
        try Locksmith.deleteDataForUserAccount(userAccount: "informuCurrentUser")
        try DispatchQueue.global(qos: .background).sync {
            try autoreleasepool {
                let session = try Session.shared()
                let realm = try Realm()
                try realm.write {
                    if let account = session.account {
                        realm.delete(account.muTags)
                        realm.delete(account)
                    }
                    session.inSafeZone = ""
                    session.mostRecentLocation = nil
                    session.mostRecentLocationName = nil
                    session.previousLocation = nil
                    session.previousLocationName = nil
                }
            }
        }
    }
}
