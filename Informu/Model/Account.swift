//
//  Login.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/10/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class Account: Object {
    dynamic var id = ""
    dynamic var user: User?
    dynamic var safeZone = ""
    let muTags = List<MuTag>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
