//
//  TutorialPage1ViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/25/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material

class TutorialPage1ViewC: UIViewController {
    var titleLabel: UILabel!
    var backgroundImage: UIImageView!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        prepareSettingLabel()
        prepareLayout()
    }
}

private extension TutorialPage1ViewC {
    func prepareBackground() {
        let image = UIImage(named: "tutorial1")!
        backgroundImage = UIImageView(image: image)
        backgroundImage.contentMode = .scaleAspectFill
        
        /*UIGraphicsBeginImageContext(view.frame.size)
        UIImage(named: "tutorial1")!.draw(in: view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image)*/
    }
    
    func prepareSettingLabel() {
        titleLabel = UILabel()
        titleLabel.numberOfLines = 2
        titleLabel.text = "Welcome to mµ tag"
        titleLabel.textAlignment = .center
        titleLabel.baselineAdjustment = .alignCenters
        titleLabel.textColor = Color.white
        titleLabel.font = RobotoFont.light(with: 60)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func prepareLayout() {
        view.layout(backgroundImage).edges()
        view.layout(titleLabel).top(40).left(10).right(10)
    }
}
