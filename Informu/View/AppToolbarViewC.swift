//
//  AppToolbarViewController.swift
//  Scrip
//
//  Created by Tom Daniel D. on 3/8/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import UIKit
import Material

class AppToolbarViewC: ToolbarController {
    var menuButton: IconButton!
    //fileprivate var addTagsButton: IconButton!
    
    open override func prepare() {
        super.prepare()
        prepareMenuButton()
        //prepareAddTagsButton()
        prepareStatusBar()
        prepareToolbar()
    }
}

// MARK: - UI Element Configuration
extension AppToolbarViewC {
    
    fileprivate func prepareMenuButton() {
        menuButton = IconButton(image: Icon.cm.menu, tintColor: .white)
        //menuButton.pulseColor = .white
        menuButton.addTarget(self, action: #selector(handleMenuButton), for: .touchUpInside)
    }
    
    fileprivate func prepareStatusBar() {
        statusBarStyle = .lightContent
        statusBar.backgroundColor = Color.deepOrange.lighten2
    }
    
    fileprivate func prepareToolbar() {
        toolbar.backgroundColor = Color.deepOrange.lighten2
        toolbar.leftViews = [menuButton]
        //toolbar.rightViews = [addTagsButton]
    }
    
}

extension AppToolbarViewC {
    @objc
    fileprivate func handleMenuButton() {
        navigationDrawerController?.openLeftView()
    }
}
