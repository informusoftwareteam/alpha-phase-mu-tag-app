//
//  MapViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import UIKit
import Material
import GoogleMaps
import RealmSwift
import MapKit

enum MapViewState {
    case primary
    case safeZone
}

protocol MapViewCDelegate: class {
    func selectedUnsafeMuTag(muTag: MuTag)
    func removedSelection()
}

class MapViewC: UIViewController {
    fileprivate let locationService = LocationService.shared
    
    weak var delegate: MapViewCDelegate?
    
    fileprivate var state = MapViewState.primary
    var realm: Realm!
    var session: Session?
    var muTags: Results<MuTag>!
    var inRangeMuTags: Results<MuTag>!
    var unsafeMuTags: Results<MuTag>!
    var sessionNotificationToken: NotificationToken?
    var muTagCollectionNotificationToken: NotificationToken?
    var muTagNotificationTokens = [String : NotificationToken]()
    var safeZoneOnMap: CLLocationCoordinate2D?
    var camera: GMSCameraPosition!
    var menuButton: IconButton!
    var mapView: GMSMapView!
    var safeZoneBar: UIView!
    var safeZoneBarLeftConstraint: NSLayoutConstraint!
    fileprivate var unsafeMuTagPicker: UIPickerView!
    var tableView: AddressTableView!
    var searchCompleter: MKLocalSearchCompleter!
    var searchBarView: UIView!
    var searchBar: UISearchBar!
    //var useCurrentButton: FlatButton!
    var searchBarRightConstraint: NSLayoutConstraint!
    var goToSafeZoneButton: FlatButton!
    var changeSafeZoneButton: FlatButton!
    var updateButton: FlatButton!
    var safeZoneMarker: GMSMarker!
    var safeZoneCircle: GMSCircle!
    var myLocationCount: UILabel!
    var myLocationMarker: GMSMarker!
    var myLocationCircle: GMSCircle!
    var unsafeMuTagNameImages = [String : UIImage]()
    var unsafeMuTagMarkers = [String : GMSMarker]()
    var unsafeMuTagCircles = [String : [GMSCircle]]()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.realm = try! Realm()
        do {
            self.session = try Session.shared()
        } catch {
            print(error)
        }
        self.muTags = realm.objects(MuTag.self)
        self.inRangeMuTags = realm.objects(MuTag.self).filter("unsafe = false")
        self.unsafeMuTags = realm.objects(MuTag.self).filter("unsafe = true")
        view.backgroundColor = Color.grey.lighten4
        
        prepareMap()
        prepareSafeZoneBar()
        prepareSearchBar()
        prepareUpdateButton()
        prepareUnsafeMuTagPicker()
        prepareSafeZoneMarker()
        prepareMyLocationMarker()
        addSafeZoneToMap()
        updateMyLocationOnMap()
        prepareRealmNotifications()
        prepareLayout()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        // Need to know when Google Maps updates it's current location so we can set our current location marker correctly
        //
        mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
        updateMapLocation()
        prepareToolbar()
        updateLayout()
        loadSafeZoneSetupIfNeeded()
    }
    
    override func viewWillLayoutSubviews() {

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        mapView.removeObserver(self, forKeyPath: "myLocation")
        load(state: .primary)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "myLocation" {
            guard let update = change, let newLocation = update[NSKeyValueChangeKey.newKey] as? CLLocation else { return }
            updateMyLocationOnMap(with: newLocation)
        }
    }
    
    func setIconColor(selected: Bool) {
        
        if selected == true {
            
            UIView.animate(withDuration: 0.25, animations: {
                self.tabItem.imageView?.tintColor = Color.grey.darken3
            }, completion: nil)
            
        } else {
            
            UIView.animate(withDuration: 0.25, animations: {
                self.tabItem.imageView?.tintColor = Color.grey.base
            }, completion: nil)
            
        }
        
    }
    
    deinit {
        //stopRealmNotifications()
    }
}

private extension MapViewC {
    func prepareToolbar() {
        guard let tc = toolbarController else { return }
        tc.toolbar.title = "mμ tags map"
        tc.toolbar.titleLabel.textColor = Color.white
        tc.toolbar.rightViews = []
        
        if menuButton == nil {
            guard let tc = toolbarController else { return }
            menuButton = tc.toolbar.leftViews.first as! IconButton
        }
    }
    
    func prepareRealmNotifications() {
        /*sessionNotificationToken = session?.observe { [weak self] change in
            switch change {
            case .change(let properties):
                if (properties.first(where: { $0.name == "mostRecentLocation" })) != nil {
                    self.updateMyLocationOnMap()
                }
            case .deleted:
                print("Warning: Session was deleted. This should never happen!")
                return
            case .error(let error):
                print(error)
            }
        }*/
        
        muTagCollectionNotificationToken = muTags.observe { [weak self] (changes: RealmCollectionChange) in
            guard let _ = self else { return }
            
            switch changes {
            case .initial:
                for muTag in self!.muTags {
                    self!.prepareMuTagNotification(on: muTag)
                    self!.updateMuTagOnMap(muTag: muTag)
                }
                
                break
            case .update(_, _, let insertions, _):
                if insertions != [] {
                    for index in insertions {
                        let muTag = self!.muTags[index]
                        self!.prepareMuTagNotification(on: muTag)
                        self!.updateMuTagOnMap(muTag: muTag)
                    }
                }
                
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                
                break
            }
        }
    }
    
    func prepareMuTagNotification(on muTag: MuTag) {
        // Prevents Realm crash.
        // Realm will crash if a deleted Realm object is referenced. MuTag ID could be used after object deletion.
        //
        let muTagId = muTag.id
        
        muTagNotificationTokens[muTagId] = muTag.observe { [weak self] change in
            switch change {
            case .change(let properties):
                if (properties.first(where: { $0.name == "unsafe" })) != nil {
                    self?.unsafeMuTagPicker.reloadAllComponents()
                    self?.updateMuTagOnMap(muTag: muTag)
                }
            case .deleted:
                print("MapViewC.prepareMuTagNotification() - The Realm 'muTag' object \(muTagId) was deleted.")
                self?.muTagNotificationTokens[muTagId] = nil
            case .error(let error):
                print(error)
            }
        }
    }
    
    func prepareMap() {
        // TODO: Set camera to current location
        //
        let defaultLocation = CLLocation(latitude: 40.0150, longitude: -105.2705)
        let currentLocation = getMostRecentLocation() ?? defaultLocation
        camera = GMSCameraPosition.camera(withTarget: currentLocation.coordinate, zoom: 18.0)
        mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        // Add padding to change the location of compass and my location buttons
        //
        let mapInsets = UIEdgeInsetsMake(40, 0, 7, 0)
        mapView.padding = mapInsets
    }
    
    func prepareSafeZoneBar() {
        safeZoneBar = UIView()
        safeZoneBar.backgroundColor = Color.deepOrange.lighten3.withAlphaComponent(0.9)
        
        goToSafeZoneButton = FlatButton(title: "Go to safe zone")
        goToSafeZoneButton.backgroundColor = Color.grey.lighten4
        goToSafeZoneButton.titleColor = Color.grey.darken2
        goToSafeZoneButton.titleLabel?.font = RobotoFont.regular(with: 14)
        goToSafeZoneButton.cornerRadiusPreset = .cornerRadius3
        goToSafeZoneButton.addTarget(self, action: #selector(handleGoToSafeZoneButton), for: .touchUpInside)
        
        changeSafeZoneButton = FlatButton(title: "Change safe zone")
        changeSafeZoneButton.backgroundColor = Color.grey.lighten4
        changeSafeZoneButton.titleColor = Color.grey.darken2
        changeSafeZoneButton.titleLabel?.font = RobotoFont.regular(with: 14)
        changeSafeZoneButton.cornerRadiusPreset = .cornerRadius3
        changeSafeZoneButton.addTarget(self, action: #selector(handleChangeSafeZoneButton), for: .touchUpInside)
        
        safeZoneBar.layout(goToSafeZoneButton).left(34).right(206).top(8).bottom(8)
        safeZoneBar.layout(changeSafeZoneButton).left(206).right(34).top(8).bottom(8)
    }
    
    func prepareUnsafeMuTagPicker() {
        unsafeMuTagPicker = UIPickerView()
        unsafeMuTagPicker.dataSource = self
        unsafeMuTagPicker.delegate = self
    }
    
    func prepareSearchBar() {
        searchCompleter = MKLocalSearchCompleter()
        searchCompleter.delegate = self
        
        searchBarView = UIView()
        searchBarView.backgroundColor = Color.deepOrange.lighten3.withAlphaComponent(0.9)
        
        searchBar = UISearchBar()
        searchBar.backgroundImage = UIImage()
        searchBar.tintColor = Color.white
        searchBar.placeholder = "Enter an address"
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        
        searchBarView.layout(searchBar).top().bottom()
        
        let views: [String : AnyObject] = ["searchBar": searchBar]

        searchBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[searchBar]|", options:[], metrics: nil, views: views))
        
        tableView = AddressTableView()
        tableView.tableFooterView = UIView()
        tableView.showsHorizontalScrollIndicator = false
        tableView.addressTableViewDelegate = self
    }
    
    func prepareUpdateButton() {
        updateButton = FlatButton(title: "Update to new address")
        updateButton.backgroundColor = Color.grey.darken2
        updateButton.titleColor = Color.white
        updateButton.cornerRadiusPreset = .cornerRadius3
        updateButton.isHidden = true
        updateButton.addTarget(self, action: #selector(setSafeZone), for: .touchUpInside)
    }
    
    func prepareSafeZoneMarker() {
        safeZoneMarker = GMSMarker()
        safeZoneMarker.snippet = "Safe zone"
        safeZoneMarker.appearAnimation = .pop

        safeZoneCircle = GMSCircle()
        safeZoneCircle.radius = 50
        safeZoneCircle.fillColor = Color.green.base.withAlphaComponent(0.25)
        safeZoneCircle.strokeWidth = 0
    }
    
    func prepareMyLocationMarker() {
        myLocationCount = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        myLocationCount.textColor = Color.white
        myLocationCount.font = RobotoFont.medium(with: 18)
        myLocationCount.textAlignment = .center
        myLocationCount.backgroundColor = Color.black
        myLocationCount.layer.masksToBounds = true
        myLocationCount.layer.cornerRadiusPreset = .cornerRadius4
        
        myLocationMarker = GMSMarker()
        myLocationMarker.snippet = "Current location"
        myLocationMarker.appearAnimation = .pop
        
        myLocationCircle = GMSCircle()
        myLocationCircle.radius = 10
        myLocationCircle.fillColor = Color.black.withAlphaComponent(0.1)
        myLocationCircle.strokeWidth = 1
        myLocationCircle.strokeColor = Color.black.withAlphaComponent(0.5)
    }
    
    func prepareLayout() {
        var allConstraints = [NSLayoutConstraint]()
        
        view.layout(mapView).left().right().top().bottom()
        
        view.layout(safeZoneBar).top().height(44)
        allConstraints += [NSLayoutConstraint(item: safeZoneBar, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1.0, constant: 0.0)]
        safeZoneBarLeftConstraint = NSLayoutConstraint(item: safeZoneBar, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1.0, constant: 0.0)
        allConstraints.append(safeZoneBarLeftConstraint)
        
        view.layout(searchBarView).top()
        allConstraints += [NSLayoutConstraint(item: searchBarView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1.0, constant: 0.0)]
        searchBarRightConstraint = NSLayoutConstraint(item: searchBarView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.0, constant: 0.0)
        allConstraints.append(searchBarRightConstraint)

        view.layout(updateButton).top(70).left(30).right(30).height(60)
        view.layout(tableView).left(16).right(16).top(44)
        
        view.layout(unsafeMuTagPicker).left(75).right(75).bottom().height(90)
        
        view.addConstraints(allConstraints)
    }
    
    func updateLayout() {
        searchBarRightConstraint.constant = view.bounds.width
    }
}

private extension MapViewC {
    @objc
    func handleGoToSafeZoneButton() {
        guard let safeZone = session?.account?.safeZone else { return }
        guard let position = locationService.parseLocationCoordinate(from: safeZone) else { return }
        
        mapView.animate(toLocation: position)
        mapView.animate(toZoom: 18)
    }
    
    @objc
    func handleChangeSafeZoneButton() {
        load(state: .safeZone)
    }
    
    @objc
    func handleBackButton() {
        addSafeZoneToMap()
        load(state: .primary)
    }
    
    func loadSafeZoneSetupIfNeeded() {
        if !locationService.getSafeZoneStatus() {
            load(state: .safeZone)
        }
    }
    
    func load(state: MapViewState) {
        self.state = state
        switch state {
        case .primary:
            dismissSearch()
            hideSafeZoneElements()
            showPrimaryElements()
        case .safeZone:
            showSafeZoneElements()
            hidePrimaryElements()
        }
    }
    
    func showPrimaryElements() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            guard let tc = self.toolbarController else { return }
            tc.toolbar.title = "mμ tags map"
            if tc.toolbar.leftViews.first != nil {
                tc.toolbar.leftViews = [self.menuButton]
            }
            self.unsafeMuTagPicker.alpha = 1
            self.safeZoneBarLeftConstraint.constant = 0
            self.searchBarRightConstraint.constant = self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hidePrimaryElements() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.unsafeMuTagPicker.alpha = 0
            self.unsafeMuTagPicker.selectRow(0, inComponent: 0, animated: false)
            self.removeUnsafeMuTagMarkers()
            self.delegate?.removedSelection()
        }, completion: nil)
    }
    
    func showSafeZoneElements() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            guard let tc = self.toolbarController else { return }
            tc.toolbar.title = "change your safe zone"
            if tc.toolbar.leftViews.first != nil {
                let backButton = IconButton(image: Icon.cm.arrowBack, tintColor: .white)
                backButton.addTarget(self, action: #selector(self.handleBackButton), for: .touchUpInside)
                tc.toolbar.leftViews = [backButton]
            }
            self.safeZoneBarLeftConstraint.constant = -self.view.bounds.width
            self.searchBarRightConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideSafeZoneElements() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.updateButton.alpha = 0
        }, completion: nil)
    }
    
    func updateMapLocation() {
        guard let currentLocation = getMostRecentLocation() else { return }
        mapView.animate(toLocation: currentLocation.coordinate)
        mapView.animate(toZoom: 18)
    }
    
    func updateMyLocationOnMap(with location: CLLocation? = nil) {
        guard let currentLocation = location ?? getMostRecentLocation() else { return }
        let coordinate = currentLocation.coordinate
        // TODO: These map updates should only occur if the 'go to my location' button is pressed and
        // the map position hasn't been move by the user
        //
        //mapView.animate(toLocation: coordinate)
        //mapView.animate(toZoom: 18)
        
        mapView.animate(toBearing: currentLocation.course)
        myLocationMarker.position = coordinate
        myLocationMarker.map = mapView
        myLocationCircle.position = coordinate
        myLocationCircle.map = mapView
    }
    
    func addSafeZoneToMap(with coordinates: CLLocationCoordinate2D? = nil) {
        guard let safeZone = session?.account?.safeZone else { return }
        guard let position = coordinates ?? locationService.parseLocationCoordinate(from: safeZone) else { return }
        safeZoneOnMap = position
        
        //safeZoneMarker.map = mapView
        //safeZoneMarker.position = position
        safeZoneCircle.position = position
        safeZoneCircle.map = mapView
    }
    
    func updateMuTagOnMap(muTag: MuTag) {
        updateMyLocationLabel()
    
        if muTag.unsafe {
            if unsafeMuTagMarkers[muTag.id] != nil { return }
            guard let location = getMostRecentLocation(from: muTag) else { return }
            
            // Create an image with the name of a mu tag
            //
            let name = UITextView(frame: .zero)
            name.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
            name.text = muTag.attachedTo
            name.textColor = Color.white
            name.font = RobotoFont.medium(with: 18)
            name.backgroundColor = Color.red.darken3.withAlphaComponent(0.75)
            name.layer.cornerRadiusPreset = .cornerRadius4
            name.sizeToFit()
            UIGraphicsBeginImageContextWithOptions(name.frame.size, false, 0)
            guard let unsafeContext = UIGraphicsGetCurrentContext() else { return }
            name.layer.render(in: unsafeContext)
            unsafeMuTagNameImages[muTag.id] = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let unsafeMuTagMarker = GMSMarker()
            unsafeMuTagMarker.position = location.coordinate
            unsafeMuTagMarker.icon = unsafeMuTagNameImages[muTag.id]
            unsafeMuTagMarker.snippet = "Unsafe mμ tag"
            unsafeMuTagMarker.appearAnimation = .pop
            unsafeMuTagMarkers[muTag.id] = unsafeMuTagMarker
            
            let unsafeMuTagCircle1 = GMSCircle()
            unsafeMuTagCircle1.position = location.coordinate
            unsafeMuTagCircle1.radius = 15
            unsafeMuTagCircle1.fillColor = Color.red.darken3.withAlphaComponent(0.25)
            unsafeMuTagCircle1.strokeWidth = 0
            unsafeMuTagCircle1.map = mapView
            
            unsafeMuTagCircles[muTag.id] = [unsafeMuTagCircle1]
        } else {
            if let marker = unsafeMuTagMarkers[muTag.id] {
                marker.map = nil
                unsafeMuTagMarkers[muTag.id] = nil
            }
            
            if let circles = unsafeMuTagCircles[muTag.id] {
                for circle in circles {
                    circle.map = nil
                }
                
                unsafeMuTagCircles[muTag.id] = nil
            }
        }
    }
    
    func showUnsafeMuTagMarker(muTag: MuTag) {
        guard let unsafeMuTagMarker = unsafeMuTagMarkers[muTag.id] else { return }
        
        removeUnsafeMuTagMarkers()

        unsafeMuTagMarker.map = mapView
    }
    
    func removeUnsafeMuTagMarkers() {
        for marker in unsafeMuTagMarkers.values {
            marker.map = nil
        }
    }
    
    func updateMyLocationLabel() {
        myLocationCount.text = String(inRangeMuTags.count)
        
        UIGraphicsBeginImageContextWithOptions(myLocationCount.bounds.size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else { return }
        myLocationCount.layer.render(in: context)
        let myLocationCountImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        myLocationMarker.icon = myLocationCountImage
    }
    
    func stopRealmNotifications() {
        sessionNotificationToken?.invalidate()
        muTagCollectionNotificationToken?.invalidate()
        for token in muTagNotificationTokens.values {
            token.invalidate()
        }
    }
    
    func getMostRecentLocation(from muTag: MuTag? = nil) -> CLLocation? {
        if let m = muTag {
            guard let locationCoordinate = locationService.parseLocationCoordinate(from: m.recentLocation) else { return nil }
            return CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
        } else if let location = session?.mostRecentLocation {
            guard let locationCoordinate = locationService.parseLocationCoordinate(from: location) else { return nil }
            return CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
        } else {
            return nil
        }
    }
    
    func getMostRecentLocationName() -> String? {
        return session?.mostRecentLocationName
    }
    
    func updateSafeZone(from address: String) {
        searchBar.text = address
        
        let geoCoder = CLGeocoder()
        
        print("geocoder getting coordinates for address: \(address)")
        
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location
                else {
                    print("geocoder could not find location coordinates for address: \(address)")
                    return
            }
            
            self.safeZoneOnMap = location.coordinate
            
            self.addSafeZoneToMap(with: location.coordinate)
            
            self.mapView.animate(toLocation: location.coordinate)
            self.mapView.animate(toZoom: 18)
        }
    }
    
    func updateSafeZone(from coordinate: CLLocationCoordinate2D) {
        let geoCoder = CLGeocoder()
        let clLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        print("geocoder getting address for coordinate: \(coordinate)")
        
        geoCoder.reverseGeocodeLocation(clLocation) { (placemarks, error) in
            guard
                let p = placemarks,
                let placemark = p.first
                else {
                    print("geocoder could not find location address for coordinate: \(coordinate)")
                    return
            }
            
            var streetNumber = ""
            if let subThoroughfare = placemark.subThoroughfare {
                streetNumber = subThoroughfare + " "
            }
            let street = placemark.thoroughfare ?? "see on map"
            let city: String = {
                if let locality = placemark.locality {
                    return ", \(locality)"
                } else {
                    return ""
                }
            }()
            let recentLocationName = "\(streetNumber)\(street)\(city)"
            
            self.searchBar.text = recentLocationName
        }
        
        safeZoneOnMap = coordinate
        
        addSafeZoneToMap(with: coordinate)
        
        self.mapView.animate(toLocation: coordinate)
        self.mapView.animate(toZoom: 18)
    }
    
    @objc
    func setSafeZone() {
        guard let safeZone = safeZoneOnMap else { return }
        locationService.registerSafeZone(at: safeZone)
        load(state: .primary)
    }
    
    func dismissSearch() {
        searchBar.text = nil
        searchBar.endEditing(true)
        
        tableView.data = []
    }
}

extension MapViewC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            tableView.data = []
        } else {
            searchCompleter.queryFragment = searchText
        }
    }
}

extension MapViewC: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        var results = completer.results.map { ($0.title, $0.subtitle) }
        
        if let location = getMostRecentLocationName() {
            let detail = location.replacingOccurrences(of: ":", with: "")
            
            results.insert(("use current location", detail), at: 0)
        } else {
            results.insert(("searching", "for current location..."), at: 0)
        }

        tableView.data = results
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension MapViewC: AddressTableViewDelegate {
    func handleCellSelection(index: Int) {
        // We have to get data from the table first even if we are not going to use it because `dismissSearch()` clears the table data
        //
        let address = tableView.data[index]
        // We execute `dismissSearch()` before `updateSafeZone()` because it clears the search field
        //
        dismissSearch()
        if index == 0 {
            guard let currentLocation = getMostRecentLocation() else { return }
            updateSafeZone(from: currentLocation.coordinate)
        } else {
            updateSafeZone(from: address.0 + " " + address.1)
        }
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.updateButton.isHidden = false
            self.updateButton.alpha = 1
        }, completion: nil)
    }
}

extension MapViewC: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row != 0 {
            let muTag = unsafeMuTags[row - 1]
            if let coordinate = getMostRecentLocation(from: muTag) {
                mapView.animate(toLocation: coordinate.coordinate)
                mapView.animate(toZoom: 18)
            }
            showUnsafeMuTagMarker(muTag: muTag)
            delegate?.selectedUnsafeMuTag(muTag: muTag)
        } else {
            removeUnsafeMuTagMarkers()
            delegate?.removedSelection()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return unsafeMuTags.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 26
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 210
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.backgroundColor = Color.grey.darken2.withAlphaComponent(0.95)
        pickerLabel.textColor = Color.white
        pickerLabel.textAlignment = NSTextAlignment.center
        
        if row == 0 {
            pickerLabel.text = "select unsafe mμ tag"
            pickerLabel.font = RobotoFont.regular(with: 14)
        } else {
            pickerLabel.text = unsafeMuTags[row - 1].attachedTo
            pickerLabel.font = RobotoFont.regular(with: 17)
        }
        
        return pickerLabel
    }
}
