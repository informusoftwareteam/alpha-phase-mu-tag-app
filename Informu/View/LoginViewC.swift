//
//  LoginViewController.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import UIKit
import Material
import GoogleSignIn

indirect enum LoginScreenState {
    // Associated values are for the previous state
    //
    case login(LoginScreenState?)
    //case signup(LoginScreenState?)
    case loading(LoginScreenState?)
    case previous
}

protocol LoginViewCDelegate: class {
    //func handleLoginButton(username: String, password: String)
    //func handleCreateAccountButton(username: String, password: String, passwordVerify: String)
    func handleFacebookButton()
    func handleGoogleButton()
}

class LoginViewC: UIViewController {
    weak var delegate: LoginViewCDelegate?
    
    fileprivate var screenState: LoginScreenState?
    fileprivate var loadingView: UIView!
    fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var logoImageView: UIImageView!
    fileprivate var topLoginContainer: UIView!
    //fileprivate var bottomLoginContainer: UIView!
    //fileprivate var emailField: ErrorTextField!
    //var passwordField: TextField!
    //var passwordVerifyField: TextField!
    //fileprivate var loginButton: FlatButton!
    //fileprivate var signUpButton: FlatButton!
    //fileprivate var createAccountButton: FlatButton!
    //fileprivate var cancelButton: FlatButton!
    //fileprivate var bottomLoginLabel: UILabel!
    fileprivate var loginLabel: UILabel!
    fileprivate var facebookButton: FlatButton!
    fileprivate var googleButton: FlatButton!
    fileprivate var views: Dictionary<String,UIView>!
    fileprivate var hConstraintLogoImageView: NSLayoutConstraint!
    fileprivate var hConstraintTopLoginContainer: NSLayoutConstraint!
    fileprivate var hConstraintBottomLoginContainer: NSLayoutConstraint!
    fileprivate var vConstraintStack: Array<NSLayoutConstraint>!
    //fileprivate var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Color.white
        
        //Looks for single or multiple taps.
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.screenWasTapped))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        prepareLoadingOverlay()
        prepareLogo()
        prepareTopLoginContainer()
        //preparePasswordField()
        //preparePasswordVerifyField()
        //prepareEmailField()
        //prepareLoginButton()
        //prepareCreateAccountButton()
        //prepareSignUpButton()
        //prepareCancelButton()
        //prepareBottomLoginContainer()
        //prepareBottomLoginLabel()
        prepareLoginLabel()
        prepareFacebookButton()
        prepareGoogleButton()
        prepareLayout()
        
        changeScreenState(to: .login(nil))
        
        /*self.view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)*/
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    func presentErrorMessage(message: String) {
        let alertController = UIAlertController(title: "Sorry", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func changeScreenState(to state: LoginScreenState) {
        switch state {
        case .login:
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.stopLoading()
                /*self.vConstraintStack[2].constant = 0
                self.vConstraintStack[4].constant = 200
                self.passwordVerifyField.alpha = 0
                self.loginButton.alpha = 1
                self.signUpButton.alpha = 1
                self.cancelButton.alpha = 0
                self.facebookButton.alpha = 1
                self.googleButton.alpha = 1
                self.view.layoutIfNeeded()
                self.passwordField.returnKeyType = .join*/
            }, completion: nil)
            
            screenState = .login(screenState)
        /*case .signup:
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.stopLoading()
                self.vConstraintStack[2].constant = 40
                self.vConstraintStack[4].constant = 0
                self.passwordVerifyField.alpha = 1
                self.loginButton.alpha = 0
                self.signUpButton.alpha = 0
                self.cancelButton.alpha = 1
                self.facebookButton.alpha = 0
                self.googleButton.alpha = 0
                self.view.layoutIfNeeded()
                self.passwordField.returnKeyType = .next
            }, completion: nil)
            
            screenState = .signup(screenState)*/
        case .loading:
            loadingView.alpha = 1
            activityIndicator.startAnimating()
            
            screenState = .loading(screenState)
        case .previous:
            guard let currentState = screenState else { return }
            
            var previousState: LoginScreenState?
            
            switch currentState {
            /*case .login(let previous):
                if let p = previous {
                    previousState = p
                }
            case .signup(let previous):
                if let p = previous {
                    previousState = p
                }*/
            case .loading(let previous):
                if let p = previous {
                    previousState = p
                }
            default: return
            }
            
            guard let p = previousState else { return }
            changeScreenState(to: p)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

fileprivate extension LoginViewC {
    func prepareLoadingOverlay() {
        loadingView = UIView()
        loadingView.backgroundColor = Color.black.withAlphaComponent(0.5)
        
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        
        loadingView.layout(activityIndicator).center()
    }
    
    func prepareLogo() {
        let logoImage = UIImage(named: "informu-logo")!
        logoImageView = UIImageView(frame: CGRect.zero)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
    }
    
    func prepareTopLoginContainer() {
        topLoginContainer = UIView()
    }
    
    /*func prepareEmailField() {
        emailField = ErrorTextField()
        emailField.tag = 0
        emailField.returnKeyType = .next
        emailField.delegate = self
        emailField.placeholder = "Email"
        emailField.detail = "Error, incorrect email"
        emailField.isClearIconButtonEnabled = true
        emailField.placeholderActiveColor = Color.deepOrange.lighten2
        emailField.dividerActiveColor = Color.deepOrange.lighten2
        emailField.leftViewActiveColor = Color.deepOrange.lighten2
        
        let leftView = UIImageView()
        leftView.image = Icon.cm.pen
        emailField.leftView = leftView
    }
    
    func preparePasswordField() {
        passwordField = TextField()
        passwordField.tag = 1
        passwordField.delegate = self
        passwordField.placeholder = "Password"
        passwordField.detail = "At least 8 chars with 1 number or special char"
        passwordField.clearButtonMode = .whileEditing
        passwordField.isVisibilityIconButtonEnabled = true
        
        passwordField.visibilityIconButton?.tintColor = Color.blue.base.withAlphaComponent(passwordField.isSecureTextEntry ? 0.1 : 0.7)
        
        passwordField.placeholderActiveColor = Color.deepOrange.lighten2
        passwordField.dividerActiveColor = Color.deepOrange.lighten2
        passwordField.leftViewActiveColor = Color.deepOrange.lighten2
        
        let leftView = UIImageView()
        leftView.image = Icon.cm.moreHorizontal
        passwordField.leftView = leftView
    }
    
    func preparePasswordVerifyField() {
        passwordVerifyField = TextField()
        passwordVerifyField.tag = 2
        passwordVerifyField.returnKeyType = .join
        passwordVerifyField.delegate = self
        passwordVerifyField.placeholder = "Retype Password"
        passwordVerifyField.clearButtonMode = .whileEditing
        passwordVerifyField.isVisibilityIconButtonEnabled = true
        
        passwordVerifyField.visibilityIconButton?.tintColor = Color.blue.base.withAlphaComponent(passwordField.isSecureTextEntry ? 0.1 : 0.7)
        
        passwordVerifyField.placeholderActiveColor = Color.deepOrange.lighten2
        passwordVerifyField.dividerActiveColor = Color.deepOrange.lighten2
        passwordVerifyField.leftViewActiveColor = Color.deepOrange.lighten2
        
        let leftView = UIImageView()
        leftView.image = Icon.cm.moreHorizontal
        passwordVerifyField.leftView = leftView
    }
    
    func prepareLoginButton() {
        loginButton = FlatButton(title: "Login")
        loginButton.backgroundColor = Color.deepOrange.lighten2
        loginButton.titleColor = Color.white
        loginButton.cornerRadiusPreset = .cornerRadius3
        loginButton.addTarget(self, action: #selector(handleLoginButton), for: .touchUpInside)
    }
    
    func prepareSignUpButton() {
        signUpButton = FlatButton(title: "Sign Up")
        signUpButton.titleColor = Color.deepOrange.lighten2
        signUpButton.titleLabel?.font = RobotoFont.regular(with: 16)
        signUpButton.addTarget(self, action: #selector(handleSignUpButton), for: .touchUpInside)
    }
    
    func prepareCreateAccountButton() {
        createAccountButton = FlatButton(title: "Create Account")
        createAccountButton.backgroundColor = Color.deepOrange.lighten2
        createAccountButton.titleColor = Color.white
        createAccountButton.titleLabel?.font = RobotoFont.regular(with: 18)
        createAccountButton.cornerRadiusPreset = .cornerRadius3
        createAccountButton.addTarget(self, action: #selector(handleCreateAccountButton), for: .touchUpInside)
        createAccountButton.alpha = 0
    }
    
    func prepareCancelButton() {
        cancelButton = FlatButton(title: "Cancel")
        cancelButton.titleColor = Color.deepOrange.lighten2
        cancelButton.titleLabel?.font = RobotoFont.regular(with: 16)
        cancelButton.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
        cancelButton.alpha = 0
    }
    
    func prepareBottomLoginContainer() {
        bottomLoginContainer = UIView()
        bottomLoginContainer.backgroundColor = Color.grey.lighten4
    }
    
    func prepareBottomLoginLabel() {
        //let label = UILabel()
        bottomLoginLabel = UILabel()
        bottomLoginLabel.text = "or continue with"
        bottomLoginLabel.textColor = Color.grey.base
        bottomLoginLabel.font = RobotoFont.regular(with: 12)
    }*/
    
    func prepareLoginLabel() {
        loginLabel = UILabel()
        loginLabel.text = "login with"
        loginLabel.textColor = Color.grey.base
        loginLabel.font = RobotoFont.light(with: 40)
    }
    
    func prepareFacebookButton() {
        facebookButton = FlatButton(title: "Facebook")
        facebookButton.backgroundColor = Color.grey.lighten4//Color.white
        facebookButton.cornerRadiusPreset = .cornerRadius3
        facebookButton.borderWidthPreset = .border2
        facebookButton.borderColor = Color.blue.darken3
        facebookButton.titleColor = Color.blue.darken3
        facebookButton.addTarget(self, action: #selector(handleFacebookButton), for: .touchUpInside)
    }
    
    func prepareGoogleButton() {
        googleButton = FlatButton(title: "Google")
        googleButton.backgroundColor = Color.grey.lighten4//Color.white
        googleButton.cornerRadiusPreset = .cornerRadius3
        googleButton.borderWidthPreset = .border2
        googleButton.borderColor = Color.deepOrange.darken3
        googleButton.titleColor = Color.deepOrange.darken3
        googleButton.addTarget(self, action: #selector(handleGoogleButton), for: .touchUpInside)
    }
}

private extension LoginViewC {
    /*@objc
    func screenWasTapped() {
        print("Screen was tapped")
        
        unfocusTextFields()
    }
    
    @objc
    func handleLoginButton() {
        guard let username = emailField.text else {
            print("No email address was entered")
            return
        }
        guard let password = passwordField.text else {
            print("No password was entered")
            return
        }
        
        delegate?.handleLoginButton(username: username, password: password)
    }
    
    @objc
    func handleSignUpButton() {
        changeScreenState(to: .signup(nil))
    }
    
    @objc
    func handleCreateAccountButton() {
        guard let username = emailField.text else {
            print("No email address was entered")
            return
        }
        guard let password = passwordField.text else {
            print("No password was entered")
            return
        }
        guard let passwordVerify = passwordVerifyField.text else {
            print("No password was entered")
            return
        }
        
        delegate?.handleCreateAccountButton(username: username, password: password, passwordVerify: passwordVerify)
    }
    
    @objc
    func handleCancelButton() {
        unfocusTextFields()
        changeScreenState(to: .previous)
    }*/
    
    @objc
    func handleFacebookButton() {
        delegate?.handleFacebookButton()
    }
    
    @objc
    func handleGoogleButton() {
        delegate?.handleGoogleButton()
    }
    
    /*@objc
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            
            guard let currentState = screenState else { return }
            
            switch currentState {
            case .signup:
                if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                    self.vConstraintStack[0].constant = 60
                    self.vConstraintStack[5].constant = 0
                    //self.keyboardHeightLayoutConstraint?.constant = 200.0
                } else {
                    var constant: CGFloat = 0.0
                    if let keyboardHeight = endFrame?.size.height {
                        constant = keyboardHeight - 60
                    }
                    self.vConstraintStack[0].constant = 60 - constant
                    self.vConstraintStack[5].constant = constant
                    //self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 200.0
                }
                UIView.animate(withDuration: duration,
                               delay: TimeInterval(0),
                               options: animationCurve,
                               animations: { self.view.layoutIfNeeded() },
                               completion: nil)
            default: return
            }
        }
    }*/
    
    func stopLoading() {
        loadingView.alpha = 0
        activityIndicator.stopAnimating()
    }
    
    /*func unfocusTextFields() {
        emailField?.resignFirstResponder()
        passwordField?.resignFirstResponder()
        passwordVerifyField?.resignFirstResponder()
    }*/
}

private extension LoginViewC {
    func prepareLayout() {
        views = ["logoImageView": logoImageView, "topLoginContainer": topLoginContainer]//, "bottomLoginContainer": bottomLoginContainer]
        var allConstraints = [NSLayoutConstraint]()
        
        for value in views.values {
            value.translatesAutoresizingMaskIntoConstraints = false
        }
        
        /*topLoginContainer.layout(emailField).top(50).left(25).right(30)
        topLoginContainer.layout(passwordField).top(115).left(25).right(30)
        topLoginContainer.layout(passwordVerifyField).top(200).left(25).right(30)
        topLoginContainer.layout(loginButton).center(offsetY: 70).left(30).right(30).height(50)
        //topLoginContainer.layout(createAccountButton).center(offsetY: 70).left(30).right(30).height(50)
        topLoginContainer.layout(signUpButton).center(offsetY: 125)
        topLoginContainer.layout(cancelButton).center(offsetY: 125)
        bottomLoginContainer.layout(bottomLoginLabel).centerHorizontally().top(20)
        bottomLoginContainer.layout(facebookButton).center(offsetY: -20).left(30).right(30).height(50)
        bottomLoginContainer.layout(googleButton).center(offsetY: 35).left(30).right(30).height(50)*/
        topLoginContainer.layout(loginLabel).center(offsetY: -100)
        topLoginContainer.layout(facebookButton).center().left(30).right(30).height(50)
        topLoginContainer.layout(googleButton).center(offsetY: 70).left(30).right(30).height(50)

        //view.addSubview(bottomLoginContainer)
        view.addSubview(topLoginContainer)
        view.addSubview(logoImageView)
        view.layout(loadingView).edges()
        
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "|[logoImageView]|", options:[], metrics: nil, views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "|[topLoginContainer]|", options:[], metrics: nil, views: views)
        //allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "|[bottomLoginContainer]|", options:[], metrics: nil, views: views)
        vConstraintStack = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-(60)-[logoImageView(==90)][topLoginContainer]|",//[bottomLoginContainer(==200)]|",
            options:[], metrics: nil, views: views
        )
        allConstraints += vConstraintStack
        
        view.addConstraints(allConstraints)
        
        // Utility to view constraint array for specific modifications
        //
        for (index, value) in vConstraintStack.enumerated() {
            print("Item \(index): \(value)")
        }
    }
}

/*extension LoginViewC: TextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0, 1:
            if cancelButton.alpha != 1 {
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.signUpButton.alpha = 0
                }, completion: nil)
            }
        default:
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        switch textField.tag {
        case 0, 1:
            if cancelButton.alpha != 1 {
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.signUpButton.alpha = 1
                }, completion: nil)
            }
        default:
            return
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 0:
            guard let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? TextField else { return false }
            _ = nextField.becomeFirstResponder()
        case 1:
            guard let currentState = screenState else { return false }
            switch currentState {
            case .login:
                handleLoginButton()
            default:
                guard let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? TextField else { return false }
                _ = nextField.becomeFirstResponder()
            }
        case 2:
            handleSignUpButton()
        default:
            return false
        }
        
        // Do not add a line break
        return false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
}*/

extension LoginViewC: GIDSignInUIDelegate {
    
}
