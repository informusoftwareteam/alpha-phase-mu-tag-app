/*
 * Copyright (C) 2015 - 2017, Daniel Dahan and CosmicMind, Inc. <http://cosmicmind.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *	*	Redistributions of source code must retain the above copyright notice, this
 *		list of conditions and the following disclaimer.
 *
 *	*	Redistributions in binary form must reproduce the above copyright notice,
 *		this list of conditions and the following disclaimer in the documentation
 *		and/or other materials provided with the distribution.
 *
 *	*	Neither the name of CosmicMind nor the names of its
 *		contributors may be used to endorse or promote products derived from
 *		this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import UIKit
import Material
import RealmSwift

/*extension TableCard: Equatable {
    static func ==(lhs: TableCard, rhs: TableCard) -> Bool {
        switch (lhs, rhs) {
        case (let .muTag(A), let .muTag(B)):
            return A == B
        default:
            return false
        }
    }
}*/

protocol TagsTableViewProtocol: class {
    func handleCellSelection(muTagId: String)
}

class TagsTableView: UITableView {
    fileprivate let muTags: Results<MuTag> = {
        let realm = try! Realm()
        return realm.objects(MuTag.self)//.sorted(byKeyPath: "rssi", ascending: false)
    }()
    
    weak var tagsTableViewDelegate: TagsTableViewProtocol?
    
    fileprivate lazy var heights = [IndexPath: CGFloat]()
    fileprivate var notificationToken: NotificationToken?

    /**
     An initializer that initializes the object with a NSCoder object.
     - Parameter aDecoder: A NSCoder instance.
     */
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepare()
    }
    
    public init() {
        super.init(frame: .zero, style: .plain)
        prepare()
    }

    open func prepare() {
        delegate = self
        dataSource = self
        separatorStyle = .none
        backgroundColor = nil
        
        register(MuTagTableViewCell.self, forCellReuseIdentifier: "MuTagTableViewCell")
        
        setupNotificationToken()
    }
}

private extension TagsTableView {
    func setupNotificationToken() {
        notificationToken = muTags.observe { [weak self] changes in
            self?.updateTable(changes: changes)
        }
    }
    
    func updateTable(changes: RealmCollectionChange<Results<MuTag>>) {
        switch changes {
        case .initial:
            // Results are now populated and can be accessed without blocking the UI
            reloadData()
        case .update(_, let deletions, let insertions, _):
            // Query results have changed, so apply them to the UITableView
            beginUpdates()
            insertRows(at: insertions.map { IndexPath(row: $0, section: 0) },
                                 with: .automatic)
            deleteRows(at: deletions.map { IndexPath(row: $0, section: 0) },
                                 with: .automatic)
            endUpdates()
        case .error(let error):
            // An error occurred while opening the Realm file on the background worker thread
            fatalError("\(error)")
        }
    }
}

extension TagsTableView: UITableViewDataSource {
    
    // TODO: editActionsForRowAtIndexPath
    //  - Looks like this is used for swipe actions!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return muTags.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Prepares the cells within the tableView.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MuTagTableViewCell", for: indexPath) as! MuTagTableViewCell
        if indexPath.row < muTags.count {
            cell.muTagId = muTags[indexPath.row].id
            cell.isLast = indexPath.row == muTags.count - 1
            heights[indexPath] = cell.height
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedMuTag = muTags[indexPath.row]
        let selectedMuTagId = selectedMuTag.id
        tagsTableViewDelegate?.handleCellSelection(muTagId: selectedMuTagId)
    }
}

extension TagsTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heights[indexPath] ?? estimatedRowHeight
    }
}
