//
//  AddressTableView.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/29/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material

protocol AddressTableViewDelegate: class {
    func handleCellSelection(index: Int)
}

class AddressTableView: UITableView {
    weak var addressTableViewDelegate: AddressTableViewDelegate?
    lazy var heights = [IndexPath: CGFloat]()
    
    var data = [(String, String)]() {
        didSet {
            reloadData()
            updateHeight()
        }
    }
    
    /**
     An initializer that initializes the object with a NSCoder object.
     - Parameter aDecoder: A NSCoder instance.
     */
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepare()
    }
    
    /// An initializer.
    public init() {
        super.init(frame: .zero, style: .plain)
        prepare()
    }
    
    /// Prepares the tableView.
    open func prepare() {
        delegate = self
        dataSource = self
        separatorStyle = .singleLine
        backgroundColor = Color.white.withAlphaComponent(0.95)
    }
}

extension AddressTableView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Prepares the cells within the tableView.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "addressCell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "addressCell")
        }
        let address = data[indexPath.row]
        cell!.textLabel?.text = address.0
        cell!.detailTextLabel?.text = address.1
        if indexPath.row == 0 {
            cell!.textLabel?.font = RobotoFont.medium(with: 17)
            cell!.textLabel?.textColor = Color.grey.darken2
            cell!.detailTextLabel?.font = RobotoFont.medium(with: 13)
            cell!.detailTextLabel?.textColor = Color.grey.darken2
            cell!.backgroundColor = Color.grey.lighten4
        } else {
            cell!.textLabel?.font = RobotoFont.regular(with: 18)
            cell!.textLabel?.textColor = Color.grey.darken3
            cell!.detailTextLabel?.font = RobotoFont.regular(with: 13)
            cell!.detailTextLabel?.textColor = Color.grey.darken3
            cell!.backgroundColor = nil
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            heights[indexPath] = 62
            return 62
        }
        heights[indexPath] = 50
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addressTableViewDelegate?.handleCellSelection(index: indexPath.row)
    }
}

private extension AddressTableView {
    func updateHeight() {
        let height: CGFloat = {
            if contentSize.height > 0 {
                return contentSize.height + 18
            }
            return contentSize.height
        }()
        let maxHeight = CGFloat(320)

        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            var frame = self.frame
            frame.size.height = height
            
            if height > maxHeight {
                frame.size.height = maxHeight
            } else {
                frame.size.height = height
            }
            
            self.frame = frame
        }, completion: nil)
    }
}
