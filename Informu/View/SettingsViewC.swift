//
//  AppDelegate.swift
//  Scrip
//
//  Created by Tom Daniel D. on 3/14/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import UIKit
import Material

class SettingsViewC: UIViewController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        let settingsLabel = UILabel()
        settingsLabel.numberOfLines = 1
        settingsLabel.text = "Settings"
        settingsLabel.textAlignment = .center
        settingsLabel.baselineAdjustment = .alignCenters
        settingsLabel.textColor = Color.grey.darken3
        settingsLabel.font = RobotoFont.thin(with: 80)
        settingsLabel.adjustsFontSizeToFitWidth = true
        
        view.layout.center(settingsLabel)
    }
    
    
}
