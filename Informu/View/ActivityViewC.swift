//
//  ActivityViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import UIKit
import Material

class ActivityViewC: UIViewController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Color.grey.lighten4
        
        prepareTabItem()
        prepareToolbar()
        
        let settingsLabel = UILabel()
        settingsLabel.numberOfLines = 1
        settingsLabel.text = "Activity"
        settingsLabel.textAlignment = .center
        settingsLabel.baselineAdjustment = .alignCenters
        settingsLabel.textColor = Color.grey.darken3
        settingsLabel.font = RobotoFont.thin(with: 80)
        settingsLabel.adjustsFontSizeToFitWidth = true
        
        view.layout.center(settingsLabel)
        
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        prepareToolbar()
    }
    
}

fileprivate extension ActivityViewC {
    
    func prepareTabItem() {
        
        tabItem.image = UIImage(named: "activity-icon")!
        tabItem.image = tabItem.image!.withRenderingMode(.alwaysTemplate)
        
    }
    
}

fileprivate extension ActivityViewC {
    
    func prepareToolbar() {
        guard let tc = toolbarController else {
            return
        }
        
        tc.toolbar.title = "activity"
        tc.toolbar.titleLabel.textColor = Color.white
        tc.toolbar.rightViews = []
        
    }
    
}
