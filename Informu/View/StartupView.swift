//
//  StartupView.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/7/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit

class StartupView: UIView {
    var logoImageView: UIImageView!
    var activityIndicator: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black
        prepareLogo()
        prepareActivityIndicator()
        prepareLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

private extension StartupView {
    func prepareLogo() {
        let logoImage = UIImage(named: "informu-logo")!
        logoImageView = UIImageView(frame: CGRect.zero)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
    }
    
    func prepareActivityIndicator() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.startAnimating()
    }
    
    func prepareLayout() {
        translatesAutoresizingMaskIntoConstraints = false
        layout(logoImageView).center(offsetY: -100)
        layout(activityIndicator).center(offsetY: 100)
    }
}
