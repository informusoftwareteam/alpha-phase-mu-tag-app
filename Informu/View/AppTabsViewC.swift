//
//  AppTabsViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import UIKit
import Material

class AppTabsViewC: TabsController {
    open override func prepare() {
        super.prepare()
        preparePageTabBar()
        prepareIconColors()
    }
}

private extension AppTabsViewC {
    func preparePageTabBar() {
        tabBarAlignment = .bottom
        tabBar.isDividerHidden = false
        tabBar.lineAlignment = .top
        tabBar.lineColor = Color.deepOrange.lighten3
        tabBar.backgroundColor = Color.white
        tabBar.delegate = self
    }
    
    func prepareIconColors() {
        setIconColor(tabItem: viewControllers[0].tabItem, selected: true)
        setIconColor(tabItem: viewControllers[1].tabItem, selected: false)
        //setIconColor(tabItem: viewControllers[2].tabItem, selected: false)
    }
    
    func setIconColor(tabItem: TabItem, selected: Bool) {
        if selected == true {
            UIView.animate(withDuration: 0.25, animations: {
                tabItem.imageView?.tintColor = Color.grey.darken2
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.25, animations: {
                tabItem.imageView?.tintColor = Color.grey.lighten1
            }, completion: nil)
        }
    }
}

extension AppTabsViewC {
    func tabBar(tabBar: TabBar, willSelect tabItem: TabItem) {
        if let selectedTabItem = tabBar.selectedTabItem {
            setIconColor(tabItem: selectedTabItem, selected: false)
        }
        
        setIconColor(tabItem: tabItem, selected: true)
    }
}
