//
//  TagDetailViewC.swift
//  Informu
//
//  Created by Thomas Dickerson on 10/4/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material
import GoogleMaps
import MapKit
import RealmSwift

enum TagDetailViewCError: Error {
    case muTagNotFoundForId
}

protocol TagDetailViewCDelegate: class {
    func goToMap()
    func viewWillDisappearComplete()
}

class TagDetailViewC: UIViewController {
    let bleSessionManager = MuTagBleSessionManager.shared
    
    weak var delegate: TagDetailViewCDelegate?
    
    fileprivate var realm: Realm!
    fileprivate var setTagPropertiesViewC: SetTagPropertiesViewC!
    fileprivate var muTagId: String!
    //fileprivate var muTag: MuTag!
    var isRemovingMuTag = false
    fileprivate var detailView: UIView!
    fileprivate var mapView: GMSMapView!
    fileprivate var notificationToken: NotificationToken?
    fileprivate var backButton: IconButton!
    fileprivate var editButton: FlatButton!
    fileprivate var attachedLabel: UILabel!
    fileprivate var proximityView: UIView!
    fileprivate var proximityLabel: UILabel!
    fileprivate var splitView: UIView!
    fileprivate var batteryView: UIView!
    fileprivate var batteryLabel: UILabel!
    fileprivate var batteryIcon: UIImageView!
    fileprivate var batteryIndicator: UIActivityIndicatorView!
    fileprivate var secondaryDetailView: UIView!
    fileprivate var mapButton: FlatButton!
    fileprivate var locationLabel: UILabel!
    
    
    init(setTagPropertiesViewC: SetTagPropertiesViewC, muTagId: String) throws {
        self.realm = try Realm()
        self.muTagId = muTagId
        self.setTagPropertiesViewC = setTagPropertiesViewC
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func viewDidLoad() {
        prepareDetailView()
        prepareMapView()
        prepareMuTagOnMap()
        prepareRealmNotification()
        prepareLayout()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        if !isRemovingMuTag {
            loadTagProperties()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isRemovingMuTag {
            guard let muTag = MuTag.get(with: muTagId) else { return }
            bleSessionManager.disconnectFromMuTagWith(id: muTagId)
        }
        //delegate?.viewWillDisappearComplete()
    }
    
    deinit {
        notificationToken?.invalidate()
    }
}

private extension TagDetailViewC {
    func prepareDetailView() {
        detailView = UIView()
        detailView.backgroundColor = Color.white
        
        UIGraphicsBeginImageContext(CGSize(width: 28, height: 28))
        Icon.cm.arrowBack?.draw(in: CGRect(x: 3.5, y: 3.5, width: 20, height: 20))
        let arrowBack = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let templateImage = arrowBack?.withRenderingMode(.alwaysTemplate)
        let arrowBackImageView = UIImageView()
        arrowBackImageView.image = templateImage
        
        backButton = IconButton(image: arrowBackImageView.image, tintColor: Color.grey.darken2)
        backButton.backgroundColor = Color.grey.lighten3
        backButton.cornerRadiusPreset = .cornerRadius3
        backButton.addTarget(self, action: #selector(handleBackButton), for: .touchUpInside)
        
        editButton = FlatButton(title: "edit")
        editButton.backgroundColor = Color.grey.lighten3
        editButton.titleColor = Color.grey.darken2
        editButton.titleLabel?.font = RobotoFont.regular(with: 14)
        editButton.contentEdgeInsetsPreset = .horizontally4
        editButton.cornerRadiusPreset = .cornerRadius3
        editButton.addTarget(self, action: #selector(handleEditButton), for: .touchUpInside)
        
        attachedLabel = UILabel()
        attachedLabel.numberOfLines = 1
        attachedLabel.textAlignment = .center
        attachedLabel.baselineAdjustment = .alignCenters
        attachedLabel.textColor = Color.grey.darken3
        attachedLabel.font = RobotoFont.thin(with: 60)
        attachedLabel.adjustsFontSizeToFitWidth = true
        
        proximityView = UIView()
        proximityLabel = UILabel()
        proximityLabel.numberOfLines = 1
        proximityLabel.textAlignment = .center
        proximityLabel.baselineAdjustment = .alignCenters
        proximityLabel.font = RobotoFont.medium(with: 20)
        
        splitView = UIView()
        splitView.backgroundColor = Color.grey.lighten4
        
        batteryView = UIView()
        batteryLabel = UILabel()
        batteryLabel.numberOfLines = 1
        batteryLabel.textAlignment = .center
        batteryLabel.baselineAdjustment = .alignCenters
        batteryLabel.textColor = Color.grey.darken3
        batteryLabel.font = RobotoFont.regular(with: 20)
        batteryIndicator = UIActivityIndicatorView()
        batteryIndicator.activityIndicatorViewStyle = .gray
        batteryIndicator.startAnimating()
        let batteryImage = UIImage(named: "battery-icon")!
        batteryIcon = UIImageView(image: batteryImage)
        batteryIcon.image = batteryIcon.image!.withRenderingMode(.alwaysTemplate)
        batteryIcon.tintColor = Color.grey.lighten1
        batteryIcon.contentMode = .scaleAspectFit
        
        secondaryDetailView = UIView()
        secondaryDetailView.backgroundColor = Color.grey.lighten5
        
        mapButton = FlatButton(title: "Open map")
        mapButton.backgroundColor = Color.deepOrange.lighten2
        mapButton.titleColor = Color.white
        mapButton.titleLabel?.font = RobotoFont.regular(with: 14)
        mapButton.contentEdgeInsetsPreset = .horizontally4
        mapButton.cornerRadiusPreset = .cornerRadius3
        mapButton.addTarget(self, action: #selector(handleMapButton), for: .touchUpInside)

        locationLabel = UILabel()
        locationLabel.numberOfLines = 1
        locationLabel.textAlignment = .center
        locationLabel.baselineAdjustment = .alignCenters
        locationLabel.textColor = Color.grey.darken2
        locationLabel.font = RobotoFont.light(with: 20)
        
        /*let gradient = CAGradientLayer()
        gradient.locations = [0.0 , 0.05]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 60, width: view.bounds.width, height: attachedLabel.frame.height)
        
        guard let muTagColorValue = MuTagColor(rawValue: muTag.tagColor)?.colorValue else { return }
        gradient.colors = [muTagColorValue.cgColor, muTagColorValue.withAlphaComponent(0.1).cgColor]
        
        detailView.layer.insertSublayer(gradient, at: 0)*/
    }
    
    func prepareMapView() {
        let locationCoordinate: CLLocationCoordinate2D = {
            guard let muTag = MuTag.get(with: muTagId) else {
                return CLLocationCoordinate2D(latitude: 40.0150, longitude: -105.2705)
            }
            return parseLocation(from: muTag.recentLocation) ?? CLLocationCoordinate2D(latitude: 40.0150, longitude: -105.2705)
        }()
        let camera = GMSCameraPosition.camera(withLatitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude, zoom: 18)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
    }
    
    func prepareMuTagOnMap() {
        let locationCoordinate: CLLocationCoordinate2D = {
            guard let muTag = MuTag.get(with: muTagId) else {
                return CLLocationCoordinate2D(latitude: 40.0150, longitude: -105.2705)
            }
            return parseLocation(from: muTag.recentLocation) ?? CLLocationCoordinate2D(latitude: 40.0150, longitude: -105.2705)
        }()
        let camera = GMSCameraPosition.camera(withLatitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude, zoom: 18)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        let muTagCircle1 = GMSCircle()
        muTagCircle1.position = locationCoordinate
        muTagCircle1.radius = 30
        muTagCircle1.fillColor = Color.grey.darken3.withAlphaComponent(0.25)
        muTagCircle1.strokeWidth = 0
        muTagCircle1.map = mapView
    }
    
    func prepareRealmNotification() {
        guard let muTag = MuTag.get(with: muTagId) else { return }
        notificationToken = muTag.observe { [weak self] change in
            switch change {
            case .change(let properties):
                for property in properties {
                    switch property.name {
                    case "attachedTo":
                        self?.attachedLabel.text = property.newValue as? String
                    case "proximity":
                        self?.proximityLabel.text = muTag.proximityString
                    case "batteryLevel":
                        if let batteryLevel = property.newValue {
                            self?.batteryLabel.text = "\(batteryLevel)%"
                        } else {
                            self?.batteryLabel.text = "\(String(describing: muTag.batteryLevel))%"
                        }
                        self?.batteryIndicator.stopAnimating()
                    case "recentLocationName":
                        guard let location = property.newValue as? String else { return }
                        self?.locationLabel.text = location.replacingOccurrences(of: ":", with: " ")
                    default: break
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("TagDetailViewC.notificationToken - The Realm 'muTag' object \(String(describing: self?.muTagId)) was deleted.")
                self?.notificationToken?.invalidate()
            }
        }
        
        // Not sure why I did this. Delete if there are no bugs.
        //
        //notificationToken = token
    }
    
    func prepareLayout() {
        detailView.layout(backButton).left(16).top(24)
        detailView.layout(editButton).right(16).top(24).height(28)
        detailView.layout(attachedLabel).left(16).right(16).top(60)
        
        proximityView.layout(proximityLabel).center()
        batteryView.layout(batteryIndicator).center(offsetX: -25)
        batteryView.layout(batteryLabel).center(offsetX: -25)
        batteryView.layout(batteryIcon).width(30).height(30).center(offsetX: 20)
        
        var allConstraints = [NSLayoutConstraint]()
        let views: [String : UIView] = ["proximity": proximityView, "split": splitView, "battery": batteryView, "detail": detailView, "map": mapView]
        
        for value in views.values {
            value.translatesAutoresizingMaskIntoConstraints = false
        }
        
        detailView.addSubview(proximityView)
        detailView.addSubview(splitView)
        detailView.addSubview(batteryView)
        
        allConstraints += [NSLayoutConstraint(item: proximityView, attribute: .top, relatedBy: .equal, toItem: attachedLabel, attribute: .bottom, multiplier: 1.0, constant: 5.0)]
        allConstraints += [NSLayoutConstraint(item: proximityView, attribute: .bottom, relatedBy: .equal, toItem: secondaryDetailView, attribute: .top, multiplier: 1.0, constant: 0.0)]
        
        allConstraints += [NSLayoutConstraint(item: splitView, attribute: .top, relatedBy: .equal, toItem: attachedLabel, attribute: .bottom, multiplier: 1.0, constant: 20.0)]
        allConstraints += [NSLayoutConstraint(item: splitView, attribute: .bottom, relatedBy: .equal, toItem: secondaryDetailView, attribute: .top, multiplier: 1.0, constant: -15.0)]
        
        allConstraints += [NSLayoutConstraint(item: batteryView, attribute: .top, relatedBy: .equal, toItem: attachedLabel, attribute: .bottom, multiplier: 1.0, constant: 5.0)]
        allConstraints += [NSLayoutConstraint(item: batteryView, attribute: .bottom, relatedBy: .equal, toItem: secondaryDetailView, attribute: .top, multiplier: 1.0, constant: 0.0)]
        allConstraints += [NSLayoutConstraint(item: proximityView, attribute: .width, relatedBy: .equal, toItem: batteryView, attribute: .width, multiplier: 1.0, constant: 0.0)]
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "|[proximity][split(2@1000)][battery]|", options:[], metrics: nil, views: views)
        
        detailView.addConstraints(allConstraints)
        
        detailView.layout(secondaryDetailView).left().right().bottom().height(90)
        secondaryDetailView.layout(mapButton).height(28).center(offsetY: 20)
        secondaryDetailView.layout(locationLabel).center(offsetY: -20)
        
        view.addSubview(detailView)
        view.addSubview(mapView)
        
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "|[detail]|", options:[], metrics: nil, views: views)
        allConstraints += [NSLayoutConstraint(item: detailView, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 0.45, constant: 0.0)]

        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "|[map]|", options:[], metrics: nil, views: views)
        allConstraints += [NSLayoutConstraint(item: mapView, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 0.55, constant: 0.0)]
        
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|[detail][map]|", options:[], metrics: nil, views: views)
        
        view.addConstraints(allConstraints)
    }
}

private extension TagDetailViewC {
    func loadTagProperties() {
        guard let muTag = MuTag.get(with: muTagId) else { return }
        let lastBatteryLevel = muTag.batteryLevel
        attachedLabel.text = muTag.attachedTo
        proximityLabel.textColor = Color.grey.darken2
        let proximity: String = {
            if muTag.unsafe {
                proximityLabel.textColor = Color.red.lighten2
                
                return "unsafe"
            } else {
                return muTag.proximityString
            }
        }()
        proximityLabel.text = proximity
        muTag.updateBatteryLevelStatus().then { _ -> () in
            //self.batteryLabel.text = String(batteryLevel) + "%"
        }.catch { error in
            print(error)
            self.batteryLabel.text = "\(lastBatteryLevel)%"
            self.batteryIndicator.stopAnimating()
        }
        locationLabel.text = muTag.recentLocationName.replacingOccurrences(of: ":", with: "")
    }
    
    @objc
    func handleBackButton() {
        if let app = UIApplication.shared.delegate as? AppDelegate {
            let transition = CATransition()
            transition.duration = 0.25;
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            app.window.layer.add(transition, forKey: nil)
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc
    func handleEditButton() {
        present(setTagPropertiesViewC, animated: true)
    }
    
    func parseLocation(from string: String) -> CLLocationCoordinate2D? {
        let splitLocation = string.components(separatedBy: " ")
        guard let latitude = Double(splitLocation[0]) else { return nil }
        guard let longitude = Double(splitLocation[1]) else { return nil }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    @objc
    func handleMapButton() {
        handleBackButton()
        delegate?.goToMap()
    }
}
