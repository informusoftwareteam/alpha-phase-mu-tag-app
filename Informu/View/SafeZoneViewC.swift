//
//  SafeZoneViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/27/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material
import GoogleMaps
import MapKit
import RealmSwift

protocol SafeZoneViewCDelegate: class {
    func safeZoneDidComplete(location: CLLocationCoordinate2D)
}

class SafeZoneViewC: UIViewController {
    var session: Session?
    var sessionNotificationToken: NotificationToken!
    var tableView: AddressTableView!
    var searchCompleter: MKLocalSearchCompleter!
    weak var delegate: SafeZoneViewCDelegate?
    var addressHeaderView: UIView!
    var mapView: GMSMapView!
    var searchBar: UISearchBar!
    var useCurrentButton: FlatButton!
    var startButton: FlatButton!
    var myLocationMarker: GMSMarker!
    var safeZoneLocation: CLLocationCoordinate2D?
    var safeZoneMarker: GMSMarker!
    var safeZoneCircle: [GMSCircle]!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        do {
            session = try Session.shared()
        } catch {
            print(error)
        }
        prepareAddressHeader()
        prepareSearchBar()
        prepareMap()
        prepareMyLocationMarker()
        prepareSafeZoneMarker()
        prepareRealmNotifications()
        prepareUseCurrentButton()
        prepareStartButton()
        prepareLayout()
        updateMyLocationOnMap()
    }
}

private extension SafeZoneViewC {
    func prepareAddressHeader() {
        statusBarController?.statusBar.backgroundColor = Color.deepOrange.lighten2
        
        addressHeaderView = UIView()
        addressHeaderView.backgroundColor = Color.deepOrange.lighten2
        
        let label = UILabel()
        label.text = "Let's setup your Safe Zone"
        label.textColor = Color.white
        label.font = RobotoFont.light(with: 28)
        label.textAlignment = .center
        
        addressHeaderView.layout(label).centerHorizontally().top(30)
    }
    
    func prepareSearchBar() {
        searchCompleter = MKLocalSearchCompleter()
        searchCompleter.delegate = self
        
        searchBar = UISearchBar()
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = Color.deepOrange.lighten3.cgColor
        searchBar.barTintColor = Color.deepOrange.lighten3
        searchBar.tintColor = Color.white
        searchBar.placeholder = "Enter an address"
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        
        tableView = AddressTableView()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = Color.grey.base.withAlphaComponent(0.5)
        tableView.showsHorizontalScrollIndicator = false
        tableView.addressTableViewDelegate = self
    }
    
    func prepareMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 40.0150, longitude: -105.2705, zoom: 18)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
    }
    
    func prepareMyLocationMarker() {
        myLocationMarker = GMSMarker()
        myLocationMarker.snippet = "Current location"
        myLocationMarker.appearAnimation = .pop
    }
    
    func prepareSafeZoneMarker() {
        safeZoneMarker = GMSMarker()
        safeZoneMarker.snippet = "Safe zone"
        safeZoneMarker.appearAnimation = .pop
        
        let safeZoneCircle0 = GMSCircle()
        safeZoneCircle0.radius = 30*0.75
        safeZoneCircle0.fillColor = Color.green.base.withAlphaComponent(0.25)
        safeZoneCircle0.strokeWidth = 0
        
        let safeZoneCircle1 = GMSCircle()
        safeZoneCircle1.radius = 30
        safeZoneCircle1.fillColor = Color.green.base.withAlphaComponent(0.25)
        safeZoneCircle1.strokeWidth = 0
        
        safeZoneCircle = [safeZoneCircle0, safeZoneCircle1]
    }
    
    func prepareRealmNotifications() {
        sessionNotificationToken = session?.observe { [weak self] change in
            switch change {
            case .change(let properties):
                if properties.first(where: { $0.name == "mostRecentLocation" }) != nil {
                    self?.updateMyLocationOnMap()
                }
            case .deleted:
                print("Warning: Session was deleted. This should never happen!")
                return
            case .error(let error):
                print(error)
            }
        }
    }
    
    func prepareUseCurrentButton() {
        useCurrentButton = FlatButton(title: "Use current location")
        useCurrentButton.backgroundColor = Color.deepOrange.lighten2
        useCurrentButton.titleColor = Color.white
        useCurrentButton.cornerRadiusPreset = .cornerRadius3
        useCurrentButton.alpha = 1
        useCurrentButton.addTarget(self, action: #selector(handleUseCurrentButton), for: .touchUpInside)
    }
    
    func prepareStartButton() {
        startButton = FlatButton(title: "Continue to app")
        startButton.backgroundColor = Color.deepOrange.lighten2
        startButton.titleColor = Color.white
        startButton.cornerRadiusPreset = .cornerRadius3
        startButton.alpha = 0
        startButton.addTarget(self, action: #selector(handleStartButton), for: .touchUpInside)
    }
    
    func prepareLayout() {
        view.layout(mapView).top(102).bottom().left().right()
        view.layout(addressHeaderView).top().left().right().height(102)
        view.layout(useCurrentButton).bottom(20).left(30).right(30).height(50)
        view.layout(startButton).bottom(20).left(30).right(30).height(50)
        view.layout(searchBar).left(20).right(22).top(80)
        view.layout(tableView).left(20).right(22).top(124)
    }
    
    func updateMyLocationOnMap() {
        guard let mostRecentLocation = session?.mostRecentLocation else { return }
        guard let currentLocation = parseLocation(from: mostRecentLocation) else { return }
        
        myLocationMarker.position = currentLocation
        myLocationMarker.map = mapView
        
        mapView.animate(toLocation: currentLocation)
        mapView.animate(toZoom: 18)
        mapView.selectedMarker = myLocationMarker
    }
    
    func parseLocation(from string: String) -> CLLocationCoordinate2D? {
        let splitLocation = string.components(separatedBy: " ")
        
        guard let latitude = Double(splitLocation[0]) else { return nil }
        guard let longitude = Double(splitLocation[1]) else { return nil }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

private extension SafeZoneViewC {
    @objc
    func handleUseCurrentButton() {
        guard let mostRecentLocation = session?.mostRecentLocation else { return }
        guard let currentLocation = parseLocation(from: mostRecentLocation) else { return }
        
        setSafeZone(from: currentLocation)
    }
    
    @objc
    func handleStartButton() {
        guard let location = safeZoneLocation else { return }
        
        delegate?.safeZoneDidComplete(location: location)
    }
    
    func setSafeZone(from address: String) {
        let geoCoder = CLGeocoder()
        
        print("geocoder getting coordinates for address: \(address)")
        
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location
            else {
                print("geocoder could not find location coordinates for address: \(address)")
                return
            }
            
            self.setSafeZone(from: location.coordinate)
        }
    }
    
    func setSafeZone(from coordinate: CLLocationCoordinate2D) {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.useCurrentButton.alpha = 0
            self.startButton.alpha = 1
        }, completion: nil)
        
        self.safeZoneLocation = coordinate
        self.showSafeZoneOnMap()
    }
    
    func showSafeZoneOnMap() {
        guard let position = safeZoneLocation else { return }
        
        safeZoneMarker.map = mapView
        safeZoneMarker.position = position
        safeZoneCircle[0].position = position
        safeZoneCircle[0].map = mapView
        safeZoneCircle[1].position = position
        safeZoneCircle[1].map = mapView
        
        mapView.animate(toLocation: position)
        mapView.animate(toZoom: 18)
    }
    
    func removeSafeZoneOnMap() {
        safeZoneMarker.map = nil
        safeZoneCircle[0].map = nil
        safeZoneCircle[1].map = nil
    }
    
    func dismissSearch() {
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
        
        tableView.data = []
    }
}

extension SafeZoneViewC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            tableView.data = []
            removeSafeZoneOnMap()
        } else {
            searchCompleter.queryFragment = searchText
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.useCurrentButton.alpha = 1
            self.startButton.alpha = 0
        }, completion: nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismissSearch()
    }
}

extension SafeZoneViewC: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        tableView.data = completer.results.map { ($0.title, $0.subtitle) }
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension SafeZoneViewC: AddressTableViewDelegate {
    func handleCellSelection(index: Int) {
        dismissSearch()
        let addressParts = tableView.data[index]
        let address = addressParts.0 + " " + addressParts.1
        searchBar.text = address
        setSafeZone(from: address)
    }
}
