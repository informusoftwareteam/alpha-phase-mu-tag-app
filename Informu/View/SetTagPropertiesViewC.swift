//
//  SetTagPropertiesViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/31/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material
import PromiseKit
import RealmSwift

@objc
protocol SetTagPropertiesViewCDelegate: class {
    @objc optional func register(muTag: MuTag)
    @objc optional func remove(muTag: MuTag)
    @objc optional func didCancelPropertyChange(for muTagId: String)
    @objc optional func changeMuTagPropertiesFailed(with error: Error, for muTagId: String)
}

class SetTagPropertiesViewC: UIViewController {
    fileprivate let realm = try! Realm()
    fileprivate let muTagParameters = try? MuTagParameters()
    
    weak var delegate: SetTagPropertiesViewCDelegate?
    
    fileprivate var muTag: MuTag?
    fileprivate var muTagId: String!
    fileprivate var editMode: Bool!
    fileprivate var attachedField: ErrorTextField!
    fileprivate var colorPicker: UIPickerView!
    fileprivate var expensiveLabel: UILabel!
    fileprivate var expensiveSwitch: Switch!
    fileprivate var irreplaceableLabel: UILabel!
    fileprivate var irreplaceableSwitch: Switch!
    fileprivate var removeButton: FABButton!
    fileprivate var removeLabel: UILabel!
    fileprivate var buttonContainer: UIView!
    fileprivate var completeButton: FlatButton!
    fileprivate var cancelButton: FlatButton!
    
    init(muTag: MuTag? = nil, muTagId: String) {
        self.muTag = muTag
        self.muTagId = muTagId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        determineEditMode()
        prepareAttachedField()
        prepareColorPicker()
        prepareExpensiveSwitch()
        prepareIrreplaceableSwitch()
        prepareCompleteButton()
        prepareRemoveButton()
        prepareButtonContainer()
        prepareCancelButton()
        prepareLayout()
        //Looks for single or multiple taps.
        //
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.screenWasTapped))
        
        self.view.addGestureRecognizer(tap)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        updateTagProperties()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
}

fileprivate extension SetTagPropertiesViewC {
    func prepareBackground() {
        view.backgroundColor = UIColor.init(red: 142/255, green: 192/255, blue: 179/255, alpha: 1)
    }
    
    func prepareAttachedField() {
        attachedField = ErrorTextField()
        attachedField.delegate = self
        attachedField.placeholder = "What is this mμ tag attached to?"
        attachedField.detail = "You must type something"
        attachedField.isClearIconButtonEnabled = true
        attachedField.textColor = Color.white
        attachedField.placeholderNormalColor = Color.grey.darken4
        attachedField.dividerNormalColor = Color.grey.darken4
        attachedField.leftViewNormalColor = Color.grey.darken4
        attachedField.placeholderActiveColor = Color.grey.darken4.withAlphaComponent(0.75)
        attachedField.dividerActiveColor = Color.grey.darken4.withAlphaComponent(0.75)
        attachedField.leftViewActiveColor = Color.grey.darken4.withAlphaComponent(0.75)
        
        let leftView = UIImageView()
        leftView.image = Icon.cm.pen
        attachedField.leftView = leftView
    }
    
    func prepareColorPicker() {
        colorPicker = UIPickerView()
        colorPicker.backgroundColor = Color.black.withAlphaComponent(0.05)
        colorPicker.dataSource = self
        colorPicker.delegate = self
    }
    
    func prepareExpensiveSwitch() {
        expensiveLabel = UILabel()
        expensiveLabel.text = "Expensive"
        expensiveLabel.numberOfLines = 1
        expensiveLabel.textAlignment = .left
        expensiveLabel.baselineAdjustment = .alignCenters
        expensiveLabel.textColor = Color.grey.darken4
        expensiveLabel.font = RobotoFont.regular(with: 18)
        expensiveLabel.adjustsFontSizeToFitWidth = true
        expensiveSwitch = Switch(state: .off, style: .dark, size: .medium)
        expensiveSwitch.buttonOnColor = Color.deepOrange.lighten2
        expensiveSwitch.trackOnColor = Color.deepOrange.lighten4
    }
    
    func prepareIrreplaceableSwitch() {
        irreplaceableLabel = UILabel()
        irreplaceableLabel.text = "Irreplaceable"
        irreplaceableLabel.numberOfLines = 1
        irreplaceableLabel.textAlignment = .left
        irreplaceableLabel.baselineAdjustment = .alignCenters
        irreplaceableLabel.textColor = Color.grey.darken4
        irreplaceableLabel.font = RobotoFont.regular(with: 18)
        irreplaceableLabel.adjustsFontSizeToFitWidth = true
        irreplaceableSwitch = Switch(state: .off, style: .light, size: .medium)
        irreplaceableSwitch.buttonOnColor = Color.deepOrange.lighten2
        irreplaceableSwitch.trackOnColor = Color.deepOrange.lighten4
    }
    
    func prepareRemoveButton() {
        removeLabel = UILabel()
        removeLabel.text = "Remove mµ tag from app"
        removeLabel.numberOfLines = 1
        removeLabel.textAlignment = .center
        removeLabel.baselineAdjustment = .alignCenters
        removeLabel.textColor = Color.grey.darken4
        removeLabel.font = RobotoFont.regular(with: 12)
        let removeImage = UIImage(named: "recycle-icon")!.withRenderingMode(.alwaysTemplate)
        removeButton = FABButton(image: removeImage, tintColor: Color.deepOrange.lighten2)
        removeButton.backgroundColor = Color.grey.darken4
        removeButton.addTarget(self, action: #selector(alertForRemoval), for: .touchUpInside)
    }
    
    func prepareButtonContainer() {
        buttonContainer = UIView()
        buttonContainer.backgroundColor = Color.grey.darken4
    }
    
    func prepareCompleteButton() {
        completeButton = FlatButton()
        if editMode {
            completeButton.title = "Save changes"
        } else {
            completeButton.title = "Add mμ tag"
        }
        completeButton.backgroundColor = Color.deepOrange.lighten2
        completeButton.titleColor = Color.white
        completeButton.titleLabel?.font = RobotoFont.regular(with: 18)
        completeButton.cornerRadiusPreset = .cornerRadius3
        completeButton.addTarget(self, action: #selector(handleCompleteButton), for: .touchUpInside)
    }
    
    func prepareCancelButton() {
        cancelButton = FlatButton(title: "Cancel")
        cancelButton.titleColor = Color.deepOrange.lighten2
        cancelButton.titleLabel?.font = RobotoFont.regular(with: 17)
        cancelButton.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
    }
    
    func determineEditMode() {
        if muTag?.attachedTo != "" {
            editMode = true
        } else {
            editMode = false
        }
    }
    
    func updateTagProperties() {
        guard let m = muTag else { return }
        
        attachedField.text = m.attachedTo
        colorPicker.selectRow(m.tagColor - 1, inComponent: 0, animated: false)
        expensiveSwitch.isOn = m.expensive
        irreplaceableSwitch.isOn = m.irreplaceable
    }
    
    @objc
    func screenWasTapped() {
        print("Screen was tapped")
        
        unfocusTextFields()
    }
    
    func unfocusTextFields() {
        attachedField?.resignFirstResponder()
    }
    
    @objc
    func handleCompleteButton() {
        if attachedField.text == "" {
            attachedField.isErrorRevealed = true
            return
        }
        if editMode {
            DispatchQueue.global(qos: .background).sync {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        guard let muTag = MuTag.get(with: muTagId) else { return }
                        try realm.write {
                            muTag.attachedTo = self.attachedField.text!
                            muTag.expensive = self.expensiveSwitch.isOn
                            muTag.irreplaceable = self.irreplaceableSwitch.isOn
                            muTag.tagColor = MuTagColor.allValues[self.colorPicker.selectedRow(inComponent: 0)].rawValue
                        }
                    } catch {
                        self.delegate?.changeMuTagPropertiesFailed?(with: error, for: muTagId)
                        dismiss(animated: true, completion: nil)
                    }
                }
            }
            dismiss(animated: true, completion: nil)
            //delegate?.didUpdateMuTag?()
        } else {
            guard let muTag = muTag else { return }
            if muTag.isInvalidated { return }
            muTag.attachedTo = attachedField.text!
            muTag.expensive = expensiveSwitch.isOn
            muTag.irreplaceable = irreplaceableSwitch.isOn
            muTag.tagColor = MuTagColor.allValues[colorPicker.selectedRow(inComponent: 0)].rawValue
            let randomParameterSet = RandomParameterSet()
            if let muTagParameters = muTagParameters {
                muTagParameters.getGlobalAdvertisingInterval().then { advertisingInterval -> Promise<Int> in
                    if advertisingInterval != 0 {
                        muTag.advertisingInterval = advertisingInterval
                    }
                    return muTagParameters.getGlobalTxPower()
                }.then { txPower -> Void in
                    if txPower != 0 {
                        muTag.txPower = txPower
                    }
                    self.delegate?.register?(muTag: muTag)
                }.catch { error in
                    print(error)
                    muTag.advertisingInterval = randomParameterSet.advertisingInterval
                    muTag.txPower = randomParameterSet.txPower
                    self.delegate?.register?(muTag: muTag)
                }
            } else {
                muTag.advertisingInterval = randomParameterSet.advertisingInterval
                muTag.txPower = randomParameterSet.txPower
                delegate?.register?(muTag: muTag)
            }
            
        }
    }
    
    @objc
    func handleCancelButton() {
        delegate?.didCancelPropertyChange?(for: muTagId)
        if editMode {
            dismiss(animated: true, completion: nil)
        } else {
            presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }

    func prepareLayout() {
        view.layout(attachedField).top(80).left(25).right(30)
        view.layout(colorPicker).top(150).left().right().height(160)
        view.layout(expensiveLabel).center(offsetY: 20).left(40)
        view.layout(expensiveSwitch).center(offsetY: 20).right(50).width(40)
        view.layout(irreplaceableLabel).center(offsetY: 60).left(40)
        view.layout(irreplaceableSwitch).center(offsetY: 60).right(50).width(40)
        if editMode {
            view.layout(removeButton).width(50).height(50).center(offsetY: 130)
            view.layout(removeLabel).center(offsetY: 170)
        }
        view.layout(buttonContainer).bottom().left().right().height(130)
        buttonContainer.layout(completeButton).horizontally(left: 30, right: 30)
            .height(50).top(20)
        buttonContainer.layout(cancelButton).centerHorizontally().top(80)
    }
    
    @objc
    func alertForRemoval() {
        let alertController = UIAlertController(title: "Remove your mµ tag", message: "Are you sure you want to erase and remove this mµ tag from your account? Don't worry, you will be able to add it again on any account you wish. Removing a mµ tag will return it to it's factory default state.", preferredStyle: .alert)
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) {
            (result : UIAlertAction) -> Void in
            guard let muTag = MuTag.get(with: self.muTagId) else { return }
            self.delegate?.remove?(muTag: muTag)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension SetTagPropertiesViewC: TextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        return
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        return
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
}

extension SetTagPropertiesViewC: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return MuTagColor.allValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 36
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = Color.white//grey.darken3
        pickerLabel.text = MuTagColor.allValues[row].stringValue
        pickerLabel.font = RobotoFont.regular(with: 18)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
}
