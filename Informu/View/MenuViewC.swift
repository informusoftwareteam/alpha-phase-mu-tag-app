//
//  MenuViewController.swift
//  Scrip
//
//  Created by Tom Daniel D. on 3/8/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import UIKit
import Material

protocol MenuViewCDelegate: class {
    func updateActiveViewC(screen: MainScreen)
    func logout()
}

class MenuViewC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    weak var delegate: MenuViewCDelegate?
    
    fileprivate var tableView: UITableView!
    fileprivate var items: [String] = ["Logout"]
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        prepareTableView()
        prepareLayout()
    }
}

extension MenuViewC {
    fileprivate func prepareView() {
        view.backgroundColor = Color.grey.darken4
    }
    
    fileprivate func prepareTableView() {
        tableView = UITableView()
        //tableView.frame         =   CGRect(x: 0, y: 50, width: 320, height: 500);
        tableView.delegate      =   self
        tableView.dataSource    =   self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        //tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        
        tableView.backgroundColor = Color.clear
        tableView.separatorColor = Color.grey.darken3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let rect = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44)
        let footerView = UIView(frame:rect)
        footerView.backgroundColor = Color.grey.darken3
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0//140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[indexPath.row]
        cell.textLabel?.textColor = Color.grey.lighten3
        cell.textLabel?.font = UIFont(name: "Roboto-Light", size: 30)
        
        cell.backgroundColor = Color.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        cellToDeSelect.contentView.backgroundColor = Color.grey.darken2
        
        if items[indexPath.row] == "Logout" {
            delegate?.logout()
        } else {
            guard let selectedScreen = MainScreen(rawValue: items[indexPath.row]) else { return }
            
            delegate?.updateActiveViewC(screen: selectedScreen)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        cellToDeSelect.contentView.backgroundColor = Color.clear
    }
}

extension MenuViewC {
    fileprivate func prepareLayout() {
        view.layout(tableView).left().right().top().bottom()
    }
}
