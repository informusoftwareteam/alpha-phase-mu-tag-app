/*
 * Copyright (C) 2015 - 2017, Daniel Dahan and CosmicMind, Inc. <http://cosmicmind.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *	*	Redistributions of source code must retain the above copyright notice, this
 *		list of conditions and the following disclaimer.
 *
 *	*	Redistributions in binary form must reproduce the above copyright notice,
 *		this list of conditions and the following disclaimer in the documentation
 *		and/or other materials provided with the distribution.
 *
 *	*	Neither the name of CosmicMind nor the names of its
 *		contributors may be used to endorse or promote products derived from
 *		this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import UIKit
import Material
import RealmSwift

class MuTagTableViewCell: TableViewCell {
    fileprivate let realm = try! Realm()
    
    private var spacing: CGFloat = 14
    
    /// A boolean that indicates whether the cell is the last cell.
    public var isLast = false
    
    public lazy var card: Card = Card()
    
    /// Toolbar views.
    private var toolbar: Toolbar!
    //private var moreButton: IconButton!
    private var deviceIcon: UIView!
    
    /// Bottom Bar views.
    private var bottomBar: Bar!
    private var locationView: UIView!
    private var locationLabel: UILabel!
    private var proximityView: UIView!
    private var proximityLabel: UILabel!
    private var gradient: CAGradientLayer!
    private var muTagIconView: UIImageView?
    
    private var notificationToken : NotificationToken?
    
    public var muTagId: String? {
        didSet {
            prepareRealmNotification()
            layoutSubviews()
        }
    }
    
    /// Calculating dynamic height.
    var height: CGFloat {
        get {
            return card.frame.height + spacing
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if let muTagId = muTagId {
            guard let muTag = MuTag.get(with: muTagId) else { return }
            toolbar.title = "   " + muTag.attachedTo
            let splitLocationName = muTag.recentLocationName.components(separatedBy: ":")
            if splitLocationName.count == 3 {
                locationLabel.text = splitLocationName[1] + splitLocationName[2]
            } else {
                locationLabel.text = muTag.recentLocationName.replacingOccurrences(of: ":", with: "")
            }
            let proximity: String = {
                if muTag.unsafe {
                    proximityLabel.textColor = Color.red.lighten2
                    return "unsafe"
                } else {
                    proximityLabel.textColor = Color.grey.base
                    return muTag.proximityString
                }
            }()
            proximityLabel.text = proximity
            setIconColor()
        }

        card.updateConstraints()
        card.setNeedsLayout()
        card.layoutIfNeeded()
    }
    
    open override func prepare() {
        super.prepare()
        
        layer.rasterizationScale = Screen.scale
        layer.shouldRasterize = true
        
        pulseAnimation = .none
        backgroundColor = nil
        
        prepareLocationLabel()
        prepareProximityLabel()
        prepareDeviceIcon()
        prepareToolbar()
        prepareBottomBar()
        preparePresenterCard()
        prepareGradient()
        layoutSubviews()
    }
    
    open override func prepareForReuse() {
        super.prepareForReuse()
        //notificationToken?.invalidate()
    }
    
    fileprivate func prepareRealmNotification() {
        guard let muTagId = muTagId, let persistedMuTag = MuTag.get(with: muTagId) else { return }
        notificationToken = persistedMuTag.observe { [weak self] change in
            switch change {
            case .change:
                self?.layoutSubviews()
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("MuTagTableViewCell.prepareRealmNotification() - The Realm 'muTag' object \(self?.muTagId ?? "") was deleted.")
                self?.notificationToken?.invalidate()
            }
        }
    }
    
    fileprivate func setIconColor() {
        guard let muTagIcon = muTagIconView else { return }
        guard let muTagId = muTagId, let muTag = MuTag.get(with: muTagId) else { return }
        guard let muTagColorValue = MuTagColor(rawValue: muTag.tagColor)?.colorValue else { return }
        muTagIcon.tintColor = muTagColorValue
    }
    
    private func prepareLocationLabel() {
        locationView = UIView()
        locationLabel = UILabel()
        locationLabel.sizeToFit()
        locationLabel.font = RobotoFont.regular(with: 16)
        locationLabel.numberOfLines = 1
        locationLabel.textColor = Color.grey.base
        locationLabel.textAlignment = .left
        
        locationView.layout.centerVertically(locationLabel).left()
    }
    
    private func prepareProximityLabel() {
        proximityView = UIView()
        proximityLabel = UILabel()
        proximityLabel.sizeToFit()
        proximityLabel.font = RobotoFont.medium(with: 20)
        proximityLabel.numberOfLines = 1
        proximityLabel.textColor = Color.grey.base
        
        proximityView.layout.centerVertically(proximityLabel).right(8)
    }
    
    private func prepareDeviceIcon() {
        var image = UIImage(named: "mutag-angle-icon")!
        image = image.withRenderingMode(.alwaysTemplate)
        muTagIconView = UIImageView(image: image)
        muTagIconView!.contentMode = .scaleAspectFit
        //muTagIconView.tintColor = Color.white
        deviceIcon = UIView()
        
        deviceIcon.layout(muTagIconView!).center()
    }
    
    private func prepareToolbar() {
        toolbar = Toolbar()
        toolbar.heightPreset = .normal
        toolbar.contentEdgeInsetsPreset = .horizontally4
        toolbar.titleLabel.textAlignment = .left
        toolbar.titleLabel.font = RobotoFont.light(with: 28)
        toolbar.titleLabel.numberOfLines = 1
        toolbar.titleLabel.textColor = Color.grey.darken3
        toolbar.leftViews = [deviceIcon]
        
        toolbar.backgroundColor = nil
    }
    
    private func prepareBottomBar() {
        bottomBar = Bar()
        bottomBar.heightPreset = .xsmall
        bottomBar.contentEdgeInsets.left = 73
        bottomBar.leftViews = [locationView]
        bottomBar.rightViews = [proximityView]
        
        bottomBar.backgroundColor = nil
    }
    
    private func prepareGradient() {
        gradient = CAGradientLayer()
        gradient.colors = [Color.grey.lighten1.cgColor, Color.grey.lighten4.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: card.frame.height - 23, height: card.frame.height)
        gradient.cornerRadiusPreset = .cornerRadius3
        
        card.container.layer.insertSublayer(gradient, at: 0)
    }
    
    private func preparePresenterCard() {
        card.toolbar = toolbar
        card.bottomBar = bottomBar
        card.bottomBarEdgeInsets.bottom = 8
        card.depthPreset = .depth2
        card.cornerRadiusPreset = .cornerRadius3
        card.backgroundColor = Color.white

        contentView.layout(card).horizontally(left: 8, right: 8).top(18)
    }
}
