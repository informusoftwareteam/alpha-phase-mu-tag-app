//
//  TutorialPage4ViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/7/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material

protocol TutorialPage4ViewCDelegate: class {
    func continueToApp()
}

class TutorialPage4ViewC: UIViewController {
    weak var delegate: TutorialPage4ViewCDelegate?
    
    fileprivate var titleLabel: UILabel!
    fileprivate var subText: UILabel!
    fileprivate var buttonContainer: UIView!
    fileprivate var startButton: FlatButton!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        preparetitleLabel()
        prepareSubText()
        prepareStartButton()
        prepareLayout()
    }
}

private extension TutorialPage4ViewC {
    func prepareBackground() {
        view.backgroundColor = UIColor.init(red: 142/255, green: 192/255, blue: 179/255, alpha: 1)
    }
    
    func preparetitleLabel() {
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.text = "Let's setup a mµ tag!"
        titleLabel.textAlignment = .center
        titleLabel.baselineAdjustment = .alignCenters
        titleLabel.textColor = Color.white
        titleLabel.font = RobotoFont.light(with: 40)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func prepareSubText() {
        subText = UILabel()
        subText.numberOfLines = 0
        subText.text = "Go to the next page to begin"
        subText.textAlignment = .center
        subText.baselineAdjustment = .alignCenters
        subText.textColor = Color.grey.darken4
        subText.font = RobotoFont.light(with: 30)
        subText.adjustsFontSizeToFitWidth = true
    }
    
    func prepareStartButton() {
        buttonContainer = UIView()
        buttonContainer.backgroundColor = Color.black
        startButton = FlatButton(title: "Continue to app")
        startButton.backgroundColor = Color.grey.darken4
        startButton.titleColor = Color.white
        startButton.cornerRadiusPreset = .cornerRadius3
        startButton.addTarget(self, action: #selector(handleStartButton), for: .touchUpInside)
    }
    
    func prepareLayout() {
        view.layout(titleLabel).top(100).left(10).right(10)
        view.layout(subText).left(20).right(20).centerVertically()
        view.layout(buttonContainer).bottom().left().right().height(40)
        buttonContainer.layout(startButton).bottom().left(30).right(30).height(30)
    }
}

private extension TutorialPage4ViewC {
    @objc
    func handleStartButton() {
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { timer in
            self.delegate?.continueToApp()
        }
    }
}
