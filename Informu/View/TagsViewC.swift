//
//  TagsViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import UIKit
import Material
import PromiseKit
import RealmSwift

protocol TagsViewCDelegate: class {
    //var tagListArray: Array<MuTag> { get }
    //func getTags() -> Array<MuTag>
    func addTags()
    func handleTagSelection(muTagId: String) throws
}

class TagsViewC: UIViewController {
    
    weak var delegate: TagsViewCDelegate?
    var tableView: TagsTableView!
    var menuButton: IconButton?
    fileprivate var addTagsButton: IconButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        prepareTabItem()
        prepareTableView()
        tableView.tagsTableViewDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareAddTagsButton()
        prepareToolbar()
    }
}

fileprivate extension TagsViewC {
    func prepareBackground() {
        view.backgroundColor = UIColor.init(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
        let image = UIImage(named: "mu-icon")!
        let backgroundImage = UIImageView(image: image)
        backgroundImage.image = backgroundImage.image!.withRenderingMode(.alwaysTemplate)
        backgroundImage.tintColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1)
        backgroundImage.contentMode = .scaleAspectFill
        view.layout(backgroundImage).edges()
    }
    
    func prepareTabItem() {
        tabItem.image = UIImage(named: "mutag-icon")!
        tabItem.image = tabItem.image!.withRenderingMode(.alwaysTemplate)
    }

    func prepareTableView() {
        tableView = TagsTableView()
        tableView.showsHorizontalScrollIndicator = false
        
        view.layout(tableView).edges()
    }
    
    @objc
    func handleAddTagsButton() {
        delegate?.addTags()
    }
}

// Toolbar
fileprivate extension TagsViewC {
    func prepareAddTagsButton() {
        addTagsButton = IconButton(image: Icon.cm.add, tintColor: .white)
        addTagsButton.pulseColor = .white
        addTagsButton.addTarget(self, action: #selector(handleAddTagsButton), for: .touchUpInside)
    }
    
    func prepareToolbar() {
        guard let tc = toolbarController else { return }
        // TODO: Fade animations
        tc.toolbar.title = "my mμ tags"
        tc.toolbar.titleLabel.textColor = Color.white
        tc.toolbar.rightViews = [addTagsButton]
    }
}

extension TagsViewC: TagsTableViewProtocol {
    func handleCellSelection(muTagId: String) {
        do {
            try delegate?.handleTagSelection(muTagId: muTagId)
        } catch {
            print(error)
        }
    }
}
