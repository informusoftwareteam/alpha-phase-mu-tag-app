//
//  ProgressView.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/15/17.
//  Copyright © 2017 informu. All rights reserved.
//

import UIKit
import Material
import UICircularProgressRing

class ProgressView: UIView {
    fileprivate let titleLabel = UILabel()
    fileprivate let progressRing = UICircularProgressRingView(frame: CGRect.zero)
    fileprivate let subtextView = UIView()
    fileprivate let subtext = UILabel()
    
    fileprivate var backgroundBlurView: UIVisualEffectView!
    fileprivate var vibrancyView: UIVisualEffectView!
    fileprivate var attachedTo: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareBackground()
        prepareTitle()
        prepareProgressRing()
        prepareSubtext()
        prepareLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setMuTag(attachedTo: String) {
        self.attachedTo = attachedTo
        
        // DEBUG
        print("DEBUG - attachedTo \(self.attachedTo)")
    }
    
    func setProgress(to value: CGFloat, speed: Double = 0.5, completion: (() -> Void)? = nil) {
        progressRing.setProgress(value: value, animationDuration: speed, completion: completion)
    }
}

private extension ProgressView {
    func prepareBackground() {
        backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .dark)
        backgroundBlurView = UIVisualEffectView(effect: blurEffect)
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
    }
    
    func prepareTitle() {
        titleLabel.numberOfLines = 0
        let message: String = {
            let text = "Update in progress for mµ tag"
            guard let attachedTo = attachedTo else {
                return text + "..."
            }
            return text + " attached to \(attachedTo)..."
        }()
        titleLabel.text = message
        titleLabel.textAlignment = .center
        titleLabel.baselineAdjustment = .alignCenters
        titleLabel.textColor = Color.white
        titleLabel.font = RobotoFont.light(with: 32)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func prepareProgressRing() {
        progressRing.outerRingColor = Color.deepOrange.lighten2
        progressRing.innerRingColor = UIColor.white
        progressRing.shouldShowValueText = false
    }
    
    func prepareSubtext() {
        subtextView.backgroundColor = Color.black.withAlphaComponent(0.1)
        subtext.numberOfLines = 0
        subtext.text = "WARNING: Do not disrupt this process in any way"
        subtext.textAlignment = .center
        subtext.baselineAdjustment = .alignCenters
        subtext.textColor = Color.white
        subtext.font = RobotoFont.light(with: 16)
        subtext.adjustsFontSizeToFitWidth = true
    }
    
    func prepareLayout() {
        vibrancyView.contentView.layout(titleLabel).top(40).left(15).right(15)
        vibrancyView.contentView.layout(progressRing).center().width(200).height(200)
        backgroundBlurView.contentView.layout(vibrancyView).edges()
        subtextView.layout(subtext).centerVertically().left(15).right(15)
        backgroundBlurView.contentView.layout(subtextView).bottom().left().right().height(100)
        layout(backgroundBlurView).edges()
    }
}
