//
//  AddTagsViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 8/2/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material
import PromiseKit

protocol AddTagsViewCDelegate: class {
    func didCancelAddMuTag()
}

class AddTagsViewC: UIViewController {
    weak var delegate: AddTagsViewCDelegate?
    
    fileprivate var muTag: MuTag?
    fileprivate var backgroundImage: UIImageView!
    fileprivate var messageTitle: UILabel!
    fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var buttonContainer: UIView!
    fileprivate var cancelButton: FlatButton!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        prepareMessageTitle()
        prepareActivityIndicator()
        prepareCancelButton()
        prepareLayout()
    }

    open override func viewWillLayoutSubviews() {
        messageTitle.sizeToFit()
    }
    
    /*func didConnectToNewMuTag(muTag: MuTag) {
        delegate?.didConnectToNewMuTag(muTag: muTag)
    }
    
    func newMuTagAdded() {
        handleCancelButton()
    }*/
}

fileprivate extension AddTagsViewC {
    func prepareBackground() {
        let image = UIImage(named: "add-mutag")!
        backgroundImage = UIImageView(image: image)
        backgroundImage.contentMode = .scaleAspectFill
    }
    
    func prepareMessageTitle() {
        messageTitle = UILabel()
        messageTitle.text = "Scanning for new mμ tag"
        messageTitle.numberOfLines = 2
        messageTitle.textAlignment = .center
        messageTitle.baselineAdjustment = .alignCenters
        messageTitle.textColor = Color.white
        messageTitle.font = RobotoFont.light(with: 40)
        messageTitle.adjustsFontSizeToFitWidth = true
    }
    
    func prepareActivityIndicator() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.startAnimating()
    }
    
    func prepareCancelButton() {
        buttonContainer = UIView()
        buttonContainer.backgroundColor = Color.black
        cancelButton = FlatButton(title: "Cancel")
        cancelButton.backgroundColor = Color.grey.darken4
        cancelButton.titleColor = Color.white
        cancelButton.cornerRadiusPreset = .cornerRadius3
        cancelButton.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
    }
    
    func prepareLayout() {
        view.layout(backgroundImage).edges()
        view.layout(messageTitle).top(40).left(10).right(70)
        view.layout(activityIndicator).top(78).right(20)
        view.layout(buttonContainer).bottom().left().right().height(40)
        buttonContainer.layout(cancelButton).bottom().left(30).right(30).height(30)
        //view.layout(cancelButton).horizontally(left: 20, right: 20).bottom(40).height(60)
    }
    
    @objc
    func handleCancelButton() {
        delegate?.didCancelAddMuTag()
        self.dismiss(animated: true, completion: nil)
    }
}
