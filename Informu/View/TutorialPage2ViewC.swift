//
//  TutorialPage2ViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/25/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material

protocol TutorialPage2ViewCDelegate: class {
    func continueToApp()
}

class TutorialPage2ViewC: UIViewController {
    weak var delegate: TutorialPage2ViewCDelegate?
    
    fileprivate var backgroundImage: UIImageView!
    fileprivate var titleLabel: UILabel!
    fileprivate var buttonContainer: UIView!
    fileprivate var startButton: FlatButton!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        preparetitleLabel()
        prepareStartButton()
        prepareLayout()
    }
}

private extension TutorialPage2ViewC {
    func prepareBackground() {
        let image = UIImage(named: "tutorial2")!
        backgroundImage = UIImageView(image: image)
        backgroundImage.contentMode = .scaleAspectFill
    }
    
    func preparetitleLabel() {
        titleLabel = UILabel()
        titleLabel.numberOfLines = 2
        titleLabel.text = "Never lose your belongings again"
        titleLabel.textAlignment = .center
        titleLabel.baselineAdjustment = .alignCenters
        titleLabel.textColor = Color.white
        titleLabel.font = RobotoFont.light(with: 40)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func prepareStartButton() {
        buttonContainer = UIView()
        buttonContainer.backgroundColor = Color.black
        startButton = FlatButton(title: "Continue to app")
        startButton.pulseColor = Color.white
        startButton.backgroundColor = Color.grey.darken4
        startButton.titleColor = Color.white
        startButton.cornerRadiusPreset = .cornerRadius3
        startButton.addTarget(self, action: #selector(handleStartButton), for: .touchUpInside)
    }
    
    func prepareLayout() {
        view.layout(backgroundImage).edges()
        view.layout(titleLabel).top(50).left(10).right(10)
        view.layout(buttonContainer).bottom().left().right().height(40)
        buttonContainer.layout(startButton).bottom().left(30).right(30).height(30)
    }
}

private extension TutorialPage2ViewC {
    @objc
    func handleStartButton() {
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { timer in
            self.delegate?.continueToApp()
        }
    }
}
