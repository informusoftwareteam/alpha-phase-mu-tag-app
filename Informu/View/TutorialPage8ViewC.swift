//
//  TutorialPage7ViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/8/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import Material

protocol TutorialPage8ViewCDelegate: class {
    func configureSafeZone()
}

class TutorialPage8ViewC: UIViewController {
    weak var delegate: TutorialPage8ViewCDelegate?
    
    fileprivate var titleLabel: UILabel!
    fileprivate var subText: UILabel!
    fileprivate var subTextContainer: UIView!
    fileprivate var safeZoneButton: FlatButton!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareBackground()
        preparetitleLabel()
        prepareSubText()
        preparesafeZoneButton()
        prepareLayout()
    }
}

private extension TutorialPage8ViewC {
    func prepareBackground() {
        view.backgroundColor = UIColor.init(red: 142/255, green: 192/255, blue: 179/255, alpha: 1)
    }
    
    func preparetitleLabel() {
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.text = "One more thing and you're ready to go"
        titleLabel.textAlignment = .center
        titleLabel.baselineAdjustment = .alignCenters
        titleLabel.textColor = Color.white
        titleLabel.font = RobotoFont.light(with: 40)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func prepareSubText() {
        subTextContainer = UIView()
        subTextContainer.backgroundColor = Color.black.withAlphaComponent(0.5)
        subText = UILabel()
        subText.numberOfLines = 0
        subText.text = "On the next page you will setup your safe zone. What is a safe zone? It's a place where you consider your belongings safe and will not receive notifications. Don't worry, the app will still show you the current status of all your mµ tags."
        subText.textAlignment = .left
        subText.baselineAdjustment = .alignCenters
        subText.textColor = Color.white
        subText.font = RobotoFont.light(with: 20)
        subText.adjustsFontSizeToFitWidth = true
    }
    
    func preparesafeZoneButton() {
        safeZoneButton = FlatButton(title: "Setup safe zone")
        safeZoneButton.backgroundColor = Color.deepOrange.lighten2
        safeZoneButton.titleColor = Color.white
        safeZoneButton.cornerRadiusPreset = .cornerRadius3
        safeZoneButton.addTarget(self, action: #selector(handlesafeZoneButton), for: .touchUpInside)
    }
    
    func prepareLayout() {
        view.layout(titleLabel).top(40).left(10).right(10)
        view.layout(subTextContainer).top(220).left().right().bottom()
        subTextContainer.layout(subText).top(10).left(20).right(20).bottom(100)
        view.layout(safeZoneButton).bottom(20).left(30).right(30).height(60)
    }
}

private extension TutorialPage8ViewC {
    @objc
    func handlesafeZoneButton() {
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { timer in
            self.delegate?.configureSafeZone()
        }
    }
}
