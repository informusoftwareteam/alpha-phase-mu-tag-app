//
//  LoginController.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol LoginControllerDelegate: class {
    func loginDidComplete(withSavedSession: Bool)
}

class LoginController: NSObject, ActiveController {
    var viewController: UIViewController
    // BUG: 'weak var' is not working here for some reason. Temporarily removing 'weak'
    //
    var delegate: LoginControllerDelegate?
    
    let loginViewC: LoginViewC
    let login: Login
    
    init(muTagRepo: MuTagRepository) throws {
        self.loginViewC = LoginViewC()
        self.viewController = self.loginViewC
        self.login = try Login(muTagRepo: muTagRepo)
        
        super.init()
        
        loginViewC.delegate = self
    }
    
    func tryLoginFromSavedSession() {
        loginViewC.changeScreenState(to: .loading(nil))
        
        // Determine if a login session has been saved or if the login screen should be presented
        //
        login.authenticateFromSession().then { () -> Void in
            self.delegate?.loginDidComplete(withSavedSession: true)
            }.catch { error in
                print(error)
                
                switch error {
                case AuthenticationError.sessionLoginDataNotFound: return
                case AuthenticationError.facebookAccessTokenNotFound: return
                default:
                    self.loginViewC.presentErrorMessage(message: error.localizedDescription)
                }
            }.always {
                self.loginViewC.changeScreenState(to: .previous)
        }
    }
}

// MARK: - LoginViewCDelegate Conformance
extension LoginController: LoginViewCDelegate {
    /*func handleLoginButton(username: String, password: String) {
        loginViewC.changeScreenState(to: .loading(nil))
        
        login.authenticate(username: username, password: password).then {
            self.delegate?.loginDidComplete(withSavedSession: false)
        }.catch { error in
            self.loginViewC.presentErrorMessage(message: error.localizedDescription)
        }.always {
            self.loginViewC.changeScreenState(to: .previous)
        }
    }
    
    func handleCreateAccountButton(username: String, password: String, passwordVerify: String) {
        loginViewC.changeScreenState(to: .loading(nil))
        
        login.create(username: username, password: password, passwordVerify: passwordVerify).then {
            self.delegate?.loginDidComplete(withSavedSession: false)
        }.catch { error in
            if error.localizedDescription == "The passwords you entered do no match" {
                self.loginViewC.passwordField.text = nil
                self.loginViewC.passwordVerifyField.text = nil
                _ = self.loginViewC.passwordField.becomeFirstResponder()
            }

            self.loginViewC.presentErrorMessage(message: error.localizedDescription)
        }.always {
            self.loginViewC.changeScreenState(to: .previous)
        }
    }*/
    
    func handleFacebookButton() {
        loginViewC.changeScreenState(to: .loading(nil))
        
        login.signInWithFacebook(viewController: viewController).then {
            self.delegate?.loginDidComplete(withSavedSession: false)
        }.catch { error in
            self.loginViewC.presentErrorMessage(message: error.localizedDescription)
        }.always {
            self.loginViewC.changeScreenState(to: .previous)
        }
    }
    
    func handleGoogleButton() {
        loginViewC.changeScreenState(to: .loading(nil))
        
        login.signInWithGoogle().then {
            self.delegate?.loginDidComplete(withSavedSession: false)
        }.catch { error in
            self.loginViewC.presentErrorMessage(message: error.localizedDescription)
        }.always {
            self.loginViewC.changeScreenState(to: .previous)
        }
    }
}
