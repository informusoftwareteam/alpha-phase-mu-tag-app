//
//  ProgressViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/15/17.
//  Copyright © 2017 informu. All rights reserved.
//

//import UIKit
import Material

class ProgressViewC: UIViewController {
    fileprivate let mainView = ProgressView(frame: CGRect.zero)
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
    
    func setMuTag(attachedTo: String) {
        mainView.setMuTag(attachedTo: attachedTo)
    }
    
    func updateProgress(to value: CGFloat, speed: Double = 0.5, completion: (() -> Void)? = nil) {
        mainView.setProgress(to: value, speed: speed, completion: completion)
    }
}

private extension ProgressViewC {
    func prepareView() {
        view.backgroundColor = .clear
        view.layout(mainView).edges()
    }
}
