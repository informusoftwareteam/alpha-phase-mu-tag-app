//
//  SettingsController.swift
//  Scrip
//
//  Created by Tom Daniel D. on 6/1/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import Foundation
import UIKit

class SettingsController: ActiveController {
    
    var viewController: UIViewController
    
    let settingsVC = SettingsViewC()
    
    init() {
        self.viewController = settingsVC
        
        // Set delegates
        //settingsVC.delegate = self
        //settingsVC.keypad.delegate = self
    }
    
}
