//
//  MapController.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import MapKit
import Material

class MapController: ActiveController {
    
    var viewController: UIViewController
    fileprivate let mapFabMenuViewC: MapFABMenuViewC
    private let mapViewC = MapViewC()
    
    var unsafeMuTagSelected: MuTag?
    
    init() {
        self.mapFabMenuViewC = MapFABMenuViewC(rootViewController: self.mapViewC)
        self.viewController = self.mapFabMenuViewC
        
        // Set delegates
        //
        mapViewC.delegate = self
        mapFabMenuViewC.delegate = self
    }
    
}

private extension MapController {
    func openNavigation(to coordinate: CLLocationCoordinate2D) {
        let googleMapsInstalled = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        if googleMapsInstalled {
            let googleMapsUrl = URL(string: "comgooglemaps-x-callback://" +
                "?daddr=\(coordinate.latitude),\(coordinate.longitude)&directionsmode=driving&zoom=17")!
            UIApplication.shared.open(googleMapsUrl)
        } else {
            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.openInMaps(launchOptions: nil)
        }
    }
    
    func parseLocation(from string: String) -> CLLocationCoordinate2D? {
        let splitLocation = string.components(separatedBy: " ")
        
        guard let latitude = Double(splitLocation[0]) else { return nil }
        guard let longitude = Double(splitLocation[1]) else { return nil }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

extension MapController: MapViewCDelegate {
    func selectedUnsafeMuTag(muTag: MuTag) {
        unsafeMuTagSelected = muTag
        
        //mapFabMenuViewC.fabButton.tintColor = Color.grey.darken3
        mapFabMenuViewC.fabMenu.isHidden = false
    }
    
    func removedSelection() {
        mapFabMenuViewC.fabMenu.isHidden = true
    }
}

extension MapController: MapFABMenuViewCDelegate {
    func selectedNavigation() {
        guard let location = unsafeMuTagSelected?.recentLocation else { return }
        guard let coordinate = parseLocation(from: location) else { return }
        
        openNavigation(to: coordinate)
    }
}
