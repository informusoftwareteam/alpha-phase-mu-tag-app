//
//  TagController.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit
import RealmSwift

enum TagsControllerError: Error {
    case muTagNotFoundForId
}

protocol TagsControllerDelegate: class {
    func goToMap()
}

extension NSNotification.Name {
    static let muTagReadyForRemoval = NSNotification.Name("muTagReadyForRemoval")
}

class TagsController: ActiveController {
    fileprivate let notificationCenter = NotificationCenter.default
    
    weak var delegate: TagsControllerDelegate?
    
    var debugTags: List<MuTag>?
    
    let viewController: UIViewController
    let tagsViewC: TagsViewC
    let addTagsViewC: AddTagsViewC
    //let setTagPropertiesViewC: SetTagPropertiesViewC
    let muTagRepo: MuTagRepository
    let muTagBleSessionManager = MuTagBleSessionManager.shared
    fileprivate let locationService = LocationService.shared
    let muTagFactory: MuTagFactory
    
    fileprivate var tagDetailViewC: TagDetailViewC?
    
    init(muTagRepo: MuTagRepository, muTagFactory: MuTagFactory) {
        self.tagsViewC = TagsViewC()
        self.viewController = self.tagsViewC
        //self.setTagPropertiesViewC = SetTagPropertiesViewC()
        self.addTagsViewC = AddTagsViewC()
        self.muTagRepo = muTagRepo
        self.muTagFactory = muTagFactory
        
        self.muTagBleSessionManager.delegate = self
        self.tagsViewC.delegate = self
        self.addTagsViewC.delegate = self
    }
    
    /*func updateMuTags(muTags: List<MuTag>? = nil) {
        if let muTags = muTags {
            // DEBUG
            print("TagsController.updateMuTags received mμ tags (\(muTags))")
        } else {
            //tagsViewC.updateTags()
        }
    }
    
    func updateNewMuTags(muTags: Array<MuTag>) {
        //addTagsViewC.updateNewTags(tags: muTags)
    }*/
}

extension TagsController: MuTagBleSessionManagerDelegate {
    func didConnectToNewMuTag(muTag: MuTag, bleDispatcher: MuTagBleDispatcher) {
        let setTagPropertiesViewC = SetTagPropertiesViewC(muTag: muTag, muTagId: muTag.id)
        setTagPropertiesViewC.delegate = self
        viewController.presentedViewController?.present(setTagPropertiesViewC, animated: true)
    }
}

extension TagsController: TagsViewCDelegate {
    func addTags() {
        muTagBleSessionManager.scanForNewMuTagAndConnect()
        viewController.present(addTagsViewC, animated: true)
    }
    
    func handleTagSelection(muTagId: String) throws {
        let realm = try Realm()
        let setTagPropertiesViewC = SetTagPropertiesViewC(muTagId: muTagId)
        setTagPropertiesViewC.delegate = self
        tagDetailViewC = try TagDetailViewC(setTagPropertiesViewC: setTagPropertiesViewC, muTagId: muTagId)
        tagDetailViewC?.delegate = self
        if let app = UIApplication.shared.delegate as? AppDelegate {
            let transition = CATransition()
            transition.duration = 0.25;
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            app.window.layer.add(transition, forKey: nil)
        }
        if let vc = tagDetailViewC {
            viewController.present(vc, animated: false)
        }
    }
}

extension TagsController: AddTagsViewCDelegate {    
    func didCancelAddMuTag() {
        muTagBleSessionManager.stopScanningForNewMuTag()
    }
}

extension TagsController: TagDetailViewCDelegate {
    func goToMap() {
        delegate?.goToMap()
    }
    
    func viewWillDisappearComplete() {
        notificationCenter.post(name: .muTagReadyForRemoval, object: nil)
    }
}

extension TagsController: SetTagPropertiesViewCDelegate {
    func register(muTag: MuTag) {
        muTag.provision().then { _ -> Promise<Void> in
            print("Successfully provisioned mµ tag device")
            muTag.updateStatusToProvisioned()
            return self.muTagRepo.add(muTag: muTag)
        }.then { () -> Void in
            print("Successfully added mμ tag to account!")
            self.locationService.startRanging(for: [muTag])
            self.locationService.startMonitoringFor(muTagIds: [muTag.id])
            self.viewController.dismiss(animated: true, completion: nil)
        }.catch { error in
            print(error)
            _ = muTag.unprovision()
            self.viewController.dismiss(animated: true, completion: nil)
        }
    }
    
    func remove(muTag: MuTag) {
        let muTagId = muTag.id
        tagDetailViewC?.isRemovingMuTag = true
        viewController.dismiss(animated: true, completion: nil)
        muTag.unprovision().then { _ -> Promise<Void> in
            print("Successfully unprovisioned mµ tag device")
            self.muTagBleSessionManager.removeLegacyPeripheralFor(muTagId: muTagId)
            self.locationService.stopMonitoringFor(muTagIds: [muTagId])
            return self.muTagRepo.removeWith(muTagId: muTagId)
        }.then { () -> Void in
            print("Successfully removed mμ tag from account")
        }.catch { error in
            print(error)
        }
        /*var notificationObserver: NSObjectProtocol?
        notificationObserver = notificationCenter.addObserver(forName: .muTagReadyForRemoval, object: nil, queue: nil, using: { _ in
            self.muTagRepo.remove(id: muTag.id)
            self.notificationCenter.removeObserver(notificationObserver!, name: .muTagReadyForRemoval, object: nil)
        })*/
    }
    
    func didCancelPropertyChange(for muTagId: String) {
        
    }

    func changeMuTagPropertiesFailed(with error: Error, for muTagId: String) {
        print(error)
    }
}
