//
//  TutorialController.swift
//  Informu
//
//  Created by Tom Daniel D. on 9/25/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit
import MapKit
import RealmSwift
import Firebase

protocol TutorialControllerDelegate: class {
    func continueToApp()
    func configureSafeZone()
}

class TutorialController: ActiveController {
    let viewController: UIViewController
    let tutorialViewControllers: [UIViewController]
    fileprivate let session: Session
    fileprivate let addTagsViewC: AddTagsViewC
    //fileprivate let setTagPropertiesViewC: SetTagPropertiesViewC
    fileprivate let tutorialPage1ViewC = TutorialPage1ViewC()
    fileprivate let tutorialPage2ViewC = TutorialPage2ViewC()
    fileprivate let tutorialPage3ViewC = TutorialPage3ViewC()
    fileprivate let tutorialPage4ViewC = TutorialPage4ViewC()
    fileprivate let tutorialPage5ViewC = TutorialPage5ViewC()
    fileprivate let tutorialPage6ViewC = TutorialPage6ViewC()
    fileprivate let tutorialPage8ViewC = TutorialPage8ViewC()
    fileprivate let safeZoneViewC: SafeZoneViewC
    fileprivate let muTagRepo: MuTagRepository
    fileprivate let muTagBleSessionManager = MuTagBleSessionManager.shared
    fileprivate let locationService = LocationService.shared
    fileprivate let muTagFactory: MuTagFactory
    
    weak var delegate: TutorialControllerDelegate?
    
    init(muTagRepo: MuTagRepository, muTagFactory: MuTagFactory) throws {
        self.session = try Session.shared()
        self.viewController = TutorialViewC()
        //self.setTagPropertiesViewC = SetTagPropertiesViewC()
        self.addTagsViewC = AddTagsViewC()
        self.safeZoneViewC = SafeZoneViewC()
        self.tutorialViewControllers = [
            self.tutorialPage1ViewC,
            self.tutorialPage2ViewC,
            self.tutorialPage3ViewC,
            self.tutorialPage4ViewC,
            self.tutorialPage5ViewC,
            self.tutorialPage6ViewC,
            self.addTagsViewC,
            self.tutorialPage8ViewC
        ]
        self.muTagRepo = muTagRepo
        self.muTagFactory = muTagFactory
        
        if let vc = viewController as? TutorialViewC {
            vc.orderedViewControllers = tutorialViewControllers
        }
        
        // Set delegates
        //
        //self.setTagPropertiesViewC.delegate = self
        muTagBleSessionManager.delegate = self
        tutorialPage2ViewC.delegate = self
        tutorialPage3ViewC.delegate = self
        tutorialPage4ViewC.delegate = self
        tutorialPage5ViewC.delegate = self
        tutorialPage6ViewC.delegate = self
        tutorialPage8ViewC.delegate = self
        addTagsViewC.delegate = self
    }
}

extension TutorialController: MuTagBleSessionManagerDelegate {
    func didConnectToNewMuTag(muTag: MuTag, bleDispatcher: MuTagBleDispatcher) {
        guard let vc = viewController as? TutorialViewC else { return }
        let setTagPropertiesViewC = SetTagPropertiesViewC(muTag: muTag, muTagId: muTag.id)
        setTagPropertiesViewC.delegate = self
        vc.present(setTagPropertiesViewC, animated: true)
    }
}

extension TutorialController: AddTagsViewCDelegate {
    /*func startScanningForNewMuTag() {
        muTagBleSessionManager.scanForNewMuTagAndConnect()
    }*/

    func didCancelAddMuTag() {
        muTagBleSessionManager.stopScanningForNewMuTag()
    }
}

extension TutorialController: SetTagPropertiesViewCDelegate {
    func register(muTag: MuTag) {
        muTag.provision().then { _ -> Promise<Void> in
            print("Successfully provisioned mµ tag device")
            muTag.updateStatusToProvisioned()
            return self.muTagRepo.add(muTag: muTag)
        }.then { () -> Void in
            print("Successfully added mμ tag to account!")
            self.locationService.startRanging(for: [muTag])
            self.locationService.startMonitoringFor(muTagIds: [muTag.id])
            self.viewController.dismiss(animated: true, completion: nil)
            if let vc = self.viewController as? TutorialViewC {
                let nextPage = vc.orderedViewControllers[7]
                vc.setCurrentPage(to: nextPage)
            }
        }.catch { error in
            print(error)
            _ = muTag.unprovision()
            self.viewController.dismiss(animated: true, completion: nil)
            if let vc = self.viewController as? TutorialViewC {
                let previousPage = vc.orderedViewControllers[5]
                vc.setCurrentPage(to: previousPage)
            }
        }
    }
    
    /*func didUpdateMuTag(muTag: MuTag) {
     // TODO
     }*/
    
    func didCancelPropertyChangeFor(muTagId: String) {
        muTagBleSessionManager.stopScanningForNewMuTag()
        muTagBleSessionManager.disconnectFromMuTagWith(id: muTagId)
    }
    
    func changeMuTagPropertiesFailedFor(muTagId: String, with error: Error) {
        print(error)
        muTagBleSessionManager.stopScanningForNewMuTag()
        muTagBleSessionManager.disconnectFromMuTagWith(id: muTagId)
    }
}

extension TutorialController:
    TutorialPage2ViewCDelegate,
    TutorialPage3ViewCDelegate,
    TutorialPage4ViewCDelegate,
    TutorialPage5ViewCDelegate,
    TutorialPage6ViewCDelegate
{
    func continueToApp() {
        delegate?.continueToApp()
    }
}

extension TutorialController: TutorialPage8ViewCDelegate {
    func configureSafeZone() {
        delegate?.configureSafeZone()
    }
}

/*extension TutorialController: SafeZoneViewCDelegate {
    func safeZoneDidComplete(location: CLLocationCoordinate2D) {
        
        // DEBUG
        //
        print("DEBUG - safeZoneDidComplete: \(location)")
        
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                do {
                    let session = try Session.shared()
                    let realm = try Realm()
                    try realm.write {
                        session.account?.safeZone = String(location.latitude) + " " + String(location.longitude)
                    }
                } catch {
                    print(error)
                }
            }
        }
        guard let account = session.account else { return }
        let firebase = Database.database().reference()
        
        // Save safe zone to Firebase
        //
        firebase.child("accounts/\(account.id)/safe_zone").setValue(account.safeZone)
        
        locationService.registerSafeZone(at: location)
        delegate?.tutorialDidComplete()
    }
}*/
