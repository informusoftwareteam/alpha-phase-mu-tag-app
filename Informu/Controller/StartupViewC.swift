//
//  StartupViewC.swift
//  Informu
//
//  Created by Tom Daniel D. on 11/7/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit

class StartupViewC: UIViewController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
}

private extension StartupViewC {
    func prepareView() {
        view.translatesAutoresizingMaskIntoConstraints = false
        let mainView = StartupView(frame: CGRect.zero)
        view.layout(mainView).edges()
    }
}
