//
//  StateController.swift
//  Scrip
//
//  Created by Tom Daniel D. on 4/25/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Firebase

protocol ActiveController: class {
    var viewController: UIViewController { get }
}

class StateController {
    fileprivate let session: Session
    fileprivate let firebaseReactiveService = FirebaseReactiveService.shared
    fileprivate let locationService = LocationService.shared
    fileprivate let interactiveFeedback = InteractiveFeedback.shared
    fileprivate let muTagBleSessionManager = MuTagBleSessionManager.shared
    fileprivate let muTagRepo = MuTagRepository.shared
    fileprivate let muTagFactory: MuTagFactory
    let loginController: LoginController
    fileprivate let appWindow: UIWindow
    
    var rootViewC: UIViewController
    fileprivate var activeController: ActiveController!
    fileprivate var firmwareService: FirmwareService!
    fileprivate var account: Account!
    fileprivate var menuViewC: MenuViewC!
    fileprivate var toolbarViewC: AppToolbarViewC!
    fileprivate var appNavigationDrawerViewC: AppNavigationDrawerViewC!
    fileprivate var screenSelector: ScreenSelector!
    fileprivate var homeController: HomeController?
    fileprivate var settingsController: SettingsController!
    fileprivate var screenDictionary: Dictionary<MainScreen, ActiveController>!
    fileprivate var tutorialController: TutorialController?
    
    init(appWindow: UIWindow) throws {
        self.appWindow = appWindow
        // Creating a Realm instance could sometimes fail if resources are constrained. In practice,
        // this can only happen the first time a Realm instance is created on a given thread.
        //
        //self.realm = try! Realm()
        self.session = try Session.shared()
        self.muTagFactory = try MuTagFactory()
        self.loginController = try LoginController(muTagRepo: muTagRepo)
        self.rootViewC = self.loginController.viewController
        
        self.loginController.delegate = self
        self.locationService.delegate = self
        //self.muTagDispatcher.delegate = self
        
        // A sloppy way to wait for current view controller to load so we can present the activity indicator
        //
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { timer in
            self.loginController.tryLoginFromSavedSession()
        }
    }
}


private extension StateController {
    func loadApp(with homeScreen: HomeScreen) {
        menuViewC = MenuViewC()
        homeController = HomeController(muTagRepo: muTagRepo, muTagFactory: muTagFactory)
        //settingsController = SettingsController()
        screenDictionary = [
            MainScreen.Home: homeController!,
            //MainScreen.Settings: settingsController
        ]
        screenSelector = ScreenSelector(screens: screenDictionary)
        activeController = screenSelector.loadController(screen: MainScreen.Home)!
        let viewController = activeController.viewController
        // Prepare view controller hierarchy
        //
        toolbarViewC = AppToolbarViewC(rootViewController: viewController)
        appNavigationDrawerViewC = AppNavigationDrawerViewC(rootViewController: toolbarViewC, leftViewController: menuViewC)
        menuViewC.delegate = self
        
        homeController?.load(screen: homeScreen)
        appWindow.rootViewController = appNavigationDrawerViewC
        guard let account = session.account else { return }
        _ = locationService.getSafeZoneStatus()
        locationService.startRanging(for: Array(account.muTags)).always {
            self.firmwareService = FirmwareService.shared
        }
    }
    
    func unloadApp() {
        menuViewC = nil
        homeController = nil
        settingsController = nil
        screenDictionary = nil
        screenSelector = nil
        activeController = nil
        toolbarViewC = nil
        appNavigationDrawerViewC = nil
    }
    
    func showTutorial() throws {
        tutorialController = try TutorialController(muTagRepo: muTagRepo, muTagFactory: muTagFactory)
        tutorialController!.delegate = self
        appWindow.rootViewController = tutorialController!.viewController
    }
    
    func closeNavigationDrawer(_ result: Bool) {
        appNavigationDrawerViewC.closeLeftView()
    }
    
    func deleteFirebaseData() {
        let alertController = UIAlertController(title: "Erase Firebase Data", message: "WARNING! You are about to permanently remove data from Firebase", preferredStyle: .alert)
        let eraseAction = UIAlertAction(title: "ERASE", style: .destructive) {
            (result : UIAlertAction) -> Void in
            print("Attempting to remove Firebase data...")
            //Database.database().reference().child("accounts").removeValue()
            //Database.database().reference().child("lastAccountId").removeValue()
            //Database.database().reference().child("muTags").removeValue()
            //Database.database().reference().child("users").removeValue()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        alertController.addAction(eraseAction)
        alertController.addAction(cancelAction)
        appWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - MuTagDispatcherDelegate Conformance
//
extension StateController: LocationServiceDelegate {
    func didEnterSafeZone() {
        print("We are now in the safe zone")
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                do {
                    let session = try Session.shared()
                    guard let safeZone = session.account?.safeZone else { return }
                    let realm = try Realm()
                    let muTags = realm.objects(MuTag.self).filter("unsafe = false")
                    try realm.write {
                        session.inSafeZone = safeZone
                        for muTag in muTags {
                            muTag.inSafeZone = safeZone
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func didExitSafeZone() {
        print("We have exited the safe zone")
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                do {
                    let session = try Session.shared()
                    let realm = try Realm()
                    let muTags = realm.objects(MuTag.self).filter("unsafe = false")
                    try realm.write {
                        session.inSafeZone = ""
                        for muTag in muTags {
                            muTag.inSafeZone = ""
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
}

// MARK: - LoginControllerDelegate Conformance
//
extension StateController: LoginControllerDelegate {
    func loginDidComplete(withSavedSession: Bool) {
        switch withSavedSession {
        case true:
            loadApp(with: .tags)
        case false:
            do {
                try showTutorial()
            } catch {
                print(error)
                loadApp(with: .tags)
            }
        }
    }
}

// MARK: - TutorialControllerDelegate Conformance
//
extension StateController: TutorialControllerDelegate {
    func continueToApp() {
        locationService.getSafeZoneStatus()
        loadApp(with: .tags)
    }
    
    func configureSafeZone() {
        loadApp(with: .map)
    }
}

// MARK: - MenuViewCDelegate Conformance
//
extension StateController: MenuViewCDelegate {
    func updateActiveViewC(screen: MainScreen) {
        print("updateActiveViewC")
        
        activeController = screenSelector.loadController(screen: screen)!
        let activeViewC = activeController.viewController
        toolbarViewC.transition(to: activeViewC, completion: closeNavigationDrawer)
    }
    
    func logout() {
        do {
            try Session.resetSession()
        } catch {
            print(error)
        }

        unloadApp()
        
        appWindow.rootViewController = loginController.viewController
    }
}
