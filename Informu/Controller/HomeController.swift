//
//  HomeController.swift
//  Informu
//
//  Created by Tom Daniel D. on 7/28/17.
//  Copyright © 2017 informu. All rights reserved.
//

import Foundation
import UIKit
import Material
import RealmSwift

enum HomeScreen: Int {
    case tags = 0
    case map
}

class HomeController: ActiveController {
    var viewController: UIViewController
    
    let viewControllers: Array<UIViewController>
    fileprivate let tabsViewC: TabsController
    fileprivate let tagsController: TagsController
    fileprivate let mapController: MapController
    //fileprivate let activityController: ActivityController
    
    init(muTagRepo: MuTagRepository, muTagFactory: MuTagFactory) {
        self.tagsController = TagsController(muTagRepo: muTagRepo, muTagFactory: muTagFactory)
        self.mapController = MapController()
        //self.activityController = ActivityController()
        self.viewControllers = [
            tagsController.viewController,
            mapController.viewController,
            //activityController.viewController
        ]
        self.tabsViewC = AppTabsViewC(viewControllers: self.viewControllers)
        self.viewController = self.tabsViewC
        
        tagsController.delegate = self
    }
    
    func load(screen: HomeScreen) {
        tabsViewC.select(at: screen.rawValue)
    }
}

extension HomeController: TagsControllerDelegate {
    func goToMap() {
        tabsViewC.select(at: HomeScreen.map.rawValue)
    }
}
