//
//  AppDelegate.swift
//  Scrip
//
//  Created by Tom Daniel D. on 3/8/17.
//  Copyright © 2017 Peradym. All rights reserved.
//

import UIKit
import Material
import Firebase
import GoogleSignIn
import FacebookCore
import GoogleMaps
import Fabric
import Crashlytics
import PinpointKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    fileprivate static let pkConfiguration = Configuration(
        sender: PinpointKitSender(),
        feedbackConfiguration: FeedbackConfiguration(recipients: ["software@informu.io"]))
    fileprivate static let pinpointKit = PinpointKit(configuration: pkConfiguration)
    fileprivate let requirementsService = RequirementsService()
    let window = ShakeDetectingWindow(frame: UIScreen.main.bounds, delegate: AppDelegate.pinpointKit)//UIWindow(frame: Screen.bounds)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //
        // Override point for customization after application launch.
        
        // DEBUG
        //Crashlytics.sharedInstance().crash()
        
        let startupViewC = StartupViewC()
        window.rootViewController = startupViewC
        window.makeKeyAndVisible()
        requirementsService.verifyAllRequirements().then { _ -> () in
            Fabric.with([Crashlytics.self])
            FirebaseApp.configure()
            GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
            GMSServices.provideAPIKey("AIzaSyDchNPC61TH7HCJO2hC7bpZ7rOnsPWSNzU")
            SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
            let stateController = try StateController(appWindow: self.window)
            self.window.rootViewController = stateController.rootViewC
        }.catch { error in
            print(error)
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        //
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        LocationService.shared.setLocationAccuracyForBackground()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        //
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        LocationService.shared.setLocationAccuracyForForeground()
        requirementsService.verifyAllRequirements()
        FirmwareService.shared.updateIfNeeded().then { () -> () in
            try? MuTagParameters().updateAccountMuTagsFromGlobalIfNeeded()
        }.catch { error in
            print(error)
            try? MuTagParameters().updateAccountMuTagsFromGlobalIfNeeded()
        }
        InteractiveFeedback.shared.alertForFeedbackIfNeeded()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        //
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
                                                                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                annotation: [:])
        let facebookDidHandle = SDKApplicationDelegate.shared.application(app, open: url, options: options)
        return googleDidHandle || facebookDidHandle
    }
}
